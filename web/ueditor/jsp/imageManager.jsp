<%@ page language="java" pageEncoding="utf-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.servlet.ServletContext"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="com.knife.news.logic.SiteService,com.knife.news.logic.impl.SiteServiceImpl,com.knife.news.object.Site,com.knife.util.CommUtil,com.knife.web.Globals"%>
<% 
	String sid = CommUtil.null2String(request.getParameter("sid"));
	Site site = new Site();
	SiteService siteDAO = new SiteServiceImpl();
	if(sid.length()>0){
		site = siteDAO.getSiteById(sid);
	}else{
		site = siteDAO.getSite();
	}
	String path = "/html/" + site.getPub_dir() + "/upload/";
	String imgStr ="";
	String realpath = getRealPath(request,path)+"/"+path;
	List<File> files = getFiles(realpath,new ArrayList());
	for(File file :files ){
		imgStr+=file.getPath().replace(getRealPath(request,path),"")+"ue_separate_ue";
	}
	if(imgStr!=""){
        imgStr = imgStr.substring(0,imgStr.lastIndexOf("ue_separate_ue")).replace(File.separator, "/").trim();
    }
	out.print(imgStr);		
%>
<%!
public List getFiles(String realpath, List files) {
	
	File realFile = new File(realpath);
	if (realFile.isDirectory()) {
		File[] subfiles = realFile.listFiles();
		for(File file :subfiles ){
			if(file.isDirectory()){
				getFiles(file.getAbsolutePath(),files);
			}else{
				if(!getFileType(file.getName()).equals("")) {
					files.add(file);
				}
			}
		}
	}
	return files;
}

public String getRealPath(HttpServletRequest request,String path){
	//ServletContext application = request.getSession().getServletContext();
	String str = Globals.APP_BASE_DIR+"/html";
	return new File(str).getParent();
}

public String getFileType(String fileName){
	String[] fileType = {".gif" , ".png" , ".jpg" , ".jpeg" , ".bmp"};
	Iterator<String> type = Arrays.asList(fileType).iterator();
	while(type.hasNext()){
		String t = type.next();
		if(fileName.endsWith(t)){
			return t;
		}
	}
	return "";
}
%>