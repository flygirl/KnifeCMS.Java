<%@ page language="java" pageEncoding="UTF-8"  import="java.util.*,com.knife.tools.TreeUtils"%>
<%@ include file="checkUser.jsp" %>
<%
try{
	if(user!=null){
		String nodeId = request.getParameter("id");
		nodeId = nodeId == null ? "0":nodeId.trim();
		String tree_id = request.getParameter("tree_id");
		tree_id = tree_id == null ? "":tree_id.trim();
		String jsonStr = "";
		String user_id = popedom==0 ? user.getId():"";
		jsonStr = TreeUtils.getSubTreeNodesJson("",nodeId,user_id,tree_id,"");
		response.setContentType("text/json;charset=utf-8");
		response.getWriter().write(jsonStr);
	}
}catch(Exception e){
}
%>