// JavaScript Document
var picid="";
var oldLine=0;
var oldColumn=0;
var newLine=0;
var newColumn=0;

	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column",
			stop: function(event, ui) {
				newColumn=$( ".column" ).index(ui.item.parent(".column")[0]);
				newLine = $(".portlet").index(ui.item[0]);
				//alert("元素已从 列:"+oldColumn+" 移动到了 列："+newColumn+",序列编号由 "+oldLine+" 变为 "+newLine);
				updatePosition();
			}
		});
		
		//开始拖动
		$( ".portlet" ).mousedown(function() {
			oldColumn=$( ".column" ).index($(this).parent(".column")[0]);
			oldLine = $(".portlet").index(this);
			$(this).addClass( "portalet-move" );
			$( ".column" ).addClass( "guide-line" );
			picid=this.id
		});
		
		//拖动完毕
		$( ".portlet" ).mouseup(function() {
			$(this).removeClass( "portalet-move" );
			$( ".column" ).removeClass( "guide-line" );
		});
		
		$( ".column" ).disableSelection();
	});
	
function updatePosition(){
	if(oldLine==newLine){
	}else{
		if(picid!=""){
			var newOrder=newLine+1;
			//alert('图片'+picid+'将被移至:'+newOrder);
			jQuery.get("/manage_pic_ajax.ad" , { parameter : "order", id:picid , order : newOrder } );
			picid="";
		}
	}
}