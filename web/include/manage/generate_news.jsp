<%@ page language="java" pageEncoding="UTF-8"
	import="java.util.*,java.io.*,tools.HtmlUtil,com.knife.service.HTMLGenerater,com.knife.util.CommUtil,com.knife.web.Globals"%>
<%@ page import="com.knife.news.logic.*,com.knife.news.logic.impl.*"%>
<%@ page import="com.knife.news.object.Type,com.knife.news.object.News"%>
<%@ include file="checkUser.jsp"%><html>
	<head>
		<title>KnifeCMS生成文章</title>
		<style>
body {
	font-size: 12px;
	background: black;
	color: white
}
</style>
	</head>
	<body>
		<%
			String id = "";
			if (request.getParameter("id") != null) {
				id = request.getParameter("id");
			}
			if (id.length() > 0) {
			//锁定生成
			File lock=new File(Globals.APP_BASE_DIR+"/html/gen.lock");
			if(!lock.exists()){
        		try{
        			lock.createNewFile();
        		}catch(Exception e){
        			System.out.println("can't create lock file,it may cause generate error,info:"+e.getMessage());
        		}
        	}
			
				TypeService typeDAO = TypeServiceImpl.getInstance();
				NewsService newsDAO = NewsServiceImpl.getInstance();
				News anews = newsDAO.getNewsById(id);
				Type gtype = typeDAO.getTypeById(anews.getType());
				HTMLGenerater g = new HTMLGenerater(gtype.getSite());
				g.saveNewsToHtml(anews.getId());
				out.println("[生成文章]" + anews.getTitle() + ":"+anews.getHref()+"<br/>");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				out.flush();
				gtype.setNeedpub(1);
				typeDAO.updateType(gtype);
				out.println("[频道更新]" + gtype.getName() + ":频道将在后台更新<br/>");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				out.flush();
				/*
				Collection<Object> paras = new ArrayList<Object>();
				paras.add(gtype.getId());
				String sql = "k_display='1' and k_type=?";
				int rows = newsDAO.getSum(1, "",gtype.getId(), "", "", "",1);
				int pageSize = CommUtil.null2Int(gtype.getPagenum());// 每页20条
				int totalPage = (int) Math.ceil((float) (rows)
						/ (float) (pageSize));// 计算页数
				if(totalPage<1){totalPage=1;}
				for (int i = 0; i < totalPage; i++) {
					int nowPage=i+1;
					g.saveTypeListToHtml(gtype.getId(),sql, paras,nowPage);
					out.println("[生成频道]" + gtype.getName() + ":"
							+ gtype.getHref(nowPage) + "<br/>");
					out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
					out.flush();
				}
				*/
				g.saveIndexToHtml();
				out.println("[生成首页]<br/>");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				out.flush();
				out.println("生成完毕！");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				
        	if(lock.exists()){
        		lock.delete();
        	}
			}
		%>
	</body>
</html>