<%@ page language="java" pageEncoding="UTF-8" import="com.knife.news.model.Constants,com.knife.news.object.User"%>
<%@ include file="checkUser.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>发文统计</title>
		<link rel="stylesheet" href="/include/manage/zTreeStyle/zTreeStyle.css" type="text/css" />
<script type="text/javascript" src="/include/manage/js/jquery.js"></script>
<script type="text/javascript" src="/include/manage/js/jquery-ztree.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
	$.ajaxSetup ({
		cache: false
	});

	var zTree1;
	var setting;

	setting = {
		showLine: true,
		callback: {
			click: zTreeOnClick,
			rightClick: zTreeOnRightClick
		}
	};

	zNodes =[
		{ "id":"user", "name":"按用户统计"},
		{ "id":"type", "name":"按频道统计"}
	];

	$(document).ready(function(){
		refreshTree();

		$("body").bind("mousedown", 
			function(event){
				if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
					$("#rMenu").hide();
				}
			});
	});

	function showRMenu(type, x, y) {
		$("#rMenu ul").show();
		if (type=="root") {
			$("#m_add").hide();
		}
		$("#rMenu").css({"top":y+"px", "left":x+"px", "display":"block"});
	}
	function hideRMenu() {
		$("#rMenu").hide();
	}

	function zTreeOnRightClick(event, treeId, treeNode) {
		
	}
	
	function expandAll(expandSign) {
		zTree1.expandAll(expandSign);
	}

	function refreshTree() {
		hideRMenu();
		//zTree1 = $("#treeList").zTree(setting);
		zTree1 = $("#treeList").zTree(setting, zNodes);
	}
	
	function zTreeOnClick(event, treeId, treeNode){
		window.parent.frames["config"].location.href="/manage_post.ad?parameter=" + treeNode.id;
	}
	
	function addUser(){
		var node = zTree1.getSelectedNode();
		if (node) {
			window.parent.frames["config"].location.href="/manage_user.ad?parameter=add&tid=" + node.id;
		}
		hideRMenu();
	}
  //-->
</SCRIPT>
<style type="text/css">
body {
 padding: 0px;
 margin: 0px;
 font-size:12px;
}
#treeTitle{
	font-weight:bold;
	background:url('/include/manage/images/main/tab_05.gif');
	/*width:165px;*/
	height:28px;
	margin:3px;
	margin-bottom:0;
	border-left:1px solid #b5d6e6;
	border-right:1px solid #b5d6e6;
	text-indent:15px;
}
#treeTitle a{line-height:28px;color:black;text-decoration:none}
#treeList {
	margin:3px;
	margin-top:0;
	/*width:165px;*/
	height:482px;
	border:1px solid #b5d6e6;
	border-top:0;
	overflow:hidden;
	overflow-y:auto;
}
.tree{padding:0;}
/* ------------- 右键菜单 -----------------  */

div#rMenu {
	background-color:#F9F9F9;
	text-align: left;
	width:72px;
	border-top:1px solid #F1F1F1;
	border-left:1px solid #F1F1F1;
	border-right:1px solid gray;
	border-bottom:1px solid gray;
}

div#rMenu ul {
	list-style: none outside none;
	margin:0px 1px;
	padding:0;
}
div#rMenu ul li {
	cursor: pointer;
	width:70px;
	height:20px;
	line-height:20px;
	text-indent:8px;
	margin:1px 0px;
	font-size:12px;
	border-bottom:1px solid gray;
}
div#rMenu ul li a{display:block;width:100%;text-decoration:none;color:black}
div#rMenu ul li a:hover{
	color:white;
	background-color:darkblue;
}
div#rMenu ul li a:visited{color:black}
</style>
	</head>
<body>
	<div id="treeTitle"><img src="/include/manage/zTreeStyle/img/sim/tree.png" align="absmiddle"/>&nbsp;<a href="/manage_post.ad?parameter=summary" target="config">发文概览</a></div>
	<div id="treeList" class="tree"></div>
</body>
</html>