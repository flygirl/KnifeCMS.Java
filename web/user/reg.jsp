<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
Userinfo auser=null;
String login_email="";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>用户注册</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
	<link rel="stylesheet" href="skin/css/style.css" type="text/css" media="all" />
	<style type="text/css">
	#regForm{
		margin:0;
		width:1000px;
		text-align:center;
		background:#C7D7E7;
	}
	#regform{width:1000px;}
	#regForm h4{width:560px;text-align:left}
	#regForm a{
		color:#DBDBDB;
	}
	#regForm table tr td{height:30px;text-align:left}
	.inputText input{
		border:1px solid gray;
		width:230px;
	}
	.mustInput{
		color:red;
	}
	#saveUserInfo{
		border:none;
	}
	</style>
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
	<link rel="stylesheet" href="skin/js/validationEngine.jquery.css" type="text/css" />
	<script type="text/javascript" src="skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine-cn.js"></script>
	<script type="text/javascript" src="datepicker/WdatePicker.js"></script>
	<script language="javascript">
	jQuery.noConflict();
	jQuery(function(){
		jQuery("#regform").validationEngine();
	});
	
	function checkForm(){
		jQuery("#regform").submit();
	}
	
	function resetForm(){
		jQuery("#regform")[0].reset();
	}
	
	function chooseDate(){
		jQuery("#brithdate").date_input();
		return false;
	}
	
function reloadImg(id) {
   var obj = document.getElementById(id);
   var src = obj.src;
   var pos = src.indexOf('?');
   if (pos >= 0) {
      src = src.substr(0, pos);
   }
   var date = new Date();
   obj.src = src + '?v=' + date.getTime();
   return false;
}
	</script>
  </head>
  
<body>
	<div class="container">
		<%@include file="inc/head.jsp" %>
		<div class="clearfloat"></div>
		<div id="regForm">
			<div class="clearFloat" style="height:8px;"></div>
			<h4>注册试用系统会员账户</h4>
			<div class="clearFloat" style="width:1000px;height:1px;border-top:1px solid gray"></div>
			<form id="regform" name="regform" action="opt/savereg.jsp" method="post" style="margin:0;padding:0">
			<div style="background:white;margin:8px;border:1px solid gray;text-align:center">
			<table width="540" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:12px auto">
				<tr>
					<td width="280">用户名：</td>
					<td class="inputText">
						<input id="acount" type="text" name="acount" class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						密码（6位）：
					</td>
					<td class="inputText">
						<input id="password" type="password" name="password" class="validate[required,length[6,6]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						确认密码（6位）：
					</td>
					<td class="inputText">
						<input id="password2" type="password" name="password2" class="validate[required,length[6,6],equals[password]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						真实姓名：
					</td>
					<td class="inputText">
						<input id="realname" type="text" name="realname" class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						性别：
					</td>
					<td>
						<input id="male" name="sex" type="radio" value="1" checked="checked" /><label for="male">男</label>
						<input id="female" name="sex" type="radio" value="0" /><label for="female">女</label>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						出生日期：
					</td>
					<td class="inputText">
						<input id="birthday" value="1920-01-01" type="text" name="birthday" class="Wdate validate[required,custom[date]]" onClick="WdatePicker()" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						教育程度：
					</td>
					<td class="inputText">
						<input type="text" name="education" />
					</td>
				</tr>
				<tr>
					<td>
						职业经验：
					</td>
					<td class="inputText">
						<input type="text" name="professional" />
					</td>
				</tr>
				<tr>
					<td>
						E-mail：
					</td>
					<td class="inputText">
						<input id="email" type="text" name="email" class="validate[required,custom[email]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						手机：
					</td>
					<td class="inputText">
						<input id="phone" type="text" name="phone" class="validate[required,custom[phone]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						电话号码 ：
					</td>
					<td class="inputText">
						<input type="text" name="mobile" />
					</td>
				</tr>
				<tr>
					<td>
						任职机构及部门：
					</td>
					<td class="inputText">
						<input id="department" type="text" name="department" class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						职务：
					</td>
					<td class="inputText">
						<input type="text" name="organization" />
					</td>
				</tr>
				<tr>
					<td>
						地址：
					</td>
					<td class="inputText">
						<input type="text" name="location" />
					</td>
				</tr>
				<tr>
					<td>
						邮编：
					</td>
					<td class="inputText">
						<input type="text" name="zipcode" />
					</td>
				</tr>
				<tr>
					<td>
						验证码：
					</td>
					<td class="inputText">
						<input name="vcode" type="text" id="c_vcode" size="8" style="width:48px;float:left" />
						<div style="float:left"><img id="vcode" src="/valicode" alt="验证码看不清楚？请点击刷新验证码" style="cursor:pointer;" onClick="return reloadImg('vcode');" /></div>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="button" style="border:0;background:url('skin/images/agree.gif');width:196px;height:17px" onclick="checkForm()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" style="border:0;background:url('skin/images/reset.gif');width:67px;height:17px" onclick="resetForm()" />
					</td>
				</tr>
				<tr><td colspan="2"><a href="/user/lisense.html" target="_blank" style="color:black">《中国风险管理技术交流促进平台服务条款》</a></td></tr>
			</table>
				<div class="clearFloat"></div>
			</div>
			</form>
			<div class="clearFloat" style="height:1px"></div>
		</div>
		<%@include file="inc/foot.jsp" %>
	</div>
</body>
</html>