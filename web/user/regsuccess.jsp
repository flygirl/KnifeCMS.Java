<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
Userinfo auser=null;
String login_email="";
if(request.getParameter("email")!=null){
	login_email=request.getParameter("email");
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>注册成功</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<link rel="stylesheet" href="skin/css/style.css" type="text/css" media="all" />
	<style type="text/css">
	#loginForm{
		margin:0px auto;
		width:1000px;
		padding:80px 0px;
		text-align:center;
		background:#C7D7E7;
	}
	#loginform{
		width:1000px;
	}
	#loginForm a{
		color:gray;
	}
	#loginForm input{
		border:1px solid gray;
	}
	#saveUserInfo{
		border:none;
	}
	.inputText input{
		width:150px;
	}
	</style>
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
	<link rel="stylesheet" href="skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		sendActiveMail();
	});
	
	function sendActiveMail(){
		$("#msg").load("/user/opt/mailactive.jsp?email=<%=login_email%>");
	}
	</script>
  </head>

<body>
	<div class="container">
		<%@include file="inc/head.jsp" %>
		<div class="clearfloat"></div>
		<div id="loginForm">
			<table width="350" align="center" cellspacing="0" cellpadding="0" border="0" style="border:1px solid gray;text-align:center;font-size:12px;background:white">
				<tr><td style="background:#12335F;color:white;border-bottom:1px solid gray;height:32px;font-size:14px;">注册成功</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="inputText">
						<b>您已成功注册为会员</b>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
					<p id="msg" align="left">系统正向您的邮箱发送激活邮件</p>
					<p>若您未能成功接收邮件，<a href="javascript:sendActiveMail()">点击这里再次发送</a></p>
					<!-- p align="left">尊敬的会员，在浏览资料时，需要下载并安装插件，否则无法正常浏览内容，请从 <a href="/cfrisk.zip">http://www.cfrisk.org/cfrisk.zip</a> 下载程序到本地，解压并执行cfrisk.vbs安装设置！<br/>
						如果您没有安装权限，请咨询本单位系统管理员协助安装！</p -->
					<p align="center"><a href="/user/index.jsp">返回会员专区</a></p>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td style="background:#12335F;color:white;border-top:1px solid gray">&nbsp;</td></tr>
			</table>
		</div>
		<%@include file="inc/foot.jsp" %>
	</div>
</body>
</html>