<%@ page language="java" import="java.util.*,com.knife.member.*,com.knife.member.stat.*" pageEncoding="UTF-8"%>
<%@ page import="com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News"%>
<%@include file="checkuser.jsp" %><%
session.setAttribute("permission","1");
String docurl="";
String id="";
int rights=0;
int score=0;
boolean isvisited=false;
if(request.getParameter("url")!=null){
	docurl=request.getParameter("url");
	if(docurl.indexOf("/")!=0){
		docurl="";
	}
}
NewsService newsDAO=new NewsServiceImpl();
List adocs=new ArrayList();
if(docurl.length()>0){
	Collection<Object> params = new ArrayList<Object>();
	params.add(docurl);
	adocs = newsDAO.getNewsBySql("id!='' and k_docfile=?",params,0,-1);
}
if(adocs.size()>0){
	News adoc=(News)adocs.get(0);
	id=adoc.getId();
	rights=adoc.getIsfee();
	score=adoc.getScore();
}
//判断是否登录,扣除积分
isvisited=StatHandle.hasView(login_email,docurl);
if(!isvisited){
	if(auser!=null){
		if(rights>auser.getIsfee()){
			out.print("<script>alert('您的权限不够！');parent.location.href='../index.jsp';</script>");
			out.flush();
		}
		if(score>0){
			out.print("<script>if(confirm('浏览此资源要扣除资源分:"+score+"分！')){}else{parent.location.href='../index.jsp';};</script>");
			if(score>auser.getScore()){
				out.print("<script>alert('您的积分不够:"+score+"分！');parent.location.href='../index.jsp';</script>");
				out.flush();
			}else{
				auser.setScore(auser.getScore()-score);
				UserinfoDAO userDAO=new UserinfoDAO();
				userDAO.save(auser);
			}
		}
	}
}

int type=0;
if(request.getParameter("type")!=null){
	type=Integer.parseInt(request.getParameter("type").toString());
}
//记录用户访问的资料地址
		if(session.getAttribute("isRecorded"+docurl) == null) {
			// 如果本次用户访问还没有记录，就记录本次用户信息，并保存到数据库中
			session.setAttribute("isRecorded"+docurl, Boolean.TRUE);
			// 在session对象中保存一个变量"isRecorded"，并赋值
			String ip = request.getRemoteAddr();
			// 从请求头中取出客户端IP地址
			String agent = request.getHeader("User-Agent");
			System.out.println("HTTP-HEAD: "+agent);
			// 从请求头中读取User-Agent值
			StringTokenizer st = new StringTokenizer(agent, ";)");
			// 构造StringTokenizer对象，使用“；”“）”来分解User-Agent值
			st.nextToken();
			String browser = st.nextToken();
			// 得到用户的浏览器名
			String os = st.nextToken();
			/*String url = request.getScheme() + "://";
			url += request.getHeader("host");
			url += request.getRequestURI();
			if (request.getQueryString() != null)
				url += "?" + request.getQueryString();*/
			//String url = request.getParameter("url");
			// 得到用户的操作系统名
			if (ip == null) {
				ip = "未知";
				// 如果读取的IP为空，则赋值为“未知”
			}
			if (browser == null) {
				browser = "未知";
				// 如果读取的浏览器名为空，则赋值为“未知”
			}
			if (os == null) {
				os = "未知";
				// 如果读取的操作系统名为空，则赋值为“未知”
			}
			try {
				StatHandle.insert(ip,login_email, os.trim(), browser.trim(), docurl.trim(),type);
				// 调用业务逻辑，将用户数据插入到数据库中
			} catch (Exception es) {
				es.printStackTrace();
				// 在输入日志中打印异常信息
			}
		}
%>