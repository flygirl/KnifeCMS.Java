<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,java.sql.Timestamp" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
Boolean isreg=true;
int adduser=0;
UserinfoDAO userDAO = new UserinfoDAO();
Userinfo adminUser=null;
if(request.getParameter("adduser")!=null){
	adduser			= Integer.parseInt(request.getParameter("adduser"));
}
		//机构管理员
		if(adduser>0){
			adminUser = userDAO.findById((long)adduser);
			//已经使用完了所有名额
			if(adminUser.getUsednum()>=adminUser.getAdminnum()){
				msg="编辑失败！已经用完所有名额";
				isreg=false;
			}
		}

int	uid	=0;
if(request.getParameter("uid")!=null){
	uid				= Integer.parseInt(request.getParameter("uid"));
}
String acount 		= request.getParameter("acount");
String password 	= "";
if(request.getParameter("password")!=null){
	password 	= request.getParameter("password");
}
String realname 	= request.getParameter("realname");
String sex			= request.getParameter("sex");
String birthday		= request.getParameter("birthday");
String sysj		= request.getParameter("sysj");
String education	= request.getParameter("education");
String professional = request.getParameter("professional");
String email		= request.getParameter("email");
String phone		= request.getParameter("phone");
String mobile		= request.getParameter("mobile");
String department	= "";
if(request.getParameter("department")!=null){
	department		= request.getParameter("department");
}
String organization = request.getParameter("organization");
String location		= request.getParameter("location");
String zipcode		= request.getParameter("zipcode");

if(acount.equals("admin")){
	msg="编辑失败！不能使用关键字admin";
	isreg=false;
}
List checkList  = userDAO.findByEmail(email);
List checkList1 = userDAO.findByAcount(acount);
if(checkList.size()>0 && checkList1.size()==0){
	msg="编辑失败！邮箱已使用";
	isreg=false;
}
if(isreg){
	Userinfo newUser=new Userinfo();
	if(uid>0){
		newUser = userDAO.findById((long)uid);
	}
	newUser.setAcount(acount);
	if(password.trim().length()>0){
		password=CheckPassword.generatePassword(password);
		newUser.setPassword(password);
	}
	newUser.setRealname(realname);
	if(sex.equals("1")){
		newUser.setSex(true);
	}else{
		newUser.setSex(false);
	}
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	try{ 
		Date applyDate = dateFormat.parse(birthday);
		Timestamp applyTime = new Timestamp(applyDate.getTime());
		newUser.setBirthday(applyTime);
	}catch(Exception e){
	}
	try{ 
		Date applyDate1 = dateFormat.parse(sysj);
		Timestamp applyTime1 = new Timestamp(applyDate1.getTime());
		newUser.setSysj(applyTime1);
	}catch(Exception e){
	}
	newUser.setEducation(education);
	newUser.setProfessional(professional);
	newUser.setPhone(phone);
	newUser.setMobile(mobile);
	//如果管理员更改了邮件
	if(email.length()>0){
		newUser.setEmail(email);
	}
	//如果管理员更改了部门名称
	if(department.length()>0){
		if(newUser.getDepartment()!=null){
			if(newUser.getDepartment().length()>0 && !"null".equals(newUser.getDepartment())){
				List<Userinfo> users=userDAO.findByDepartment(newUser.getDepartment());
				for(Userinfo auser:users){
					auser.setDepartment(department);
					userDAO.update(auser);
				}
			}
		}
		newUser.setDepartment(department);
	}
	newUser.setOrganization(organization);
	newUser.setLocation(location);
	newUser.setZipcode(zipcode);
	try{
		userDAO.save(newUser);
		if(adduser>0){
			if(adminUser!=null){
				UserinfoDAO adminDAO = new UserinfoDAO();
				adminUser.setId((long)adduser);
				adminUser.setUsednum(adminUser.getUsednum()+1);
				adminDAO.update(adminUser);
			}
		}
		msg="编辑成功！";
	}catch(Exception e){
		msg="编辑失败！";
		e.printStackTrace();
	}
}
%>
<script language="javascript">
alert("<%=msg%>");
history.back(-1);
</script>