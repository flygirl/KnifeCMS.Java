<%@ page language="java" import="java.util.*,java.text.SimpleDateFormat,java.sql.Timestamp,com.knife.member.*" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
Boolean isreg=true;
//检查验证码
String randomCode = (String) session.getAttribute("valiCode");
String submitrandom = request.getParameter("vcode");
if(submitrandom!=null){
	if(!submitrandom.equals(randomCode)){
		msg="验证码不匹配:"+submitrandom+"/"+randomCode;
		isreg=false;
	}
}else{
	msg="非法的提交";
	isreg=false;
}

//销毁所有会员标识
session.removeAttribute("loginuser");
Cookie[] cookies=request.getCookies();
if(cookies!=null)   
{
	for(int i=0;i<cookies.length;i++){
		cookies[i].setValue(null);
		cookies[i].setMaxAge(0);
		cookies[i].setPath("/");
		response.addCookie(cookies[i]);
    }
}

String acount 		= request.getParameter("acount");
String password 	= request.getParameter("password");
String realname 	= request.getParameter("realname");
String sex			= request.getParameter("sex");
String birthday	= request.getParameter("birthday");
String education	= request.getParameter("education");
String professional = request.getParameter("professional");
String email		= request.getParameter("email");
String phone		= request.getParameter("phone");
String mobile		= request.getParameter("mobile");
String department	= request.getParameter("department");
String organization = request.getParameter("organization");
String location		= request.getParameter("location");
String zipcode		= request.getParameter("zipcode");

if(acount.equals("admin")){
	msg="注册失败！不能使用关键字admin";
	isreg=false;
}
UserinfoDAO userDAO = new UserinfoDAO();
List checkList=new ArrayList();
try{
	checkList=userDAO.findByEmail(email);
}catch(Exception e){
}
if(checkList.size()>0){
	msg="注册失败！邮箱已使用";
	isreg=false;
}

if(isreg){
	Userinfo newUser = new Userinfo();
	newUser.setAcount(acount);
	password=CheckPassword.generatePassword(password);
	newUser.setPassword(password);
	newUser.setRealname(realname);
	if(sex.equals("1")){
		newUser.setSex(true);
	}else{
		newUser.setSex(false);
	}
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
	try{ 
		Date applyDate = dateFormat.parse(birthday); 
		Timestamp applyTime = new Timestamp(applyDate.getTime());
		newUser.setBirthday(applyTime);
	}catch(Exception e){
	}
	newUser.setEducation(education);
	newUser.setProfessional(professional);
	newUser.setEmail(email);
	newUser.setPhone(phone);
	newUser.setMobile(mobile);
	newUser.setDepartment(department);
	newUser.setOrganization(organization);
	newUser.setLocation(location);
	newUser.setZipcode(zipcode);
	newUser.setIsfee(0);
	try{
		userDAO.save(newUser);
		msg="注册成功！";
	}catch(Exception e){
		msg="注册失败！";
		System.out.println("注册错误:"+e.getMessage());
	}
}
if(msg.equals("注册成功！")){
	response.sendRedirect("/user/regsuccess.jsp?email="+email);
}else{
%>
<script language="javascript">
alert("<%=msg%>");
location.href="/user/login.jsp";
</script>
<%}%>