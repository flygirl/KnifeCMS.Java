<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="javax.mail.*"%>
<%@ page import="javax.activation.*"%>
<%@ page import="javax.mail.internet.*"%>
<%@ page import="java.util.*,java.io.*"%>
<%@page import="java.net.URL"%>
<%@page import="com.knife.member.*"%>
<%
	String email = "";
	if (request.getParameter("email") != null) {
		email = request.getParameter("email");
	}
	if (email.length() <= 0) {
		out.println("<script language=\"javascript\">alert('参数错误！');history.back(-1);</script>");
		out.flush();
		return;
	}
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>发送邮件</title>
	</head>
	<body>
		<%
			InternetAddress[] address = null;
			//request.setCharacterEncoding("utf8");
			String mailserver = "smtp.126.com";//发出邮箱的服务器
			String From = "cfrisk@126.com";//发出的邮箱
			String to = email;//发到的邮箱
			String Subject = "[中国金融风险经理网]忘记密码提醒";//标题
			String type = "text/plain";//发送邮件格式为html

			Userinfo auser = null;
			UserinfoDAO userDAO = new UserinfoDAO();
			List<Userinfo> users = userDAO.findByEmail(email);
			String password = "";
			if (users.size() > 0) {
				auser = users.get(0);
				//password = auser.getPassword();
				password = "123456";
				String realpassword=CheckPassword.generatePassword(password);
				auser.setPassword(realpassword);
				userDAO.update(auser);
			} else {
				out.println("<script language=\"javascript\">alert('邮箱错误！');history.back(-1);</script>");
				out.flush();
				return;
			}
			String messageText = "您在[中国金融风险经理网]的密码为: " + password
					+ " 请注意保密，妥善保管！";// 发送内容 

			boolean sessionDebug = false;

			try {
				// 设定所要用的Mail 服务器和所使用的传输协议 
				java.util.Properties props = System.getProperties();
				props.put("mail.host", mailserver);
				props.put("mail.transport.protocol", "smtp");
				props.put("mail.smtp.auth", "true");//指定是否需要SMTP验证 

				// 产生新的Session 服务 
				javax.mail.Session mailSession = javax.mail.Session
						.getDefaultInstance(props, null);
				mailSession.setDebug(sessionDebug);
				Message msg = new MimeMessage(mailSession);

				// 设定发邮件的人 
				msg.setFrom(new InternetAddress(From));
				// 设定收信人的信箱 
				address = InternetAddress.parse(to, false);
				msg.setRecipients(Message.RecipientType.TO, address);
				// 设定信中的主题 
				msg.setSubject(Subject);
				// 设定送信的时间 
				msg.setSentDate(new Date());

				Multipart mp = new MimeMultipart();
				MimeBodyPart mbp = new MimeBodyPart();

				// 设定邮件内容的类型为 text/plain 或 text/html 
				mbp.setContent(messageText, type + ";charset=UTF-8");
				mp.addBodyPart(mbp);
				msg.setContent(mp);

				Transport transport = mailSession.getTransport("smtp");
				////请填入你的邮箱用户名和密码,千万别用我的^_^ 
				transport.connect(mailserver, "cfrisk", "cfrisk123");//设发出邮箱的用户名、密码
				transport.sendMessage(msg, msg.getAllRecipients());
				transport.close();
				//Transport.send(msg);
				out.println("<script language=\"javascript\">alert('邮件已顺利发送！');location.href='/user/login.jsp';</script>");
				out.flush();
				return;
				//out.println("邮件已顺利发送！");
			} catch (MessagingException mex) {
				mex.printStackTrace();
				out.println("<script language=\"javascript\">alert('发送失败："
						+ mex + "');history.back(-1);</script>");
				//out.flush();
				//return;
			}
		%>
	</body>
</html>