<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="checkuser.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>选购课程</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>资料名称</th>
			<th>来源</th>
			<th>作者</th>
			<th>风险类别</th>
			<th>资料类别</th>
			<th>收录时间</th>
			<th>类型</th>
			<th>操作</th>
		</tr>
		<tr>
			<td>麦道夫欺诈案</td>
			<td>天逸</td>
			<td>刘博</td>
			<td>信用风险</td>
			<td>案例分析</td>
			<td>2010年10月</td>
			<td>文本</td>
			<td><a href="dataview.jsp?url=/user/reader/cqj.swf">进入</a></td>
		</tr>
		<tr>
			<td>麦道夫欺诈案</td>
			<td>天逸</td>
			<td>刘博</td>
			<td>信用风险</td>
			<td>案例分析</td>
			<td>2010年10月</td>
			<td>文本</td>
			<td><a href="dataview.jsp?url=/user/reader/Paper3.swf">进入</a></td>
		</tr>
		<tr>
			<td>麦道夫欺诈案</td>
			<td>天逸</td>
			<td>刘博</td>
			<td>信用风险</td>
			<td>案例分析</td>
			<td>2010年10月</td>
			<td>文本</td>
			<td><a href="dataview.jsp">进入</a></td>
		</tr>
		<tr>
			<td colspan="8" style="height:32px" align="center">
				<img src="../skin/images/page_first.gif" />
				<img src="../skin/images/page_prev.gif" />
				1 2 3 4 5 ...
				<img src="../skin/images/page_next.gif" />
				<img src="../skin/images/page_last.gif" />
			</td>
		</tr>
	</table>
</body>
</html>