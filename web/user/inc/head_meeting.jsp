<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript">
function checkKeywords(){
	if($("#SearchText").val().length==0){
		alert("请填写关键字");
		return false;
	}else{
		//$("#sql").val("-and-(k_title-like-'%"+$("#SearchText").val()+"%'-or-k_content-like-'%"+$("#SearchText").val()+"%'-or-k_author-like-'%"+$("#SearchText").val()+"%')");
		$("#sql").val("-and-(k_title-like-'%"+$("#SearchText").val()+"%'-or-k_author-like-'%"+$("#SearchText").val()+"%')");
		$("#searchform").submit();
	}
}
</script>
<div class="header">
	<div class="top">
	<div class="logo">
		<%if(login_email.length()>0){%>
		<img src="/user/skin/images/document/logo.jpg" alt="中国金融风险管理技术交流促进系统" title="中国金融风险管理技术交流促进系统" name="Insert_logo" id="Insert_logo" border="0" />
		<%}else{%>
		<img src="/user/skin/images/document/logo1.jpg" alt="中国金融风险管理技术交流促进系统" title="中国金融风险管理技术交流促进系统" name="Insert_logo" id="Insert_logo" border="0" />
		<%}%>
	</div>
	<div class="search">
		<div class="userInfo">
			<%if(login_email.length()<=0){%>
				<div class="top_menu">
					<p>欢迎您，请先登录</p>
					<p><a href="/index.do"><font color=black>进入网站</font></a></p>
				</div>
            <%}%>
		</div>
		<div class="clearfloat"></div>
	<%if(login_email.length()>0){
			if(auser!=null){%>
			<div class="top_menu">
				<p><a href="/user/opt/userlogout.jsp">退出登录</a></p>
				<p><a href="/index.do">进入网站</a></p>
			</div>
			<div class="clearfloat"></div>
		<%}
	}%>
		</div>
	</div>
	<%if(login_email.length()>0){
			if(auser!=null){%>
	<div class="button">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="165" rowspan="2"><img src="/user/skin/images/document/menu_top.jpg"></td>
				<td align="right">
					<div class="buttonitem">
					<div style="float:left;margin-left:8px;">
			<%if(login_email.length()>0){
            	if(auser!=null){%>
            	欢迎您，<a href="/user/index.jsp"><%=auser.getAcount()%></a> <%
	            	if(auser.getIsfee()>0){
	            		UsertypeDAO utDAO=new UsertypeDAO();
	            		Usertype ut=utDAO.findById(auser.getIsfee());
	            		if(ut!=null){
	            			//out.print(ut.getName());
	            		}else{
	            			out.print("[试用会员]");
	            		}
	            	}else{
	            		out.print("[试用会员]");
	            	}
	            }
            }%>
            		</div>
			<%if(login_email.length()>0){
            		if(auser!=null){%>
			<div style="float:right;height:16px;margin-top:6px;">
				<form id="searchform" style="width:185px;height:16px; margin:0px 0px 0px 32px;" name="searchform" action="/user/inc/meetinglist.jsp" method="post" target="main">
					<input type="hidden" name="sql" id="sql" />
					<div style="float:left;width:171px;height:16px;border-top:1px solid #6C6765;border-right:1px solid #6C6765;background:#E6D7D1">
						 <input name="keywords" id="SearchText" type="text" style="float:left;border:1px solid #E3DDDE;height:12px; color:#333333; font-size:12px;width:100px;" value="" />
						 <input class="search_btn" style="float:right;width:66px;height:16px;border:0;" type="image" src="/user/skin/img/search_meeting.gif" onClick="checkKeywords()" />
					</div>
        		</form>
        	</div>
			<%}
			}%>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right"><div style="width:826px;height:14px;margin-right:2px;border-left:1px solid #BBC5D1;border-right:1px solid #BBC5D1;background:white">
				</div></td>
			</tr>
		</table>
	</div>
		<%}
	}%>
</div>