<%@ page language="java" contentType="text/html; charset=UTF-8" import="com.knife.member.*,org.apache.commons.codec.binary.*"
	pageEncoding="UTF-8"%>
<%
Userinfo auser=null;
String login_email="";
	boolean isok = false;
	String msg = "";
	String mail = "";
	if (request.getParameter("id") != null) {
		mail = request.getParameter("id");
	}
	if (mail.length() > 0) {
		byte[] b = Base64.decodeBase64(mail.getBytes());
		login_email = new String(b);
		UserinfoDAO userDAO = new UserinfoDAO();
		List<Userinfo> users = userDAO.findByEmail(login_email);
		String account = "";
		if (users.size() > 0) {
			auser = users.get(0);
			account = auser.getAcount();
		} else {
			isok = false;
			msg = "无效的邮件地址！";
		}
		if(auser!=null){
			int active=auser.getActive();
			if(active>0){
				isok = false;
				msg = "该帐号已激活！";
			}else{
				try{
					auser.setActive(1);
					userDAO.save(auser);
					isok = true;
					msg = "激活成功！";
				}catch(Exception e){
					isok = false;
					msg = "激活异常！";
				}
			}
			auser=null;
		}
	} else {
		isok = false;
		msg = "无效的邮件地址！";
	}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>邮件激活</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<link rel="stylesheet" href="/user/skin/css/style.css" type="text/css"
			media="all" />
		<style type="text/css">
#loginForm {
	margin: 0px auto;
	width: 1000px;
	padding: 80px 0px;
	text-align: center;
	background: #C7D7E7;
}

#loginform {
	width: 1000px;
}

#loginForm a {
	color: gray;
}

#loginForm input {
	border: 1px solid gray;
}

#saveUserInfo {
	border: none;
}

.inputText input {
	width: 150px;
}
</style>
		<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
	</head>

	<body>
		<div class="container">
			<%@include file="/user/inc/head.jsp"%>
			<div class="clearfloat"></div>
			<div id="loginForm">
				<table width="350" align="center" cellspacing="0" cellpadding="0"
					border="0"
					style="border: 1px solid gray; text-align: center; font-size: 12px; background: white">
					<tr>
						<td
							style="background: #12335F; color: white; border-bottom: 1px solid gray; height: 32px; font-size: 14px;">
							邮件激活
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="inputText">
							<b>
								<%
									if (isok) {
										out.println("激活成功");
									} else {
										out.println("激活失败");
									}
								%>
							</b>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<p align="left"><%=msg%></p>
							<!--  p align="left">
								尊敬的会员，在浏览资料时，需要下载并安装插件，否则无法正常浏览内容，请从
								<a href="/cfrisk.zip">http://www.cfrisk.org/cfrisk.zip</a>
								下载程序到本地，解压并执行cfrisk.vbs安装设置！
								<br />
								如果您没有安装权限，请咨询本单位系统管理员协助安装！
							</p-->
							<p align="center">
								<a href="/user/index.jsp">返回会员专区</a>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td
							style="background: #12335F; color: white; border-top: 1px solid gray">
							&nbsp;
						</td>
					</tr>
				</table>
			</div>
			<%@include file="/user/inc/foot.jsp"%>
		</div>
	</body>
</html>