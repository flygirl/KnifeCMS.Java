<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@ page import="com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News"%>
<%@include file="checkuser.jsp"%>
<%@include file="checkrights.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>视频浏览</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	height:460px;
	overflow:visible;
	background:#DDDDDD;
}
.data_list td{
	text-align:center;
}
a.button{
	width:65px;
	height:18px;
	text-align:center;
	line-height:18px;
	*line-height:22px;
	_line-height:18px;
	text-decoration:none;
	display:block;
	background:url('/user/skin/img/btn_gray.png');
	color:black;
	font-size:13px;
	float:right;
	margin:4px;
}
a.button:visited{color:black}
</style>
<script type="text/javascript" src="/user/reader/swfobject.js"></script>
<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
<script type="text/javascript">
function showComment(){
	$("#comment")[0].style.display="block";
}
function closeComment(){
	$("#comment")[0].style.display="none";
}
var canSave=true;
function submitComment(){
	if($("#c_user").val().length<=0){
		alert("评论用户名不能为空！");
		$("#c_user")[0].focus();
		canSave=false;
	}else{
		canSave=true;	
	}
	if(canSave){
		if($("#c_vcode").val().length<=0){
			alert("评论验证码不能为空！");
			$("#c_vcode")[0].focus();
			canSave=false;
		}else{
			canSave=true;	
		}
	}
	if(canSave){
		if($("#c_comment").val().length<=0){
			alert("评论内容不能为空！");
			$("#c_comment")[0].focus();
			canSave=false;
		}else{
			canSave=true;
		}
	}
	if(canSave){
	$.post("/comment.do",{parameter:"save",user:$("#c_user").val(),vcode:$("#c_vcode").val(),news_id:"<%=id%>",comment:$("#c_comment").val()},function(data){
		alert("提交成功！\n您的意见我们会尽快提交专家讨论，感谢您为中国金融风险管理技术交流和促进机制做出的贡献！");
		$("#c_user").val('');
		$("#c_vcode").val('');
		$("#c_comment").val('');
		reloadImg();
		closeComment();
		//alert(data);
	});
	}
}

function reloadImg() {
   var obj = document.getElementById("vcode");
   var src = obj.src;
   var pos = src.indexOf('?');
   if (pos >= 0) {
      src = src.substr(0, pos);
   }
   var date = new Date();
   obj.src = src + '?v=' + date.getTime();
	 obj1.src = obj.src;
   return false;
}

function addFav(){
	$.get("/user/opt/addfav.jsp?docid=<%=id%>",function(data){
		alert(data);
	});
}

  var flashvars =
      {
        'file':                 '<%=docurl%>',
        'image':                '/player/preview.jpg',
        'id':                   'playerID',
        'autostart':            'true'
      };

      var params =
      {
        'allowfullscreen':      'true',
        'allowscriptaccess':    'always',
        'bgcolor':              '#000000',
        'wmode':				'transparent'
      };

      var attributes =
      {
        'align':                'top',
        'name':                 'playerID',
        'id':                   'playerID'
      };

      swfobject.embedSWF('/user/player/KnifePlayer.swf', 'video', '100%', '435', '9.0.124', '/user/reader/playerProductInstall.swf', flashvars, params, attributes);
</script>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<td height="435">
				<div id="video" style="height:435px;">
        	<p>
	        	To view this page ensure that Adobe Flash Player version 
				10.0.0 or greater is installed. 
			</p>
			<script type="text/javascript"> 
				var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
				document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
								+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
			</script>
        </div>
	   	
       	<noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="435" id="KnifeReader">
                <param name="movie" value="/user/player/KnifePlayer.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="sameDomain" />
                <param name="allowFullScreen" value="true" />
                <param name="flashvars" value="file=<%=docurl%>"/>
                <param name="wmode" value="transparent" />
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="/user/player/KnifePlayer.swf" width="100%" height="435">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="sameDomain" />
                    <param name="allowFullScreen" value="true" />
                    <param name="wmode" value="transparent" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                	<p> 
                		Either scripts and active content are not permitted to run or Adobe Flash Player version
                		10.0.0 or greater is not installed.
                	</p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
	    </noscript>
					<div style="position: relative; width: 98%; height: 0px;">
						<div id="comment"
							style="display: none; position: absolute; width: 430px; right: 0; bottom: 0; z-index: 100; text-align: left; border: 1px solid gray">
							<div
								style="height: 30px; width: 100%; background-image: url(/skin/images/ind_39.jpg); border-bottom: 1px solid #DDDDDD;">
								<span
									style="font-family: '微软雅黑'; font-size: 14px; line-height: 30px; font-weight: bold">
									&nbsp;&nbsp;改进建议</span>
							</div>
							<div style="height: 180px; background-color: #EEEEEE;">
								<table width="100%" height="180" border="0" cellspacing="0"
									cellpadding="0">
									<tr>
										<td>
											<FORM name="form">
												<div
													style="font-size: 13px; height: 24px; line-height: 24px;">
													评论人：
													<input name="user" type="text" id="c_user"
														value="<%=auser.getRealname()%>" size="10" />
													验证码：
													<input name="vcode" type="text" id="c_vcode" size="8" />
													<img src="/valicode" alt="验证码看不清楚？请点击刷新验证码"
														style="cursor: pointer;" onClick="this.src='/valicode';" />
												</div>
												<div style="margin: 4px; text-align: center">
													<textarea id="c_comment" name="comment" cols="60" rows="4"
														style="width: 80%;"></textarea>
												</div>
												<div
													style="margin: 4px; text-align: right; height: 20px; width: 90%;">
													<a href="javascript:submitComment();">[提交]</a>&nbsp;&nbsp;
													<a href="javascript:closeComment();">[关闭]</a>
												</div>
											</FORM>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
			</td>
		</tr>
		<tr>
			<td align="right">
				<a class="button" href="javascript:showComment()">评论</a>
				<a class="button" href="javascript:addFav()">收藏</a>
				<a class="button" href="javascript:history.back(-1)">返回</a>
			</td>
		</tr>
	</table>
<%@include file="/user/count/count.jsp"%>
</body>
</html>