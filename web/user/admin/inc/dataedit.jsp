<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="checkadmin.jsp" %>
<%
	int	id = 0;
if(request.getParameter("id")!=null){
	id				= Integer.parseInt(request.getParameter("id"));
}
	DocumentDAO docDAO=new DocumentDAO();
	Document adoc = docDAO.findById(id);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>资料编辑</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:24px;
	text-indent:24px;
	border-bottom:1px solid gray;
}
.inputText input{border:1px solid gray;}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="/user/opt/editdata.jsp" method="post">
<input type="hidden" name="id" value="<%=adoc.getId()%>" />
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th colspan="2">资料编辑</th>
		</tr>
		<tr>
					<td width="280">资料名称：</td>
					<td class="inputText">
						<input id="title" type="text" name="title" class="validate[required,length[0,20]]" value="<%=adoc.getTitle()%>" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						是否收费：
					</td>
					<td class="inputText">
						<input id="isfree" name="isfee" type="radio" value="0"<%
						if(!adoc.getIsfee()){
							out.println(" checked=\"checked\"");
						}
						%> /><label for="isfree">免费</label>
						<input id="isfee" name="isfee" type="radio" value="1"<%
						if(adoc.getIsfee()){
							out.println(" checked=\"checked\"");
						}
						%> /><label for="isfee">收费</label>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						资料来源：
					</td>
					<td class="inputText">
						<input id="source" type="text" name="source" class="validate[required,length[0,20]]" value="<%=adoc.getSource()%>" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						作者：
					</td>
					<td class="inputText">
						<input id="author" type="text" name="author" class="validate[length[0,20]]" value="<%=adoc.getAuthor()%>" />
					</td>
				</tr>
				<tr>
					<td>
						风险类别：
					</td>
					<td class="inputText">
						<select name="risktype">
							<%
							RisktypeDAO riskDAO = new RisktypeDAO();
							Risktype risk = new Risktype();
							for(Object odoc:riskDAO.findAll()){
								risk=(Risktype)odoc;
								out.print("<option value=\""+risk.getId()+"\"");
								if(adoc.getRisktype().equals(risk.getId())){
									out.print(" selected");
								}
								out.print(">"+risk.getType()+"</option>");
							}
							%>
						</select>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						资料类别：
					</td>
					<td class="inputText">
						<select name="documenttype">
							<%
							DocumenttypeDAO doctDAO = new DocumenttypeDAO();
							Documenttype doct = new Documenttype();
							for(Object odoc:doctDAO.findAll()){
								doct=(Documenttype)odoc;
								out.print("<option value=\""+doct.getId()+"\"");
								if(adoc.getDocumenttype().equals(doct.getId())){
									out.print(" selected");
								}
								out.print(">"+doct.getType()+"</option>");
							}
							%>
						</select>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						文档类型：
					</td>
					<td class="inputText">
						<%=adoc.getFiletype()%>
					</td>
				</tr>
				<tr>
					<td>
						文档地址：
					</td>
					<td class="inputText">
						<%=adoc.getFileurl()%>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="提交" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="重置" />
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			</table>
	</form>
</body>
</html>