<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
int isfee = 0;
if(request.getParameter("isfee")!=null){
	isfee=Integer.parseInt(request.getParameter("isfee"));
}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>添加用户</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:24px;
	text-indent:24px;
	border-bottom:1px solid gray;
}
.inputText input{border:1px solid gray;}
</style>
	<link rel="stylesheet" href="/user/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/js/jquery.validationEngine-cn.js"></script>
	<script type="text/javascript" src="/user/datepicker/WdatePicker.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	
	function hideSubUser(){
		$("#subuser")[0].style.display="none";
	}
	
	function showSubUser(){
		$("#subuser")[0].style.display="block";
	}
	
	function chooseDate(){
		jQuery("#birthday").date_input();
		return false;
	}
	function chooseDate1(){
		jQuery("#sysj").date1_input();
		return false;
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="/user/opt/edituser.jsp" method="post">
<table class="data_list" cellspacing="0" cellpadding="0">
					<tr>
					<td width="280">会员帐号：</td>
					<td class="inputText">
						<input id="acount" type="text" name="acount" class="validate[required,length[0,20]]" value="" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						真实姓名：
					</td>
					<td class="inputText">
						<input id="realname" type="text" name="realname" class="validate[required,length[0,20]]" value="" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						出生日期：
					</td>
					<td class="inputText">
						<input id="birthday" type="text" name="birthday" class="Wdate validate[required,custom[date]]" onClick="WdatePicker()" value="" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						国籍：
					</td>
					<td class="inputText">
						<input type="text" name="country" value="" />
					</td>
				</tr>
				<tr>
					<td>
						性别：
					</td>
					<td>
						<input id="male" name="sex" type="radio" value="1" /><label for="male">男</label>
						<input id="female" name="sex" type="radio" value="0" /><label for="female">女</label>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						教育程度：
					</td>
					<td class="inputText">
						<input type="text" name="education" value="" />
					</td>
				</tr>
				<tr>
					<td>
						身份证或护照号：
					</td>
					<td class="inputText">
						<input type="text" name="idcard" value="" />
					</td>
				</tr>
				<tr>
					<td>
						家庭住址：
					</td>
					<td class="inputText">
						<input type="text" name="homeaddress" value="" />
					</td>
				</tr>
				<tr>
					<td>
						邮编：
					</td>
					<td class="inputText">
						<input type="text" name="zipcode" value="" />
					</td>
				</tr>
				<tr>
					<td>
						现居地区：
					</td>
					<td class="inputText">
						<input type="text" name="homearea" value="" />
					</td>
				</tr>
				<tr>
					<td>
						现工作地区：
					</td>
					<td class="inputText">
						<input type="text" name="workarea" value="" />
					</td>
				</tr>
				<tr>
					<td>
						移动电话：
					</td>
					<td class="inputText">
						<input type="text" name="mobile" value="" />
					</td>
				</tr>
				<tr>
					<td>
						固定电话：
					</td>
					<td class="inputText">
						<input type="text" name="phone" value="" />
					</td>
				</tr>
				<tr>
					<td>
						E-mail：
					</td>
					<td class="inputText">
						<input type="text" name="email" value="" />
					</td>
				</tr>
					<tr>
					<td>
						家庭成员：
					</td>
					<td class="inputText">
						<input type="text" name="family" value="" />
					</td>
				</tr>
				<tr>
					<td>
						共同居住的孩子：
					</td>
					<td class="inputText">
						<input type="text" name="childage" value="" />
					</td>
				</tr>
				<tr>
					<td>
						日常的交通工具：
					</td>
					<td class="inputText">
						<input type="text" name="traffictool" value="" />
					</td>
				</tr>
				<tr>
					<td>
						公司名称：
					</td>
					<td class="inputText">
						<input id="department" type="text" name="department" class="validate[required,length[0,20]]" value="" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						公司地址：
					</td>
					<td class="inputText">
						<input type="text" name="location" value="" />
					</td>
				</tr>
						<tr>
					<td>
						工作性质：
					</td>
					<td class="inputText">
						<input type="text" name="companynature" value="" />
					</td>
				</tr>
					
				<tr>
					<td>
						职务：
					</td>
					<td class="inputText">
						<input type="text" name="organization" value="" />
					</td>
				</tr>
					<tr>
					<td>
						家庭年总收入：
					</td>
					<td class="inputText">
						<input type="text" name="salary" value="" />
					</td>
				</tr>
					<tr>
					<td>
						个人及家庭成员爱好：
					</td>
					<td class="inputText">
						<input type="text" name="hobby" value="" />
					</td>
				</tr>
				<tr>
					<td>
						您购房目的：
					</td>
					<td class="inputText">
						<input type="text" name="buytarget" value="" />
					</td>
				</tr>
				<tr>
					<td>
						如您购房用于居住：
					</td>
					<td class="inputText">
						<input type="text" name="buyuse" value="" />
					</td>
				</tr>
				<tr>
					<td>
						付款方式：
					</td>
					<td class="inputText">
						<input type="text" name="payway" value="" />
					</td>
				</tr>
				<tr>
					<td>
						您选择该物业的关键因素，请选三项：
					</td>
					<td class="inputText">
						<input type="text" name="selectreason" value="" />
					</td>
				</tr>
				<tr>
					<td>
						1-5年内您还会再次购房吗？：
					</td>
					<td class="inputText">
						<input type="text" name="buyagain" value="" />
					</td>
				</tr>
				<tr>
					<td>
						下面哪句话最符合您目前对金融街控股的了解和态度（单选）：
					</td>
					<td class="inputText">
						<input type="text" name="attitude" value="" />
					</td>
				</tr>
				<tr>
					<td>
						加入金客会的原因（可多选）：
					</td>
					<td class="inputText">
						<input type="text" name="joinreason" value="" />
					</td>
				</tr>
				<tr>
					<td>
						会员类型：
					</td>
					<td class="inputText">
						<select name="isfee">
							<option value="0">免费会员</option>
							<%
							UsertypeDAO utDAO=new UsertypeDAO();
							List<Usertype> userTypes = utDAO.findAll();
							for(Usertype ut:userTypes){
							%>
							<option value="<%=ut.getId()%>"<%if(isfee==ut.getId()){out.print(" selected");}%>><%=ut.getName()%></option>
							<%}%>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						管理类型：
					</td>
					<td class="inputText">
						<input onclick="hideSubUser()" id="isadmin1" type="radio" name="isadmin" value="0" checked><label for="isadmin1">普通会员</label>
						<input onclick="showSubUser()" id="isadmin2" type="radio" name="isadmin" value="10"><label for="isadmin2">机构管理员</label>
						<table id="subuser" width="90%" style="border:1px dotted gray;margin:8px;display:none;" width="100%" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td>
									分配会员数：
								</td>
								<td class="inputText">
									<input type="text" name="adminnum" />
								</td>
							</tr>
						</table>
					</td>
				</tr>			
				<tr>
					<td>
						分配会员数：
					</td>
					<td class="inputText">
						<input type="text" name="adminnum" />
					</td>
				</tr>
				<tr>
					<td>
						绑定网卡：
					</td>
					<td class="inputText">
						<input type="text" name="mac" />
					</td>
				</tr>
					<tr>
					<td>
						试用时间
					</td>
					<td class="inputText">
						<input id="sysj" type="text" name="sysj" class="Wdate validate[required,custom[date]]" onClick="WdatePicker()" value="" />
						<span class="mustInput">*</span>
					</td>
				</tr>				
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="添加" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="重置" />
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			</table>
			</form>
</body>
</html>