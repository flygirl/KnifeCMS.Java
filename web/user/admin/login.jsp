<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String login_email="";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>管理登陆</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="skin/css/style.css" type="text/css" media="all" />
	<style type="text/css">
	#loginForm{
		margin:96px auto;
		width:1000px;
		text-align:center;
	}
	#loginform{
		width:1000px;
	}
	#loginForm a{
		color:#DBDBDB;
	}
	#loginForm input{
		border:1px solid gray;
	}
	#saveUserInfo{
		border:0!important;
	}
	.inputText input{
		width:150px;
	}
	</style>
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
	<link rel="stylesheet" href="skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
<!--
	if(self.parent.frames.length!=0){
		self.parent.location=document.location.href;
	}
	$(function(){
		$("#loginform").validationEngine();
	});
	
	function checkForm(){
		$("loginform").submit();
	}
-->
	</script>
  </head>

<body>
	<div class="container">
		<div id="loginForm">
			<form id="loginform" name="loginform" action="/user/opt/adminlogin.jsp" method="post">
			<table width="350" align="center" cellspacing="0" cellpadding="0" border="0" style="border:1px solid gray;text-align:center;font-size:12px;">
				<tr><td colspan="2" style="background:#12335F;color:white;border-bottom:1px solid gray;height:32px;font-size:14px;">资料库管理系统登录</td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td width="250" height="30" class="inputText" style="text-align:right!important">
						管理帐号：<input id="email" type="text" name="email" class="validate[required,length[0,20]]" />
					</td>
					<td width="100">&nbsp;</td>
				</tr>
				<tr>
					<td height="30" class="inputText" style="text-align:right!important">
						密&nbsp;&nbsp;&nbsp;&nbsp;码：<input id="password" type="password" name="password" class="validate[required,length[0,20]]" />
					</td>
					<td align="left">&nbsp;</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2">
						<input name="submit" type="image" src="/user/skin/images/login.gif" style="border:0" onclick="checkForm()" />
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr><td colspan="2" style="background:#12335F;color:white;border-top:1px solid gray">&nbsp;</td></tr>
			</table>
			</form>
		</div>
	</div>
</body>
</html>