<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import="com.knife.news.stat.*"%>
<%@ page import="org.jfree.data.category.DefaultCategoryDataset"%>
<%
DefaultCategoryDataset dataset = (DefaultCategoryDataset)session.getAttribute("dataset");
String fileName = ChartHandle.generateBarChart("访问时段流量统计", session, dataset, new PrintWriter(out));
String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename=" + fileName;
%>
<HTML>
<HEAD>
<TITLE>访问时段流量统计</TITLE>
</HEAD>
<BODY>
<P ALIGN="CENTER">
<img src="<%=graphURL%>" width=500 height=300 border=0>
</P>
</BODY>
</HTML>
