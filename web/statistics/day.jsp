<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import = "java.io.PrintWriter" %>
<%@ page import="com.knife.news.stat.*"%>
<%@ page import="org.jfree.data.category.*"%>
<%@ page import="org.jfree.data.xy.XYDataset"%>
<%
XYDataset dataset = (XYDataset)session.getAttribute("dataset");
//从session中取出数据集
String fileName = ChartHandle.generateZigzagChart("日流量统计", session, dataset, new PrintWriter(out));
//利用数据集生成图形文件，并返回文件名
String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename=" + fileName;
//利用文件名生成文件的全路径，供显示图形用
%>
<HTML>
<HEAD>
<TITLE>日流量统计</TITLE>
</HEAD>
<BODY>
<P ALIGN="CENTER">
<img src="<%=graphURL%>" width=500 height=300 border=0">
</P>
</BODY>
</HTML>
