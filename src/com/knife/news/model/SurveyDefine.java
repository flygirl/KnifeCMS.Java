package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_survey_define", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class SurveyDefine {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;

	@TableField(name = "k_xml")
	private String xml;
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id=id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getXml(){
		return xml;
	}
	
	public void setXml(String xml){
		this.xml=xml;
	}

	public void setSurveyDefine(com.knife.news.object.SurveyDefine s){
		this.id=s.getId();
		this.name=s.getName();
		this.xml=s.getXml();
	}
}