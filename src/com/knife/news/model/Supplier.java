package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_supplier", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Supplier {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_user")
	private String user;
	
	@TableField(name = "k_name")
	private String name;
	
	@TableField(name = "k_sid")
	private String sid;
	
	@TableField(name = "k_sname")
	private String sname;
	
	@TableField(name = "k_card")
	private String card;
	
	@TableField(name = "k_stype")
	private String stype;
	
	@TableField(name = "k_tel")
	private String tel;
	
	@TableField(name = "k_email")
	private String email;
	
	@TableField(name = "k_message")
	private String message;
	
	@TableField(name = "k_discount")
	private String discount;
	
	@TableField(name = "k_license")
	private String license;
	
	@TableField(name = "k_logo")
	private String logo;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}
	
	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public void setSupplier(com.knife.news.object.Supplier s){
		this.id=s.getId();
		this.user=s.getUser();
		this.name=s.getName();
		this.sid=s.getSid();
		this.sname=s.getSname();
		this.card=s.getCard();
		this.stype=s.getStype();
		this.tel=s.getTel();
		this.discount=s.getDiscount();
		this.email=s.getEmail();
		this.license=s.getLicense();
		this.logo=s.getLogo();
		this.message=s.getMessage();
	}
}
