package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;


@Table(tableName = "k_workflow_node", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class FlowNode {
	@TableField(name = "id")
	private String id;
	
	@TableField(name = "k_type")
	private int type;
	
	@TableField(name = "k_name")
	private String name;
	
	@TableField(name = "k_step")
	private int step;
	
	@TableField(name = "k_workflow_id")
	private String workflow_id;
	
	@TableField(name = "k_init_function")
	private String init_function;
	
	@TableField(name = "k_run_function")
	private String run_function;
	
	@TableField(name = "k_trans_function")
	private String trans_function;
	
	@TableField(name = "k_save_function")
	private String save_function;
	
	@TableField(name = "k_prev_node")
	private String prev_node;
	
	@TableField(name = "k_next_node")
	private String next_node;
	
	@TableField(name = "k_executor")
	private String executor;
	
	@TableField(name = "k_execute_type")
	private int execute_type;
	
	@TableField(name = "k_remind")
	private int remind;
	
	@TableField(name = "k_field")
	private String field;
	
	@TableField(name = "k_max_day")
	private int max_day;
	
	@TableField(name = "k_status")
	private int status;
	
	public String getId(){
		return id ;
	}
	
	public void setId(String id){
		this.id = id ;
	}
	
	public int getType(){
		return type ;
	}
	
	public void setType(int type){
		this.type = type ;
	}
	
	public String getName(){
		return name ;
	}
	
	public void setName(String name){
		this.name = name ;
	}
	
	public int getStep(){
		return step ;
	}
	
	public void setStep(int step){
		this.step = step ;
	}
	
	public String getWorkflow_id(){
		return workflow_id;
	}
	
	public void setWorkflow_id(String workflow_id){
		this.workflow_id = workflow_id ;
	}
	
	public String getInit_function(){
		return init_function ;
	}
	
	public void setInit_function(String init_function){
		this.init_function = init_function ;
	}
	
	public String getRun_function(){
		return run_function ;
	}

	public void setRun_function(String run_function){
		this.run_function = run_function ;
	}
	
	public String getTrans_function(){
		return trans_function ;
	}
	
	public void setTrans_function(String trans_function){
		this.trans_function = trans_function;
	}
	
	public String getSave_function(){
		return save_function ;
	}
	
	public void setSave_function(String save_function){
		this.save_function = save_function ;
	}
	
	public String getPrev_node(){
		return prev_node ;
	}
	
	public void setPrev_node(String prev_node){
		this.prev_node = prev_node ;
	}
	
	public String getNext_node(){
		return next_node ;
	}
	
	public void setNext_node(String next_node){
		this.next_node = next_node ;
	}
	
	public String getExecutor(){
		return executor ;
	}
	
	public void setExecutor(String executor){
		this.executor = executor ;
	}
	
	public int getExecute_type(){
		return execute_type ;
	}
	
	public void setExecute_type(int execute_type){
		this.execute_type = execute_type ;
	}
	
	public int getRemind(){
		return remind ;
	}
	
	public void setRemind(int remind){
		this.remind = remind ;
	}
	
	public String getField(){
		return field ;
	}
	
	public void setField(String field){
		this.field = field ;
	}
	
	public int getMax_day(){
		return max_day ;
	}
	
	public void setMax_day(int max_day){
		this.max_day = max_day ;
	}
	
	public int getStatus(){
		return status;
	}
	
	public void setStatus(int status){
		this.status = status ;
	}
	
	public void setFlowNode(com.knife.news.object.FlowNode flowNode){
		this.id = flowNode.getId();
		this.name = flowNode.getName();
		this.type = flowNode.getType();
		this.workflow_id = flowNode.getWorkflow_id();
		this.init_function = flowNode.getInit_function();
		this.run_function = flowNode.getRun_function();
		this.trans_function = flowNode.getTrans_function();
		this.save_function = flowNode.getSave_function();
		this.step = flowNode.getStep();
		this.prev_node = flowNode.getPrev_node();
		this.next_node = flowNode.getNext_node();
		this.executor = flowNode.getExecutor();
		this.execute_type = flowNode.getExecute_type();
		this.remind = flowNode.getRemind();
		this.field = flowNode.getField();
		this.max_day = flowNode.getMax_day();
		this.status = flowNode.getStatus();
	}
}
