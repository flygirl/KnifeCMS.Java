package com.knife.news.model;


import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_thumbnail", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Thumbnail {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_url")
	private String url;

	@TableField(name = "k_link")
	private String link;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	

}
