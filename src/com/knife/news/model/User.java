package com.knife.news.model;

import java.io.Serializable;
import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_user", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9059438892115588465L;

	@TableField(name = "id")
	private String id;

	@TableField(name = "k_username")
	private String username;

	@TableField(name = "k_password")
	private String password;

	@TableField(name = "k_email")
	private String email;

	@TableField(name = "k_qq")
	private String qq;

	@TableField(name = "k_tel")
	private String tel;

	@TableField(name = "k_birthday")
	private Date birthday;
	
	@TableField(name = "k_sysj")
	private Date sysj;

	@TableField(name = "k_fax")
	private String fax;

	@TableField(name = "k_addr")
	private String addr;

	@TableField(name = "k_intro")
	private String intro;

	@TableField(name = "k_popedom")
	private int popedom;

	@TableField(name = "k_document_show")
	private int document_show;
	
	public int getDocument_show() {
		return document_show;
	}

	public void setDocument_show(int document_show) {
		this.document_show = document_show;
	}

	public int getPopedom() {
		return popedom;
	}

	public void setPopedom(int popedom) {
		this.popedom = popedom;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public Date getSysj() {
		return sysj;
	}

	public void setSysj(Date sysj) {
		this.sysj = sysj;
	}
	
    public void setUser(com.knife.news.object.User u){
		this.id=u.getId();
		this.username=u.getUsername();
		this.password=u.getPassword();
		this.email=u.getEmail();
		this.qq=u.getQq();
		this.tel=u.getTel();
		this.birthday=u.getBirthday();
		this.sysj=u.getSysj();
		this.fax=u.getFax();
		this.addr=u.getAddr();
		this.intro=u.getIntro();
		this.popedom=u.getPopedom();
		this.document_show=u.getDocument_show();
    }
}
