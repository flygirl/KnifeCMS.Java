package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_tree", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Tree {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;
	
	@TableField(name = "k_tree_type")
	private String tree_type;
	
	@TableField(name = "k_show")
	private int show;
	
	@TableField(name = "k_value")
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getTree_type() {
		return tree_type;
	}

	public void setTree_type(String tree_type) {
		this.tree_type = tree_type;
	}
	
	public int getShow() {
		return show;
	}

	public void setShow(int show) {
		this.show = show;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
