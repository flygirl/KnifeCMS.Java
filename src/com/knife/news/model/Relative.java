package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_relative", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Relative {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_news_id")
	private String news_id;
	
	@TableField(name = "k_rels")
	private String rels;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNews_id() {
		return news_id;
	}

	public void setNews_id(String news_id) {
		this.news_id = news_id;
	}
	
	public String getRels() {
		return rels;
	}

	public void setRels(String rels) {
		this.rels = rels;
	}
	
	public void setRelative(com.knife.news.object.Relative rel) {
		this.id = rel.getId();
		this.news_id = rel.getNews_id();
		this.rels = rel.getRels();
	}
}
