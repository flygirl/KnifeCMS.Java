package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_news", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class News {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_title")
	private String title;
	
	@TableField(name = "k_author")
	private String author;
	
	@TableField(name = "k_source")
	private String source;

	@TableField(name = "k_keywords")
	private String keywords;
	
	@TableField(name = "k_file")
	private String file;

	@TableField(name = "k_content")
	private String content;

	@TableField(name = "k_date")
	private Date date;

	@TableField(name = "k_username")
	private String username;

	@TableField(name = "k_type")
	private String type;
	
	@TableField(name = "k_treenews")
	private String treenews;

	@TableField(name = "k_display")
	private int display;
	
    @TableField(name="k_commend")
    private String commend;
    
    @TableField(name="k_template")
    private String template;
    
    @TableField(name="k_order")
    private int order;

    @TableField(name="k_link")
    private String link;  
    
    @TableField(name="k_tofront")
    private int	 tofront;
    
    @TableField(name="k_count")
    private int count;
    
    @TableField(name="k_docfile")
	private String docfile;
    
    @TableField(name="k_isfee")
	private int isfee;
    
    @TableField(name="k_rights")
	private int rights;
    
    @TableField(name="k_type2")
	private String type2;
    
    @TableField(name="k_doctype")
	private String doctype;
    
    @TableField(name="k_summary")
	private String summary;
    
    @TableField(name="k_score")
	private int score;
    
	@TableField(name = "k_search")
	private int search;
	
	@TableField(name = "k_ispub")
	private int ispub;
	
	@TableField(name = "k_offdate")
	private Date offdate;
	
	@TableField(name = "k_xmldata")
	private String xmldata;
	
	@TableField(name = "k_flowusers")
	private String flowusers;
    
	
	/**
	 * @return the flowusers
	 */
	public String getFlowusers() {
		return flowusers;
	}

	/**
	 * @param flowusers the flowusers to set
	 */
	public void setFlowusers(String flowusers) {
		this.flowusers = flowusers;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}

	public String getKeywords() {
		return keywords;
	}
	
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
    
	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getTreenews() {
		return treenews;
	}

	public void setTreenews(String treenews) {
		this.treenews = treenews;
	}

	public String getCommend() {
		return commend;
	}

	public void setCommend(String commend) {
		this.commend = commend;
	}
	
	public int getOrder(){
		return order;
	}
	
	public void setOrder(int order){
		this.order=order;
	}
	
	public String getLink(){
		return link;
	}
	
	public void setLink(String link){
		this.link=link;
	}
	
	public int getTofront() {
		return tofront;
	}

	public void setTofront(int tofront) {
		this.tofront = tofront;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDocfile(){
		return docfile;
	}

	public void setDocfile(String docfile){
		this.docfile = docfile;
	}
	
	public int getIsfee() {
		return isfee;
	}

	public void setIsfee(int isfee) {
		this.isfee = isfee;
	}
	
	public int getRights() {
		return rights;
	}

	public void setRights(int rights) {
		this.rights = rights;
	}
	
	public String getType2(){
		return type2;
	}
	
	public void setType2(String type2) {
		this.type2 = type2;
	}
	
	public String getDoctype(){
		return doctype;
	}
	
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}
	
	public String getSummary(){
		return summary;
	}
	
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public int getScore(){
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getSearch(){
		return search;
	}
	
	public void setSearch(int search) {
		this.search = search;
	}
	
	public int getIspub() {
		return ispub;
	}

	public void setIspub(int ispub) {
		this.ispub = ispub;
	}

	public Date getOffdate() {
		return offdate;
	}

	public void setOffdate(Date offdate) {
		this.offdate = offdate;
	}
	
	public String getXmldata(){
		return xmldata;
	}
	
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	public void setNews(com.knife.news.object.News news){
		this.id=news.getId();
    	this.title=news.getTitle();
    	this.author=news.getAuthor();
    	this.source=news.getSource();
    	this.keywords=news.getKeywords();
    	this.file=news.getFile();
    	this.content=news.getContent();
    	this.date = news.getDate();
    	this.username=news.getUsername();
    	this.type=news.getType();
    	this.treenews=news.getTreenews();
    	this.display=news.getDisplay();
    	this.commend=news.getCommend();
    	this.template=news.getTemplate();
    	this.order=news.getOrder();
    	this.link=news.getLink();
    	this.tofront = news.getTofront();
    	this.count = news.getCount();
    	this.docfile=news.getDocfile();
    	this.isfee=news.getIsfee();
    	this.rights=news.getRights();
    	this.type2=news.getType2();
    	this.doctype=news.getDoctype();
    	this.summary=news.getSummary();
    	this.score=news.getScore();
    	this.search=news.getSearch();
    	this.ispub=news.getIspub();
    	this.offdate=news.getOffdate();
    	this.xmldata=news.getXmldata();
    	this.flowusers = news.getFlowusers();
	}
}
