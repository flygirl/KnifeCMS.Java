package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_subscribe", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Subscribe {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;

	@TableField(name = "k_form")
	private String form;

	@TableField(name = "k_user")
	private String user;
	
	@TableField(name = "k_stype")
	private String stype;
	
	@TableField(name = "k_mail")
	private String mail;
	
	@TableField(name = "k_phone")
	private String phone;
	
	@TableField(name = "k_identitycard")
	private String identitycard;
	
	@TableField(name = "k_address")
	private String address;
	
	@TableField(name = "k_zipcode")
	private String zipcode;

	public String getIdentitycard() {
		return identitycard;
	}

	public void setIdentitycard(String identitycard) {
		this.identitycard = identitycard;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
