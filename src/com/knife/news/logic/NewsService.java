package com.knife.news.logic;

import java.util.Collection;
import java.util.List;
import com.knife.news.object.News;

/**
 * 文章接口
 * 
 * @author Knife
 * 
 */
public interface NewsService {
	/**
	 * 添加文章
	 * 
	 * @param news
	 *            文章对象
	 * @return 新文章编号，若失败则返回""
	 */
	String addNews(News news);
	
	/**
	 * 保存文章
	 * 
	 * @param news
	 *            文章对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveNews(News news);

	/**
	 * 更新文章
	 * 
	 * @param news
	 *            文章对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateNews(News news);

	/**
	 * 删除文章
	 * 
	 * @param news
	 *            文章对象
	 * @return true删除成功，false删除失败
	 */
	boolean delNews(News news);

	/**
	 * 删除分类下文章
	 * 
	 * @param typeid
	 *            分类编号
	 */
	void delNewsByType(String typeid);

	/**
	 * 按编号取得文章
	 * 
	 * @param id
	 *            文章编号
	 * @return 文章对象
	 */
	News getNewsById(String id);

	/**
	 * 取得分类下的文章列表
	 * 
	 * @param typeid
	 *            分类编号
	 * @return 文章列表
	 */
	List<News> getNewsByType(String typeid);

	/**
	 * 按条件查询文章列表
	 * 
	 * @param sql
	 *            查询语句
	 * @param paras
	 *            查询参数
	 * @return 文章列表
	 */
	List<News> getNews(String sql,
			Collection<Object> paras);

	/**
	 * 按条件查询文章列表
	 * 
	 * @param sql 查询语句
	 * @return 文章列表
	 */
	List<News> getNews(String sql);

	/**
	 * 取得一定范围内的文章列表
	 * @param begin 起始序号
	 * @param max 最大值
	 * @return 文章列表
	 */
	List<News> getSomeNews(int begin, int max);
	
	/**
	 * 取得引用文章列表
	 * @param id 被引用文章ID
	 * @return 文章列表
	 */
	List<News> getLinkedNews(String id);

	/**
	 * 按条件查询文章列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始序号
	 * @param end 结束序号
	 * @return 文章列表
	 */
	List<News> getNewsBySql(String sql,
			Collection<Object> paras, int begin, int end);
	
	/**
	 * 取得所有首页文章
	 * 首页文章-在CMS后台勾选了“推送到首页”
	 * @param sid site编号
	 * @return 文章列表
	 */
	List<News> getIndexNews(String sid);
	
	/**
	 * 取得分类下的首页文章
	 * @param cid 分类编号
	 * @return 文章列表
	 */
	List<News> getIndexNewsByTypeID(String cid);

	/**
	 * 取得分类下指定数目的首页文章
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @return 文章列表
	 */
	List<News> getIndexNewsByTypeID(String cid, int num);

	/**
	 * 取得分类下的首页文章(包含子分类)
	 * @param cid 分类编号
	 * @return 文章列表
	 */
	List<News> getAllIndexNewsByTypeID(String cid);

	/**
	 * 取得分类下指定数目的首页文章(包含子分类)
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @return 文章列表
	 */
	List<News> getAllIndexNewsByTypeID(String cid, int num);

	/**
	 * 取得所有推荐文章
	 * 推荐文章-在CMS后台勾选了“推荐”
	 * @param sid site编号
	 * @return 文章列表
	 */
	List<News> getCommendNews(String sid);

	/**
	 * 取得分类下指定数目的推荐文章
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @return 文章列表
	 */
	List<News> getCommendNews(String cid, int num);
	
	/**
	 * 取得分类下的头条文章
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @return 文章列表
	 */
	List<News> getTopNews(String cid,int num);
	
	/**
	 * 取得分类下的头条文章
	 * @param cid 分类编号
	 * @param treenews 树文章编号
	 * @param num 文章数目
	 * @return 文章列表
	 */
	List<News> getTopNews(String cid, String treenews,int num);

	/**
	 * 按顺序取得分类下的头条文章
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @param orderby 排序字段,可用的字段有：date(发布日期),username(文章作者),source(文章来源),count(访问量)
	 * @return 文章列表
	 */
	List<News> getTopNews(String cid,int num,String orderby);
	
	/**
	 * 取得分类下的头条文章(包含子分类)
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @return 文章列表
	 */
	List<News> getAllTopNews(String cid,int num);
	
	/**
	 * 按顺序取得分类下的头条文章(包含子分类)
	 * @param cid 分类编号
	 * @param num 文章数目
	 * @param orderby 排序字段(同getTopNews的排序字段)
	 * @return 文章列表
	 */
	List<News> getAllTopNews(String cid,int num,String orderby);

	/**
	 * 按字段关键字搜索分类下的文章
	 * @param cid 分类编号
	 * @param fname 字段名称
	 * @param fvalue 字段值
	 * @return 文章列表
	 */
	List<News> getFieldsList(String cid, String fname,
			String fvalue);
	
	/**
	 * 取得文章总数
	 * @param display 是否显示
	 * @param username 用户名
	 * @param cid 分类编号
	 * @param treenews 树文章编号
	 * @param startDate 起始时间
	 * @param endDate 结束时间
	 * @return 文章数
	 */
	int getSum(int display,String username,String cid,String treenews,String startDate,String endDate);
	
	/**
	 * 取得文章总数
	 * @param display 是否显示
	 * @param username 用户名
	 * @param sid 站点编号
	 * @param cid 频道编号
	 * @param treenews 树文章编号
	 * @param startDate 起始时间
	 * @param endDate 结束时间
	 * @return 文章数
	 */
	int getSum(int display,String username,String sid,String cid,String treenews,String startDate,String endDate);
	
	/**
	 * 取得文章总数
	 * @param display 是否显示
	 * @param username 用户名
	 * @param sid 站点编号
	 * @param cid 频道编号
	 * @param treenews 树文章编号
	 * @param startDate 起始时间
	 * @param endDate 结束时间
	 * @return 文章数
	 */
	int getSum(int display,String username,String sid,String cid,String treenews,String startDate,String endDate,int ispub);

	/**
	 * 取得文章总数(包含子分类)
	 * @param display 是否显示
	 * @param username 用户名
	 * @param cid 分类编号
	 * @param treenews 树文章编号
	 * @param startDate 起始时间
	 * @param endDate 结束时间
	 * @return 文章数
	 */
	int getAllSum(int display,String username,String cid,String treenews,String startDate,String endDate);

	/**
	 * 取得站点文章总数
	 * @param display 是否显示
	 * @param sid 站点编号
	 * @return 文章数
	 */
	int getSiteSum(int display,String sid,String startDate,String endDate);
	
	/**
	 * 更新分类顺序
	 * @param typeid 分类编号
	 * @param oldOrder 旧顺序编号
	 * @param newOrder 新顺序编号
	 */
	void reArrange(String typeid, int oldOrder, int newOrder);
	
	//void reArrange(String typeid, int oldOrder, int newOrder);
}
