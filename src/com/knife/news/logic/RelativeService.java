package com.knife.news.logic;

import java.util.List;

import com.knife.news.object.News;
import com.knife.news.object.Relative;

/**
 * 相关文章接口
 * 
 * @author Knife
 * 
 */
public interface RelativeService {

	/**
	 * 保存相关文章对象
	 * @param rel 相关文章对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveRelative(Relative rel);

	/**
	 * 更新相关文章对象
	 * @param rel 相关文章对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateRelative(Relative rel);

	/**
	 * 删除相关文章对象
	 * @param rel 相关文章对象
	 * @return true删除成功，false删除失败
	 */
	boolean delRelative(Relative rel);
	
	/**
	 * 取得相关文章对象
	 * @param id 相关文章对象编号
	 * @return 相关文章对象
	 */
	Relative getRelativeById(String id);
	
	/**
	 * 取得相关文章对象
	 * @param newsid 文章编号
	 * @return 相关文章对象
	 */
	Relative getRelativeByNewsId(String newsid);
	
	/**
	 * 取得相关文章列表
	 * @param newsid 文章编号
	 * @return 文章列表
	 */
	List<News> getRelatedNewsById(String newsid);
}
