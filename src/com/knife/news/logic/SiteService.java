package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Site;

/**
 * 站点接口
 * @author Knife
 *
 */
public interface SiteService {

	/**
	 * 取得站点
	 * @return 站点对象
	 */
	Site getSite();
	
	/**
	 * 按编号取得站点
	 * @param siteid 站点编号
	 * @return 站点对象
	 */
	Site getSiteById(String siteid);
	
	/**
	 * 按编号取得站点
	 * @param template 站点编号
	 * @return 站点对象
	 */
	Site getSiteByTemplate(String template);
	
	/**
	 * 保存站点
	 * @param site 站点对象
	 * @return 新站点编号，若失败则返回""
	 */
	String saveSite(Site site);
	
	/**
	 * 更新站点
	 * @param site 站点对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateSite(Site site);
	
	/**
	 * 删除站点
	 * @param site 站点对象
	 * @return true删除成功，false删除失败
	 */
	boolean delSite(Site site);
	
	/**
	 * 取得所有站点
	 * @return 站点列表
	 */
	List<Site> getAllSites();
	
	/**
	 * 按条件取得站点列表
	 * @param sql 查询条件
	 * @return 站点列表
	 */
	List<Site> getSitesBySql(String sql);
	
	/**
	 * 按条件取得站点列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始位置
	 * @param end 结束位置
	 * @return 站点列表
	 */
	List<Site> getSitesBySql(String sql, Collection<Object> paras, int begin, int end);
}
