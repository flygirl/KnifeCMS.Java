package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.object.FlowNode;
import com.knife.news.logic.FlowNodeService;

public class FlowNodeServiceImpl extends DAOSupportService implements FlowNodeService {
	private static FlowNodeServiceImpl flowNodeDao = new FlowNodeServiceImpl();
	
	public static FlowNodeServiceImpl getInstance() {
		return flowNodeDao;
	}
	
	
	public boolean delFlowNode(FlowNode flowNode) {
		// TODO Auto-generated method stub
		com.knife.news.model.FlowNode aflowNode = new com.knife.news.model.FlowNode();
		aflowNode.setFlowNode(flowNode);
		return this.dao.del(aflowNode);
	}

	public List<FlowNode> getAllFlowNode() {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.FlowNode.class, "id!=''"));
		
	}

	
	public List<FlowNode> getFlowNodeByFlowId(String flowId) {
		// TODO Auto-generated method stub
		try{
			Collection<Object> paras = new ArrayList<Object>();
			if(flowId.length()>0){
				
				paras.add(flowId);
			
				return	this.getFlowNodeBySql("k_workflow_id=? order by k_step desc",paras,0,-1);
			}
			return null;
		}catch(Exception e){
			return null;
		}
		
	}

	
	public FlowNode getFlowNodeById(String id) {
		// TODO Auto-generated method stub
		try{
			com.knife.news.model.FlowNode aflowNode = (com.knife.news.model.FlowNode) this.dao.get(com.knife.news.model.FlowNode.class, id);
			FlowNode flowNode = new FlowNode(aflowNode);
			return flowNode;
		}catch(Exception e ){
			return null;
		}
	}

	
	public List<FlowNode> getFlowNodeBySql(String sql,
			Collection<Object> paras, int begin, int end) {
		// TODO Auto-generated method stub
		
		return changeListType(this.dao.query(com.knife.news.model.FlowNode.class , sql , paras , begin , end));
	}

	
	public boolean saveFlowNode(FlowNode flowNode) {
		// TODO Auto-generated method stub
		com.knife.news.model.FlowNode aflowNode = new com.knife.news.model.FlowNode();
		flowNode.setId(aflowNode.getId());
		aflowNode.setFlowNode(flowNode);
		return this.dao.save(aflowNode);
	}

	
	public boolean updateFlowNode(FlowNode flowNode) {
		// TODO Auto-generated method stub
		com.knife.news.model.FlowNode aflowNode = new com.knife.news.model.FlowNode();
		aflowNode.setFlowNode(flowNode);
		return this.dao.update(aflowNode);
	}

	public List<FlowNode> changeListType(List<Object> objs){
		List<FlowNode> reasults = new ArrayList();
		if(objs != null){
			for(Object newobj : objs){
				com.knife.news.model.FlowNode mobj = (com.knife.news.model.FlowNode)newobj;
				FlowNode  oobj = new FlowNode(mobj);
				reasults.add(oobj);
			}
		}
		return reasults;
	}

	
	public FlowNode getNextFlowNode(FlowNode flowNode) {
		// TODO Auto-generated method stub
		String nextNodeId = flowNode.getNext_node();
		if(nextNodeId == ""||nextNodeId==null){
			return null;
		}else{
			return flowNodeDao.getFlowNodeById(nextNodeId);
		}
	}

	
	public FlowNode getPrevFlowNode(FlowNode flowNode) {
		// TODO Auto-generated method stub
		String prevNodeId = flowNode.getPrev_node();
		if(prevNodeId == ""||prevNodeId==null){
			return null;
		}else{
			return flowNodeDao.getFlowNodeById(prevNodeId);
		}
	}
}
