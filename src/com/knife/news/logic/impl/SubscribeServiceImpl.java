package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.SubscribeService;
import com.knife.news.model.Subscribe;

public class SubscribeServiceImpl extends DAOSupportService implements SubscribeService {
	private static SubscribeServiceImpl ssDAO = new SubscribeServiceImpl();

	public static SubscribeServiceImpl getInstance() {
		return ssDAO;
	}

	public Subscribe getSubscribe(){
		return (Subscribe)this.dao.uniqueResult("select * from k_subscribe");
	}
	
	public List<Subscribe> getAllSubscribes(){
		return changeListType(this.dao.query(Subscribe.class, "id!=''"));
	}

	public List<Subscribe> getSubscribesBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(Subscribe.class, sql));
	}

	public List<Subscribe> getSubscribesBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(Subscribe.class, sql, paras, begin, end));
	}

	public Subscribe getSubscribeById(String id) {
		return (Subscribe)this.dao.get(Subscribe.class, id);
	}
	
	public Subscribe getSubscribeByMail(String mail) {
		List<Subscribe> ss= changeListType(this.dao.query(Subscribe.class,"k_mail='"+mail+"'"));
		Subscribe s=null;
		if(ss.size()>0){
			s=ss.get(0);
		}
		return s;
	}

	public boolean saveSubscribe(Subscribe form) {
		return this.dao.save(form);
	}

	public boolean updateSubscribe(Subscribe form) {
		return this.dao.update(form);
	}

	public boolean delSubscribe(Subscribe form) {
		return this.dao.del(form);
	}
	
	public List<Subscribe> changeListType(List<Object> news){
		List<Subscribe> results = new ArrayList<Subscribe>();
		if(news!=null){
			for (Object newobj : news) {
				Subscribe bnews = (Subscribe) newobj;
				results.add(bnews);
			}
		}
		return results;	
	}
}