package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.VideoService;
import com.knife.news.object.Video;

public class VideoServiceImpl extends DAOSupportService implements VideoService {
	private static VideoServiceImpl videoDAO = new VideoServiceImpl();

	public static VideoServiceImpl getInstance() {
		return videoDAO;
	}

	public List<Video> getAllVideos(){
		return changeListType(this.dao.query(com.knife.news.model.Video.class, "id!=''"));
	}
	
	public List<Video> getVideosByType(String typeid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(typeid);
		return changeListType(this.dao.query(com.knife.news.model.Video.class, "k_newsid in(select id from k_news where k_type=?)",paras));
	}
	
	public List<Video> getVideosByTree(String treeid){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(treeid);
		return changeListType(this.dao.query(com.knife.news.model.Video.class, "k_newsid in(select id from k_news where k_type in(select id from k_type where k_tree_id=?))",paras));
	}

	public List<Video> getVideosByNewsId(String newsid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(newsid);
		return changeListType(this.dao.query(com.knife.news.model.Video.class, "k_newsid=? order by length(k_order) desc,k_order desc",paras));
	}
	
	public List<Video> getVideosByNewsId(String newsid,String orderby) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(newsid);
		return changeListType(this.dao.query(com.knife.news.model.Video.class, "k_newsid=? order by "+orderby,paras));
	}

	public List<Video> getVideosBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.Video.class, sql));
	}

	public List<Video> getVideosBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Video.class, sql, paras, begin, end));
	}

	public Video getVideoById(String id) {
		com.knife.news.model.Video apic=(com.knife.news.model.Video)this.dao.get(com.knife.news.model.Video.class, id);
		return new Video(apic);
		
	}
	
	public List<Video> getVideosByUrl(String url){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(url);
		return changeListType(this.dao.query(com.knife.news.model.Video.class, "k_url=?",paras));
	}

	public boolean saveVideo(Video pic) {
		com.knife.news.model.Video bpic=new com.knife.news.model.Video();
		bpic.setVideo(pic);
		return this.dao.save(bpic);
	}

	public boolean updateVideo(Video pic) {
		com.knife.news.model.Video bpic=new com.knife.news.model.Video();
		bpic.setVideo(pic);
		return this.dao.update(bpic);
	}

	public boolean delVideo(Video pic) {
		com.knife.news.model.Video bpic=new com.knife.news.model.Video();
		bpic.setVideo(pic);
		return this.dao.del(bpic);
	}
	
	public List<com.knife.news.object.Video> changeListType(List<Object> news){
		List<Video> results = new ArrayList<Video>();
		if(news!=null){
			for (Object newobj : news) {
				com.knife.news.model.Video bnews = (com.knife.news.model.Video) newobj;
				Video apic=new Video(bnews);
				results.add(apic);
			}
		}
		return results;	
	}
}