package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Form;

/**
 * 表单接口
 * @author Knife
 *
 */
public interface FormService {

	/**
	 * 取得表单
	 * @return 表单对象
	 */
	Form getForm();
	
	/**
	 * 取得所有表单列表
	 * @return 表单列表
	 */
	List<Form> getAllForms();
	
	/**
	 * 按条件取得表单列表
	 * @param sql 查询语句
	 * @return 表单列表
	 */
	List<Form> getFormsBySql(String sql);
	
	/**
	 * 按条件取得指定范围内的表单列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 表单列表
	 */
	List<Form> getFormsBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按编号取得编号
	 * @param id 表单编号
	 * @return 表单对象
	 */
	Form getFormById(String id);
	
	/**
	 * 保存表单
	 * @param form 表单对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveForm(Form form);
	
	/**
	 * 更新表单
	 * @param form 表单对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateForm(Form form);
	
	/**
	 * 删除表单
	 * @param form 表单对象
	 * @return true删除成功，false删除失败
	 */
	boolean delForm(Form form);
}
