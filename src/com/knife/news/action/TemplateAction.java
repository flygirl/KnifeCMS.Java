package com.knife.news.action;

import java.io.File;

import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class TemplateAction extends BaseCmdAction{

	public Page doDesignEdit(WebForm form, Module module) {
		String pdir = CommUtil.null2String(form.get("sid"));
		String nowdir = CommUtil.null2String(form.get("id"));
		String name = CommUtil.null2String(form.get("name"));
		String templateFile = "";
		if(pdir.length()>0){
			templateFile = templateFile + pdir.replace("_spt_", File.separator) + File.separator;
			form.addResult("sid", pdir);
			String siteTemplate=pdir;
			if(pdir.indexOf("_spt_")>0){
				siteTemplate=pdir.substring(0,pdir.indexOf("_spt_")-1);
			}
			thisSite = siteDAO.getSiteByTemplate(siteTemplate);
			form.addResult("thisSite", thisSite);
		}
		if(nowdir.length()>0){
			templateFile = templateFile + nowdir + File.separator;
			form.addResult("tid", nowdir);
		}
		//String pdir = CommUtil.null2String(form.get("sid"));
		//String name = CommUtil.null2String(form.get("name"));
		String visualDesign = "<link href=\"/include/manage/ui/portal.css\" type=\"text/css\" rel=\"stylesheet\" />"+
		"<script type=\"text/javascript\" src=\"/include/manage/js/jquery.js\"></script>"+
		"<script type=\"text/javascript\" src=\"/include/manage/js/jquery-ui.js\"></script>"+
		"<script type=\"text/javascript\" src=\"/include/manage/js/portal.js\"></script>"+
		"<script type=\"text/javascript\">"+
		"tName='"+name+"';"+
		"siteid='"+pdir+"';"+
		"typeid='"+nowdir+"';"+
		"</script>";
		form.addResult("visualDesign", visualDesign);
		form.addResult("name", name);
		//return module.findPage("design");
		return new Page("design", name , "html" , "/web/" + thisSite.getTemplate()+"/");
	}

	@Override
	public Page doInit(WebForm arg0, Module arg1) {
		// TODO Auto-generated method stub
		return null;
	}
}
