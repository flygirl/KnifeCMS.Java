package com.knife.news.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.member.FindPoint;
import com.knife.member.Room;
import com.knife.member.RoomDAO;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class RoomAction extends BaseCmdAction {

	@Override
	public Page doInit(WebForm form, Module module) {
		return doSummary(form,module);
	}

	public Page doSummary(WebForm form, Module module){
		
		return new Page("score", "user_score.htm","html","/web/" + thisSite.getTemplate() + "/");
	}
	
	public Page doList(WebForm form, Module module){
		Integer userid = 0;
		if(onlineUser==null){
			return new Page("index", "index.htm","html","/web/" + thisSite.getTemplate() + "/");
		}
		String cstguid = onlineUser.getCstguid();
		userid = Integer.parseInt(onlineUser.getId().toString());
		List<Room> roomList = new ArrayList<Room>();
		roomList = FindPoint.findRoomByCstGUID(cstguid);
		form.addResult("roomList",roomList);
		
		return new Page("room", "user_room.htm","html","/web/" + thisSite.getTemplate() + "/");
	}
}
