package com.knife.news.action;

import org.jasig.cas.client.authentication.AttributePrincipal;

import auth.persistent.UserLicenseMgr;

import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.model.Constants;
import com.knife.news.object.User;
import com.knife.tools.WebXML;
import com.knife.util.CommUtil;
import com.knife.web.ActionContext;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;
import com.knife.web.tools.AbstractCmdAction;

public class LoginAction extends AbstractCmdAction {
	UserServiceImpl userdao = UserServiceImpl.getInstance();
	String sso_username="";
	User user = (User) ActionContext.getContext().getSession().getAttribute(
			Constants.SESSION_USER);
	int popedom;
	
	@Override
	public Page doInit(WebForm form, Module module) {
		//如果配置了CAS单点登陆已传回了session则不作验证
		try{
			//AttributePrincipal principal = (AttributePrincipal)request.getUserPrincipal();
			//AttributePrincipal principal = (AttributePrincipal) ActionContext.getContext().getRequest().getUserPrincipal(); 
			//sso_username = CommUtil.null2String(principal.getName()); 
			sso_username = CommUtil.null2String(ActionContext.getContext().getRequest().getRemoteUser());
		}catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println("未使用SSO，使用默认登录方式.");
		}
		if(!sso_username.equals("")){
			user = userdao.getUserByName(sso_username);
			if(user!=null){
				ActionContext.getContext().getSession().setAttribute(Constants.SESSION_USER, user);
				popedom=user.getPopedom();
				form.addResult("user", user);
				form.addResult("pop", popedom);
				form.addResult("cas", "1");
				return module.findPage("index");
			}else{
				form.addResult("msg", "登录失败，不存在的用户！");
				//返回到单点登录服务器
				WebXML webxml=new WebXML();
				String casUrl=webxml.readParamValue("casServerLoginUrl");
				return new Page("login",casUrl,"url");
				//return module.findPage("login");
			}
		}else{
			return module.findPage("login");
		}
	}

	public Page doLogin(WebForm form, Module module) {
		ActionContext.getContext().getSession().removeAttribute(Constants.SESSION_USER);		
    	//检测许可
		//if(UserLicenseMgr.checkSystem()){
		String randomCode = (String) ActionContext.getContext().getSession().getAttribute("valiCode");
		String submitrandom = CommUtil.null2String(form.get("randomCode"));
		if(!submitrandom.equalsIgnoreCase(randomCode)){
			form.addResult("msg", "验证码不匹配:"+submitrandom+"/"+randomCode);
			return module.findPage("login");
		}
		String username = (String) form.get("username");
		User user = userdao.getUserByName(username);
		//System.out.println("取得用户:" + user);
		if (user != null){
			if(user.getPassword().equals((String) form.get("password"))){
				ActionContext.getContext().getSession().setAttribute(Constants.SESSION_USER, user);
				popedom=user.getPopedom();
				form.addResult("user", user);
				form.addResult("pop", popedom);
				form.addResult("cas", "0");
				return module.findPage("index");
			}else{
				form.addResult("msg", "登录失败，请检查密码！");
				return module.findPage("login");
			}
		} else {
			form.addResult("msg", "登录失败，不存在的用户！");
			return module.findPage("login");
		}
		/*
		}else{
			form.addResult("msg", "登录失败:"+UserLicenseMgr.error_info);
			return module.findPage("login");
		}
		*/
	}

	public Page doLogout(WebForm form, Module module) {
		ActionContext.getContext().getSession().removeAttribute(Constants.SESSION_USER);
		ActionContext.getContext().getSession().invalidate();
		try{
			//AttributePrincipal principal = (AttributePrincipal) ActionContext.getContext().getRequest().getUserPrincipal();
			//sso_username = CommUtil.null2String(principal.getName());
			sso_username = CommUtil.null2String(ActionContext.getContext().getRequest().getRemoteUser());
		}catch(Exception e){
			//e.printStackTrace();
			System.out.println("未使用SSO，使用默认登出方式.");
		}
		if(!sso_username.equals("")){
			//https://knife:8443/cas/logout
			WebXML webxml=new WebXML();
			String casUrl=webxml.readParamValue("casServerUrlPrefix");
			//String casUrl="http://passport.dev.cqrcb.com:8180";
			casUrl+="/logout?url="
				+ActionContext.getContext().getRequest().getRemoteHost()
				+ActionContext.getContext().getRequest().getContextPath();
			System.out.println("跳转到:"+casUrl);
			return new Page("logout",casUrl,"url");
		}else{
			return module.findPage("login");
		}
	}
}
