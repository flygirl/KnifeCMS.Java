package com.knife.news.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.knife.news.object.Type;
import com.knife.tools.MailUtil;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FormAction extends BaseCmdAction {

	public Page doInit(WebForm form, Module module) {
		String newsname = CommUtil.null2String(form.get("name"));
		String tid = CommUtil.null2String(form.get("tid"));
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String thisDate = sdf.format(new Date());
		Type type=typeDAO.getTypeById(tid);
		form.addResult("thisType", type);
		form.addResult("thisDate",thisDate);
		form.addResult("name", newsname);
		return module.findPage("index");
	}
	
	public Page doForm(WebForm form, Module module){
		String newsname = CommUtil.null2String(form.get("name"));
		String tid = CommUtil.null2String(form.get("tid"));
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String thisDate = sdf.format(new Date());
		Type type=typeDAO.getTypeById(tid);
		form.addResult("thisType", type);
		form.addResult("thisDate",thisDate);
		form.addResult("name", newsname);
		return module.findPage("form");
	}
	
	public Page doConfirm(WebForm form, Module module){
		String newsname = CommUtil.null2String(form.get("name"));
		String tid = CommUtil.null2String(form.get("tid"));
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String thisDate = sdf.format(new Date());
		Type type=typeDAO.getTypeById(tid);
		form.addResult("thisType", type);
		form.addResult("thisDate",thisDate);
		form.addResult("name", newsname);
		return module.findPage("confirm");
	}
	
	public Page doConfirmSchool(WebForm form, Module module){
		String newsname = CommUtil.null2String(form.get("name"));
		String tid = CommUtil.null2String(form.get("tid"));
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String thisDate = sdf.format(new Date());
		Type type=typeDAO.getTypeById(tid);
		form.addResult("thisType", type);
		form.addResult("thisDate",thisDate);
		form.addResult("name", newsname);
		return module.findPage("confirm_s");
	}

	public Page doSave(WebForm form, Module module) {
		//NewsServiceImpl newsDAO = NewsServiceImpl.getInstance();
		//TypeServiceImpl typeDAO = TypeServiceImpl.getInstance();
		//String type = (String) form.get("type");
		//createHtml(form);
		try{
		MailUtil amail=new MailUtil();
		if(amail.send(form)){
			form.addResult("msg", "表单提交成功");
			return module.findPage("succeed");
		}else{
			form.addResult("msg", "表单提交失败");
			return module.findPage("failed");
		}
		}catch(Exception e){
			e.printStackTrace();
			form.addResult("msg", "表单提交失败,请检查配置信息");
			return module.findPage("failed");
		}
	}
}
