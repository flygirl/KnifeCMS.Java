package com.knife.news.action;

import com.knife.news.logic.SupplierService;
import com.knife.news.logic.impl.SupplierServiceImpl;
import com.knife.news.object.Supplier;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SupplierAction extends BaseCmdAction{
	SupplierService supplierDAO = SupplierServiceImpl.getInstance();
	@Override
	public Page doInit(WebForm form, Module module) {
		// TODO Auto-generated method stub
		
		return new Page("test", "test.htm","html","/web/" + thisSite.getTemplate() + "/");
	}

	//保存供应商信息
	public Page doSave(WebForm form, Module module) {
		Supplier supplier = (Supplier) form.toPo(Supplier.class);
		if(supplierDAO.saveSupplier(supplier)) {
			form.addResult("msg", "添加成功！");
		} else {
			form.addResult("msg", "添加失败！");
		}
		return new Page("businesses3", "/type.do?tid=2657700133709245","url");
	}
}
