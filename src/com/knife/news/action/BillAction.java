package com.knife.news.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.BillService;
import com.knife.news.logic.SupplierService;
import com.knife.news.logic.impl.BillServiceImpl;
import com.knife.news.logic.impl.SupplierServiceImpl;
import com.knife.news.object.Bill;
import com.knife.news.object.Supplier;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

/*
 * 账单
 */
public class BillAction extends BaseCmdAction{
	private BillService billDAO = BillServiceImpl.getInstance();
	private SupplierService supplierDAO = SupplierServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		//return module.findPage("index");
		return new Page("index", "bill_list.htm","html","/web/" + thisSite.getTemplate());
	}
	
	public Page doList(WebForm form, Module module) {
		String suser = CommUtil.null2String(form.get("suser"));
		String spass = CommUtil.null2String(form.get("spass"));
		form.addResult("suser", suser);
		form.addResult("spass", spass);
		List<Bill> typeList = new ArrayList<Bill>();
		Supplier supplier = supplierDAO.getSupplierByUserAndPass(suser,spass);
		if(supplier!=null){
			form.addResult("supplier", supplier);
			//System.out.println("ssssssssss:"+supplier.getId());
			typeList = billDAO.getBillBySuser(supplier.getId());
		}else{
			typeList = null;
			//typeList = billDAO.getAllBill();
			form.addResult("msg", "用户名密码不正确！");
		}
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 1;
		int nextPage = 2;
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		if(nextPage>totalPage){
			nextPage=totalPage;
		}
		Collection<Object> paras = new ArrayList<Object>();
		if(supplier!=null){
			paras.add(supplier.getId());
		}
		List<Bill> firstTypes = billDAO.getBillBySql("id!='' and k_suser=?", paras,0, pageSize);
		form.addResult("tid", tid);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("billList", firstTypes);
		//return module.findPage("list");
		return new Page("list", "bill_list.htm","html","/web/" + thisSite.getTemplate());
	}

	public Page doPage(WebForm form, Module module) {
		String suser = CommUtil.null2String(form.get("suser"));
		String spass = CommUtil.null2String(form.get("spass"));
		form.addResult("suser", suser);
		form.addResult("spass", spass);
		List<Bill> typeList = new ArrayList<Bill>();
		Supplier supplier = supplierDAO.getSupplierByUserAndPass(suser,spass);
		if(supplier!=null){
			form.addResult("supplier", supplier);
			typeList = billDAO.getBillBySuser(supplier.getId());
		}else{
			//typeList = billDAO.getAllBill();
			form.addResult("msg", "用户名密码不正确！");
		}
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		if (frontPage <= 0) {
			form.addResult("msg", "这是第一页了！");
			form.addResult("frontPage", 1);
		} else {
			form.addResult("frontPage", frontPage);
		}
		if (nextPage > totalPage) {
			nextPage=totalPage;
			form.addResult("msg", "这是最后一页了！");
		}
		form.addResult("nextPage", nextPage);
		List<Bill> firstTypes = new ArrayList<Bill>();
		Collection<Object> paras = new ArrayList<Object>();
		if(supplier!=null){
			paras.add(supplier.getId());
		}
		if (end < pageSize) {
			firstTypes = billDAO.getBillBySql("id!='' and k_suser=?", paras, begin - 1, end);
		} else {
			firstTypes = billDAO.getBillBySql("id!='' and k_suser=?", paras, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("billList", firstTypes);
		//return module.findPage("list");
		return new Page("list", "bill_list.htm","html","/web/" + thisSite.getTemplate());
	}
}
