package com.knife.news.object;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.io.SAXReader;

import com.knife.news.logic.impl.FieldsServiceImpl;
import com.knife.news.logic.impl.FormServiceImpl;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.PicServiceImpl;
import com.knife.news.logic.impl.RelativeServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.logic.impl.VideoServiceImpl;

public class NewsVersion {

	private String id;
	private int versionnumber;
	private String newsid;
	private String description;
	private String linkedid;
	private String title;
	private String author;
	private String source;
	private String keywords;
	private String file;
	private String fileType;
	private String content;
	private Date date;
	private String username;
	private String type;
	private String treenews;
	private int display;
	private String commend;
	private String template;
	private int order;
	private String summary;
	private String href;
	private String link;
	private int tofront;
	private int count;
	private String docfile;
	private int isfee;
	private int rights;
	private String type2;
	private String doctype;
	private int score;
	private int search;
	private int ispub;
	private Date offdate;
	private String xmldata;
	private String flowusers;
	
	private Map<String, String> xmlfields = new HashMap<String, String>();
	private SimpleDateFormat dateformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private SiteServiceImpl siteDAO = SiteServiceImpl.getInstance();
	private NewsServiceImpl newsDAO = NewsServiceImpl.getInstance();
	private TypeServiceImpl typeDAO = TypeServiceImpl.getInstance();
	private FormServiceImpl formDAO = FormServiceImpl.getInstance();
	private FieldsServiceImpl fieldsDAO = FieldsServiceImpl.getInstance();
	private RelativeServiceImpl relativeDAO = RelativeServiceImpl.getInstance();
	private PicServiceImpl picDAO = PicServiceImpl.getInstance();
	private VideoServiceImpl videoDAO = VideoServiceImpl.getInstance();

	
	public NewsVersion() {
	}
	
	public NewsVersion(com.knife.news.object.News news){
		this.newsid = news.getId();
		this.linkedid = news.getLink();
		this.title = news.getTitle();
		this.author = news.getAuthor();
		this.source = news.getSource();
		this.keywords = news.getKeywords();
		this.file = news.getFile();
		this.fileType = news.getFileType();
		this.content =news.getContent();
		this.username = news.getUsername();
		this.date = news.getDate();
		this.treenews = news.getTreenews();
		this.type = news.getType();
		this.display = news.getDisplay();
		this.commend = news.getCommend();
		this.template = news.getTemplate();
		this.order = news.getOrder();
		this.summary = news.getSummary();
		this.href = news.getHref();
		this.link = news.getLink();
		this.tofront = news.getTofront();
		this.count = news.getCount();
		this.docfile = news.getDocfile();
		this.isfee = news.getIsfee();
		this.rights = news.getRights();
		this.type2 = news.getType2();
		this.doctype = news.getDoctype();
		this.score = news.getScore();
		this.search = news.getSearch();
		this.ispub = news.getIspub();
		this.offdate = news.getOffdate();
		this.xmldata = news.getXmldata();
		this.flowusers = news.getFlowusers();
		
	}
	/**
	 * 构造方法，将数据库模型转换为对象
	 * 
	 * @param news
	 *            文章的数据模型对象
	 */
	public NewsVersion(com.knife.news.model.NewsVersion newsverison) {
		if (newsverison != null) {
			this.id = newsverison.getId();
			this.versionnumber= newsverison.getVersionnumber();
			this.newsid = newsverison.getNewsid();
			this.description = newsverison.getDescription();
			this.title = newsverison.getTitle();
			this.author = newsverison.getAuthor();
			this.source = newsverison.getSource();
			this.keywords = newsverison.getKeywords();
			if (newsverison.getFile() != null) {
				this.file = newsverison.getFile();
				if (this.file.lastIndexOf(".") > 0) {
					this.fileType = newsverison.getFile().substring(
							this.file.lastIndexOf(".") + 1, this.file.length())
							+ ".gif";
				} else {
					this.fileType = "unknown.gif";
				}
				/*
				 * if(n.getFile().length()>14){ this.fileType =
				 * n.getFile().substring(8,14)+".gif"; }
				 */
			}
			if (newsverison.getLink() != null) {
				this.fileType = "link.gif";
			}
			this.content = newsverison.getContent();
			this.date = newsverison.getDate();
			this.username = newsverison.getUsername();
			this.type = newsverison.getType();
			this.treenews = newsverison.getTreenews();
			this.display = newsverison.getDisplay();
			this.commend = newsverison.getCommend();
			this.template = newsverison.getTemplate();
			this.order = newsverison.getOrder();
			this.link = newsverison.getLink();
			this.linkedid = newsverison.getLink();
			this.tofront = newsverison.getTofront();
			this.count = newsverison.getCount();
			this.docfile = newsverison.getDocfile();
			this.isfee = newsverison.getIsfee();
			this.rights = newsverison.getRights();
			this.type2 = newsverison.getType2();
			this.doctype = newsverison.getDoctype();
			this.summary = newsverison.getSummary();
			this.score = newsverison.getScore();
			this.search = newsverison.getSearch();
			this.ispub = newsverison.getIspub();
			this.offdate = newsverison.getOffdate();
			this.xmldata = newsverison.getXmldata();
			if (this.xmldata != null) {
				this.getXmlFields();
			}
			this.flowusers = newsverison.getFlowusers();
			
		}
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the versionnumber
	 */
	public int getVersionnumber() {
		return versionnumber;
	}
	/**
	 * @param versionnumber the versionnumber to set
	 */
	public void setVersionnumber(int versionnumber) {
		this.versionnumber = versionnumber;
	}
	/**
	 * @return the newsid
	 */
	public String getNewsid() {
		return newsid;
	}
	/**
	 * @param newsid the newsid to set
	 */
	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the linkedid
	 */
	public String getLinkedid() {
		return linkedid;
	}
	/**
	 * @param linkedid the linkedid to set
	 */
	public void setLinkedid(String linkedid) {
		this.linkedid = linkedid;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}
	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * 取得文章发布日期
	 * 
	 * @return 发布日期字符串
	 */
	public String getDateString() {
		return dateformat.format(date);
	}

	/**
	 * 设置文章发布日期
	 * 
	 * @param date
	 *            发布日期字符串
	 */
	public void setDateString(String date) {
		try {
			this.date = dateformat.parse(date);
		} catch (Exception e) {
			this.date = new Date();
		}
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the treenews
	 */
	public String getTreenews() {
		return treenews;
	}
	/**
	 * @param treenews the treenews to set
	 */
	public void setTreenews(String treenews) {
		this.treenews = treenews;
	}
	/**
	 * @return the display
	 */
	public int getDisplay() {
		return display;
	}
	/**
	 * @param display the display to set
	 */
	public void setDisplay(int display) {
		this.display = display;
	}
	/**
	 * @return the commend
	 */
	public String getCommend() {
		return commend;
	}
	/**
	 * @param commend the commend to set
	 */
	public void setCommend(String commend) {
		this.commend = commend;
	}
	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}
	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}
	/**
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}
	/**
	 * @param order the order to set
	 */
	public void setOrder(int order) {
		this.order = order;
	}
	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}
	/**
	 * @param summary the summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}
	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}
	/**
	 * @param href the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}
	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}
	/**
	 * @return the tofront
	 */
	public int getTofront() {
		return tofront;
	}
	/**
	 * @param tofront the tofront to set
	 */
	public void setTofront(int tofront) {
		this.tofront = tofront;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the docfile
	 */
	public String getDocfile() {
		return docfile;
	}
	/**
	 * @param docfile the docfile to set
	 */
	public void setDocfile(String docfile) {
		this.docfile = docfile;
	}
	/**
	 * @return the isfee
	 */
	public int getIsfee() {
		return isfee;
	}
	/**
	 * @param isfee the isfee to set
	 */
	public void setIsfee(int isfee) {
		this.isfee = isfee;
	}
	/**
	 * @return the rights
	 */
	public int getRights() {
		return rights;
	}
	/**
	 * @param rights the rights to set
	 */
	public void setRights(int rights) {
		this.rights = rights;
	}
	/**
	 * @return the type2
	 */
	public String getType2() {
		return type2;
	}
	/**
	 * @param type2 the type2 to set
	 */
	public void setType2(String type2) {
		this.type2 = type2;
	}
	/**
	 * @return the doctype
	 */
	public String getDoctype() {
		return doctype;
	}
	/**
	 * @param doctype the doctype to set
	 */
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}
	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * @return the search
	 */
	public int getSearch() {
		return search;
	}
	/**
	 * @param search the search to set
	 */
	public void setSearch(int search) {
		this.search = search;
	}
	/**
	 * @return the ispub
	 */
	public int getIspub() {
		return ispub;
	}
	/**
	 * @param ispub the ispub to set
	 */
	public void setIspub(int ispub) {
		this.ispub = ispub;
	}
	/**
	 * @return the offdate
	 */
	public Date getOffdate() {
		return offdate;
	}
	/**
	 * @param offdate the offdate to set
	 */
	public void setOffdate(Date offdate) {
		this.offdate = offdate;
	}
	/**
	 * @return the xmldata
	 */
	public String getXmldata() {
		return xmldata;
	}
	/**
	 * @param xmldata the xmldata to set
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}
	/**
	 * @return the flowusers
	 */
	public String getFlowusers() {
		return flowusers;
	}
	/**
	 * @param flowusers the flowusers to set
	 */
	public void setFlowusers(String flowusers) {
		this.flowusers = flowusers;
	}
	
	private Map<String, String> getXmlFields() {
		SAXReader reader = new SAXReader();
		if (this.xmldata.trim().length() > 0) {
			try {
				org.dom4j.Document xmlDoc = reader
						.read(new ByteArrayInputStream(this.xmldata
								.getBytes("UTF-8")));
				org.dom4j.Element rootElement = xmlDoc.getRootElement();
				if (rootElement.elements().size() > 0) {
					for (int i = 0; i < rootElement.elements().size(); i++) {
						org.dom4j.Element fieldElement = (org.dom4j.Element) rootElement
								.elements().get(i);
						String key = fieldElement.getName();
						String value = fieldElement.getText();
						// Map<String,String> field=new
						// HashMap<String,String>();
						xmlfields.put(key, value);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return xmlfields;
	}

	/**
	 * 取得外部数据对应的键值
	 * 
	 * @param fieldName
	 *            外部数据键
	 * @return 外部数据值
	 */
	public String getField(String fieldName) {
		if (xmlfields.size() > 0) {
			return xmlfields.get(fieldName);
		} else {
			return null;
		}
	}
}
