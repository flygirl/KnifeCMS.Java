package com.knife.news.object;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.io.SAXReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import com.knife.news.logic.impl.FieldsServiceImpl;
import com.knife.news.logic.impl.FormServiceImpl;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.PicServiceImpl;
import com.knife.news.logic.impl.RelativeServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.logic.impl.VideoServiceImpl;
import com.knife.news.manage.thread.KnifeThreadPool;
import com.knife.news.manage.thread.NewsGenerate;
import com.knife.news.manage.thread.TypeGenerate;
import com.knife.news.model.Fields;
import com.knife.news.model.Form;
import com.knife.tools.PinYinUtil;
import com.knife.tools.string.ChineseString;

/**
 * 文章对象
 * 
 * @author Knife
 * 
 */
public class News {
	private String id;
	private String linkedid;
	private String title;
	private String author;
	private String source;
	private String keywords;
	private String file;
	private String fileType;
	private String content;
	private Date date;
	private String username;
	private String type;
	private String treenews;
	private int display;
	private String commend;
	private String template;
	private int order;
	private String summary;
	private String href;
	private String link;
	private int tofront;
	private int count;
	private String docfile;
	private int isfee;
	private int rights;
	private String type2;
	private String doctype;
	private int score;
	private int search;
	private int ispub;
	private Date offdate;
	private String xmldata;
	private String flowusers;


	private Map<String, String> xmlfields = new HashMap<String, String>();
	private SimpleDateFormat dateformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private SiteServiceImpl siteDAO = SiteServiceImpl.getInstance();
	private NewsServiceImpl newsDAO = NewsServiceImpl.getInstance();
	private TypeServiceImpl typeDAO = TypeServiceImpl.getInstance();
	private FormServiceImpl formDAO = FormServiceImpl.getInstance();
	private FieldsServiceImpl fieldsDAO = FieldsServiceImpl.getInstance();
	private RelativeServiceImpl relativeDAO = RelativeServiceImpl.getInstance();
	private PicServiceImpl picDAO = PicServiceImpl.getInstance();
	private VideoServiceImpl videoDAO = VideoServiceImpl.getInstance();

	public News() {
	}

	/**	 
	* 构造方法，文章版本恢复	 
	* @param news	
	*            文章的版本对象	 
	*/	
	public News(com.knife.news.object.NewsVersion newsv){		
		this.id = newsv.getNewsid();		
		this.title=newsv.getTitle();    	
		this.author=newsv.getAuthor();    	
		this.source=newsv.getSource();    	
		this.keywords=newsv.getKeywords();    	
		this.file=newsv.getFile();    	
		this.content=newsv.getContent();    	
		this.date = newsv.getDate();    	
		this.username=newsv.getUsername();    	
		this.type=newsv.getType();    	
		this.treenews=newsv.getTreenews();    	
		this.display=newsv.getDisplay();    	
		this.commend=newsv.getCommend();    	
		this.template=newsv.getTemplate();    	
		this.order=newsv.getOrder();    	
		this.link=newsv.getLink();    	
		this.tofront = newsv.getTofront();    	
		this.count = newsv.getCount();    	
		this.docfile=newsv.getDocfile();    	
		this.isfee=newsv.getIsfee();    	
		this.rights=newsv.getRights();    	
		this.type2=newsv.getType2();    	
		this.doctype=newsv.getDoctype();    	
		this.summary=newsv.getSummary();    	
		this.score=newsv.getScore();    	
		this.search=newsv.getSearch();    	
		this.ispub=newsv.getIspub();    	
		this.offdate=newsv.getOffdate();    	
		this.xmldata=newsv.getXmldata();    	
		this.flowusers = newsv.getFlowusers();	
	}
	
	/**
	 * 构造方法，将数据库模型转换为对象
	 * 
	 * @param news
	 *            文章的数据模型对象
	 */
	public News(com.knife.news.model.News news) {
		if (news != null) {
			this.id = news.getId();
			this.title = news.getTitle();
			this.author = news.getAuthor();
			this.source = news.getSource();
			this.keywords = news.getKeywords();
			if (news.getFile() != null) {
				this.file = news.getFile();
				if (this.file.lastIndexOf(".") > 0) {
					this.fileType = news.getFile().substring(
							this.file.lastIndexOf(".") + 1, this.file.length())
							+ ".gif";
				} else {
					this.fileType = "unknown.gif";
				}
				/*
				 * if(n.getFile().length()>14){ this.fileType =
				 * n.getFile().substring(8,14)+".gif"; }
				 */
			}
			if (news.getLink() != null) {
				this.fileType = "link.gif";
			}
			this.content = news.getContent();
			this.date = news.getDate();
			this.username = news.getUsername();
			this.type = news.getType();
			this.treenews = news.getTreenews();
			this.display = news.getDisplay();
			this.commend = news.getCommend();
			this.template = news.getTemplate();
			this.order = news.getOrder();
			this.link = news.getLink();
			this.linkedid = news.getLink();
			this.tofront = news.getTofront();
			this.count = news.getCount();
			this.docfile = news.getDocfile();
			this.isfee = news.getIsfee();
			this.rights = news.getRights();
			this.type2 = news.getType2();
			this.doctype = news.getDoctype();
			this.summary = news.getSummary();
			this.score = news.getScore();
			this.search = news.getSearch();
			this.ispub = news.getIspub();
			this.offdate = news.getOffdate();
			this.xmldata = news.getXmldata();
			if (this.xmldata != null) {
				this.getXmlFields();
			}
			this.flowusers = news.getFlowusers();
			
		}
	}
	
	/**
	 * @return the flowusers
	 */
	public String getFlowusers() {
		return flowusers;
	}

	/**
	 * @param flowusers the flowusers to set
	 */
	public void setFlowusers(String flowusers) {
		this.flowusers = flowusers;
	}

	/**
	 * 取得文章编号
	 * 
	 * @return 文章编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置文章编号
	 * 
	 * @param id
	 *            文章编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得文章标题
	 * 
	 * @return 文章标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 按字数截取文章标题(1个汉字占两位)
	 * 
	 * @param limits
	 *            限制字数
	 * @return 文章标题
	 */
	public String getTitle(int limits) {
		int realnum = ChineseString.getLength(title);
		if (realnum > limits) {
			return title.substring(0, (int) (limits / 2) - 2) + "…";
		} else {
			return title;
		}
	}

	/**
	 * 设置文章标题
	 * 
	 * @param title
	 *            文章标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 取得文章作者
	 * 
	 * @return 文章作者
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * 设置文章作者
	 * 
	 * @param author
	 *            文章作者
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * 取得内容模板路径
	 * 
	 * @return 模板路径
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * 设置内容模板路径
	 * 
	 * @param template
	 *            模板路径
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * 是否审核
	 * 
	 * @return 1审核，0未审核
	 */
	public int getDisplay() {
		return display;
	}

	/**
	 * 设置是否审核
	 * 
	 * @param display
	 *            1审核，0未审核
	 */
	public void setDisplay(int display) {
		this.display = display;
	}

	/**
	 * 取得文章内容
	 * 
	 * @return 文章内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 取得文章文本内容
	 * 
	 * @return 文本
	 */
	public String getTextContent() {
		return Jsoup.parse(content).text();
	}

	/**
	 * 设置文章内容
	 * 
	 * @param content
	 *            文章内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 取得文章发布日期
	 * 
	 * @return 发布日期
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * 设置文章发布日期
	 * 
	 * @param date
	 *            发布日期
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * 取得文章发布日期
	 * 
	 * @return 发布日期字符串
	 */
	public String getDateString() {
		return dateformat.format(date);
	}

	/**
	 * 设置文章发布日期
	 * 
	 * @param date
	 *            发布日期字符串
	 */
	public void setDateString(String date) {
		try {
			this.date = dateformat.parse(date);
		} catch (Exception e) {
			this.date = new Date();
		}
	}

	/**
	 * 取得文章附件
	 * 
	 * @return 附件路径
	 */
	public String getFile() {
		return file;
	}

	/**
	 * 设置文章附件
	 * 
	 * @param file
	 *            附件路径
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * 取得附件类型
	 * 
	 * @return 附件类型:png,jpg,gif,doc等
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * 取得文章来源
	 * 
	 * @return 文章来源
	 */
	public String getSource() {
		return source;
	}

	/**
	 * 设置文章来源
	 * 
	 * @param source
	 *            文章来源
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * 取得文章关键字
	 * 
	 * @return 文章关键字
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * 设置文章关键字
	 * 
	 * @param keywords
	 *            文章关键字
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * 取得文章编辑
	 * 
	 * @return 文章编辑
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 设置文章编辑
	 * 
	 * @param username
	 *            文章编辑
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 取得文章类别
	 * 
	 * @return 文章类别编号
	 */
	public String getType() {
		return type;
	}

	/**
	 * 设置文章类别
	 * 
	 * @param type
	 *            文章类别编号
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 取得所属附加文章编号
	 * 
	 * @return 文章编号
	 */
	public String getTreenews() {
		return treenews;
	}

	/**
	 * 设置所属附加文章编号
	 * 
	 * @param treenews
	 *            文章编号
	 */
	public void setTreenews(String treenews) {
		this.treenews = treenews;
	}

	/**
	 * 是否推荐
	 * 
	 * @return 1推荐，0不推荐
	 */
	public String getCommend() {
		return commend;
	}

	/**
	 * 设置是否推荐
	 * 
	 * @param commend
	 *            1推荐，0不推荐
	 */
	public void setCommend(String commend) {
		this.commend = commend;
	}

	/**
	 * 取得文章排序编号
	 * 
	 * @return 排序编号
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * 设置文章排序编号
	 * 
	 * @param order
	 *            排序编号
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * 取得文章引用源地址(仅针对引用文章)
	 * 
	 * @return 引用文章编号
	 */
	public String getLink() {
		if (this.link != null) {
			if (this.link.length() > 0) {
				return link;
			} else {
				return null;
			}
		}
		return link;
	}

	/**
	 * 设置文章引用源
	 * 
	 * @param link
	 *            引用文章编号
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * 取得文章访问数
	 * 
	 * @return 访问数
	 */
	public int getCount() {
		return count;
	}

	/**
	 * 设置文章访问数
	 * 
	 * @param count
	 *            访问数
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * 取得文章链接地址
	 * 
	 * @return 链接地址
	 */
	public String getHref() {
		return this.getHref(0);
	}

	/**
	 * 取得文章发布年
	 * 
	 * @return 发布年份
	 */
	public String getNewsYear() {
		String str1 = this.getDateString();
		String newsyear = str1.substring(0, 4);
		return newsyear;
	}

	/**
	 * 取得文章发布月
	 * 
	 * @return 发布月份
	 */
	public String getNewsMonth() {
		String str1 = this.getDateString();
		String newsmonth = "";
		try {
			newsmonth = str1.substring(6, 2);
		} catch (Exception e) {
		}
		return newsmonth;
	}

	/**
	 * 取得文章发布日
	 * 
	 * @return 发布日期
	 */
	public String getNewsDay() {
		String str1 = this.getDateString();
		String newsday = str1.substring(9, 2);
		return newsday;
	}

	/**
	 * 取得文章链接地址
	 * 
	 * @param mode
	 *            0普通模式,1引用模式
	 * @return 链接地址
	 */
	public String getHref(int mode) {
		if (mode == 1) {
			if (this.linkedid != null) {
				News anews = newsDAO.getNewsById(this.linkedid);
				this.href = anews.getHref();
				return this.href;
			}
		}
		String datedir = "";
		Type type = new Type();
		Site site = new Site();
		try {
			type = typeDAO.getTypeById(this.getType());
			site = siteDAO.getSiteById(type.getSite());
			if (site.getHtml() == 0) {
				this.href = "/news.do?id=" + this.getId();
			} else {
				if (type.getHtml() == 0) {
					this.href = "/news.do?id=" + this.getId();
				} else {
					if (site.getHtml_dir() == 0) {
						datedir = getNewsYear() + "/";
					} else if (site.getHtml_dir() == 1) {
						datedir = getNewsYear() + "/" + getNewsMonth() + "/";
					} else {
						datedir = getNewsYear() + "/" + getNewsMonth() + "/"
								+ getNewsDay() + "/";
					}
					if (site.getHtml_rule() == 0) {
						this.href = "/html/"
								+ site.getPub_dir()
								+ type.getTypeNamePath()
								+ "/"
								+ datedir
								+ Jsoup.clean(this.getTitle(), Whitelist
										.simpleText()) + ".htm";
					} else if (site.getHtml_rule() == 1) {
						this.href = "/html/" + site.getPub_dir()
								+ type.getTypeIdPath() + "/" + datedir
								+ this.getId() + ".htm";
					} else {
						this.href = "/html/" + site.getPub_dir()
								+ type.getTypeCNSpellPath() + "/" + datedir
								+ this.getCNSpellTitle() + ".htm";
					}
				}
			}
		} catch (Exception e) {
			return null;
		}
		return this.href;
	}

	/**
	 * 设置文章链接地址
	 * 
	 * @param href
	 *            链接地址
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * 取得文章摘要，默认为文本格式的文章内容
	 * 
	 * @return 文章摘要
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * 按字数截取文章摘要
	 * 
	 * @param nums
	 *            截取字数
	 * @return 文章摘要
	 */
	public String getSummary(int nums) {
		if (this.summary.length() > nums) {
			return this.summary.substring(0, nums - 2) + "…";
		} else {
			return this.summary;
		}
	}

	/**
	 * 设置文章摘要
	 * 
	 * @param summary
	 *            文章摘要
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * 取得自定义表单字段值
	 * 
	 * @param fieldName
	 *            字段名称
	 * @return 字段值
	 */
	public String get(String fieldName) {
		String order = "";
		try {
			Type newsType = typeDAO.getTypeById(this.type);
			Form newsForm = formDAO.getFormById(newsType.getForm());
			String linkid = this.id;
			if (this.linkedid != null) {
				if (this.linkedid.length() > 0) {
					linkid = this.linkedid;
				}
			}
			List<FormField> formFields = fieldsDAO.jsonStringToList(linkid,
					newsForm.getContent());
			for (FormField afield : formFields) {
				if (afield.getName().equalsIgnoreCase(fieldName)) {
					order = afield.getOrder();
					break;
				}
			}
			if (!order.equals("")) {
				Fields fields = fieldsDAO.getFieldsByNews(linkid, order);
				if (fields != null) {
					return fields.getValue();
				} else {
					return "";
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 按字数截取自定义表单字段值(1个汉字占两位)
	 * 
	 * @param fieldName
	 *            字段名称
	 * @param nums
	 *            截取字数
	 * @return 字段值
	 */
	public String get(String fieldName, int nums) {
		int realnum = 0;
		String ret = this.get(fieldName);
		if (ret != null) {
			realnum = ChineseString.getLength(ret);
		}
		if (realnum > nums) {
			return ret.substring(0, nums - 2) + "..";
		} else {
			return ret;
		}
	}

	/**
	 * 取得文章关联的相关文章列表
	 * 
	 * @return 相关文章列表
	 */
	public List<com.knife.news.object.News> getRelatives() {
		String linkid = this.id;
		if (this.linkedid != null) {
			if (this.linkedid.length() > 0) {
				linkid = this.linkedid;
			}
		}
		return relativeDAO.getRelatedNewsById(linkid);
	}

	/**
	 * 取得文章关联的图片列表
	 * 
	 * @param orderby
	 *            排序字段
	 * @return 图片列表
	 */
	public List<com.knife.news.object.Pic> getPics(String orderby) {
		String linkid = this.id;
		if (this.linkedid != null) {
			if (this.linkedid.length() > 0) {
				linkid = this.linkedid;
			}
		}
		if (orderby != null) {
			if (orderby.length() > 0) {
				return picDAO.getPicsByNewsId(linkid, orderby);
			}
		}
		return picDAO.getPicsByNewsId(linkid);
	}

	/**
	 * 取得文章关联的视频列表
	 * 
	 * @param orderby
	 *            排序字段
	 * @return 视频列表
	 */
	public List<com.knife.news.object.Video> getVideos(String orderby) {
		String linkid = this.id;
		if (this.linkedid != null) {
			if (this.linkedid.length() > 0) {
				linkid = this.linkedid;
			}
		}
		if (orderby != null) {
			if (orderby.length() > 0) {
				return videoDAO.getVideosByNewsId(linkid, orderby);
			}
		}
		return videoDAO.getVideosByNewsId(linkid);
	}

	/**
	 * 取得文章分类名称
	 * 
	 * @return 分类名称
	 */
	public String getTypeName() {
		String ret = "";
		if (this.getType() != null) {
			Type atype = null;
			try {
				atype = typeDAO.getTypeById(this.getType());
			} catch (Exception e) {
			}
			if (atype != null) {
				ret = atype.getName();
			}
		}
		return ret;
	}

	/**
	 * 取得引用文章源文章（仅针对引用文章）
	 * 
	 * @return 文章对象
	 */
	public News getLinkNews() {
		if (this.getLink() != null) {
			News anew = newsDAO.getNewsById(this.getLink());
			// anew.setId(this.getId());
			// anew.linkedid = this.getLink();
			return anew;
		} else {
			return null;
		}
	}

	/**
	 * 是否首页显示
	 * 
	 * @return 1首页显示，2首页不显示
	 */
	public int getTofront() {
		return tofront;
	}

	/**
	 * 设置是否首页显示
	 * 
	 * @param tofront
	 *            1首页显示，2首页不显示
	 */
	public void setTofront(int tofront) {
		this.tofront = tofront;
	}

	/**
	 * 取得资料库资料
	 * 
	 * @return 资料路径
	 */
	public String getDocfile() {
		return docfile;
	}

	/**
	 * 设置资料库资料
	 * 
	 * @param docfile
	 *            资料路径
	 */
	public void setDocfile(String docfile) {
		this.docfile = docfile;
	}

	/**
	 * 是否会员
	 * 
	 * @return 1会员，0非会员
	 */
	public int getIsfee() {
		return isfee;
	}

	/**
	 * 设置是否会员
	 * 
	 * @param isfee
	 *            1会员，0非会员
	 */
	public void setIsfee(int isfee) {
		this.isfee = isfee;
	}

	/**
	 * 会员权限
	 * 
	 * @return 权限分数
	 */
	public int getRights() {
		return rights;
	}

	/**
	 * 设置会员权限
	 * 
	 * @param rights
	 *            权限分数
	 */
	public void setRights(int rights) {
		this.rights = rights;
	}

	/**
	 * 取得第二分类
	 * 
	 * @return 分类名称
	 */
	public String getType2() {
		return type2;
	}

	/**
	 * 设置第二分类
	 * 
	 * @param type2
	 *            分类名称
	 */
	public void setType2(String type2) {
		this.type2 = type2;
	}

	/**
	 * 取得资料类型
	 * 
	 * @return 资料类型
	 */
	public String getDoctype() {
		return doctype;
	}

	/**
	 * 设置资料类型
	 * 
	 * @param doctype
	 *            资料类型
	 */
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}

	/**
	 * 取得资料积分
	 * 
	 * @return 资料积分
	 */
	public int getScore() {
		return score;
	}

	/**
	 * 设置资料积分
	 * 
	 * @param score
	 *            资料积分
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * 是否能被搜索到
	 * 
	 * @return 搜索状态:0被搜索到,1不被搜索
	 */
	public int getSearch() {
		return search;
	}

	/**
	 * 设置是否能被搜索到
	 * 
	 * @param search
	 *            搜索状态:0被搜索到,1不被搜索
	 */
	public void setSearch(int search) {
		this.search = search;
	}

	/**
	 * 是否已经发布
	 * 
	 * @return 发布状态:0未发布,1已发布
	 */
	public int getIspub() {
		return ispub;
	}

	/**
	 * 设置是否已经发布
	 * 
	 * @param ispub
	 *            发布状态:0未发布,1已发布
	 */
	public void setIspub(int ispub) {
		this.ispub = ispub;
	}

	/**
	 * 取得下线时间
	 * 
	 * @return 下线时间
	 */
	public Date getOffdate() {
		return offdate;
	}

	/**
	 * 设置下线时间
	 * 
	 * @param offdate
	 *            下线时间
	 */
	public void setOffdate(Date offdate) {
		this.offdate = offdate;
	}

	/**
	 * 取得外部数据
	 * 
	 * @return 外部数据(xml格式)
	 */
	public String getXmldata() {
		return xmldata;
	}

	/**
	 * 设定外部数据
	 * 
	 * @param xmldata
	 *            外部数据(xml格式)
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	private Map<String, String> getXmlFields() {
		SAXReader reader = new SAXReader();
		if (this.xmldata.trim().length() > 0) {
			try {
				org.dom4j.Document xmlDoc = reader
						.read(new ByteArrayInputStream(this.xmldata
								.getBytes("UTF-8")));
				org.dom4j.Element rootElement = xmlDoc.getRootElement();
				if (rootElement.elements().size() > 0) {
					for (int i = 0; i < rootElement.elements().size(); i++) {
						org.dom4j.Element fieldElement = (org.dom4j.Element) rootElement
								.elements().get(i);
						String key = fieldElement.getName();
						String value = fieldElement.getText();
						// Map<String,String> field=new
						// HashMap<String,String>();
						xmlfields.put(key, value);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return xmlfields;
	}

	/**
	 * 取得外部数据对应的键值
	 * 
	 * @param fieldName
	 *            外部数据键
	 * @return 外部数据值
	 */
	public String getField(String fieldName) {
		if (xmlfields.size() > 0) {
			return xmlfields.get(fieldName);
		} else {
			return null;
		}
	}

	/**
	 * 取得英文/拼音缩写名
	 * 
	 * @return 英文/拼音缩写名
	 */
	public String getCNSpellTitle() {
		return PinYinUtil.cn2FirstSpell(this.getTitle());
	}

	/**
	 * 取得前一篇文章
	 * 
	 * @return 文章对象
	 */
	public News getPrevNews() {
		News anew = new News();
		try {
			anew = (News) newsDAO.getNewsBySql(
					"k_type='" + this.type + "' and k_order>" + this.order
							+ " order by length(k_order) asc,k_order asc",
					null, 0, 1).get(0);
		} catch (Exception e) {
			anew = null;
		}
		return anew;
	}

	/**
	 * 取得后一篇文章
	 * 
	 * @return 文章对象
	 */
	public News getNextNews() {
		News anew = new News();
		try {
			anew = (News) newsDAO.getNewsBySql(
					"k_type='" + this.type + "' and k_order<" + this.order
							+ " order by length(k_order) desc,k_order desc",
					null, 0, 1).get(0);
		} catch (Exception e) {
			anew = null;
		}
		return anew;
	}

	/**
	 * 取得所属站点编号
	 * 
	 * @return 站点编号
	 */
	public String getSite() {
		String sid = "";
		Type atype = (Type) typeDAO.getTypeById(this.getType());
		if (atype != null) {
			sid = atype.getSite();
		}
		return sid;
	}

	/**
	 * 提取正文中的第一张图片
	 * 
	 * @return 图片地址
	 */
	public String getContentPic() {
		String cpic = null;
		Document doc = Jsoup.parse(this.content);
		Elements pics = doc.getElementsByTag("img");
		if (pics.size() > 0) {
			Element pic = pics.get(0);
			if (pic.attr("src") != null) {
				cpic = pic.attr("src");
			}
		}
		return cpic;
	}

	/*
	 * 
	 */
	public List<News> getLinkedNews() {
		return newsDAO.getLinkedNews(this.getId());
	}

	/*
	 * 取得文章的分类树频道列表
	 */
	public List<Type> getNewsTree() {
		List<Type> realTypes = new ArrayList<Type>();
		try {
			List<Type> treeTypes = typeDAO.getTreeTypesByTypeID(this.getType(),1);
			if(treeTypes!=null){
				for (Type atype : treeTypes) {
					atype.setSite(this.getSite());
					atype.setTreenews(this.getId());
					atype.setHref(atype.getHref());
					realTypes.add(atype);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realTypes;
	}

	// 文章上线
	public void onLine() {
		try {
			this.setIspub(1);
			newsDAO.updateNews(this);
			Type type = typeDAO.getTypeById(this.getType());
			if (type != null) {
				KnifeThreadPool.getInstance().execute(
						NewsGenerate.getInstance(this, KnifeThreadPool
								.getLock(), 6));
			} else {
				this.setType("0");
				newsDAO.updateNews(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 文章撤销
	public void cancelOnLine() {
		try {
			this.setIspub(0);
			newsDAO.updateNews(this);
			Type type = typeDAO.getTypeById(this.getType());
			if (type != null) {
				KnifeThreadPool.getInstance().execute(
						NewsGenerate.getInstance(this, KnifeThreadPool
								.getLock(), 6));
			} else {
				this.setType("0");
				newsDAO.updateNews(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 文章下线
	public void offLine() {
		try {
			Type type = typeDAO.getTypeById(this.getType());
			if (type != null) {
				this.setIspub(2);
				newsDAO.updateNews(this);
				KnifeThreadPool.getInstance().execute(
						NewsGenerate.getInstance(this, KnifeThreadPool
								.getLock(), 6));
			} else {
				this.setType("0");
				this.setIspub(2);
				newsDAO.updateNews(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// delete
	public void delLine() {
		try {
			Type type = typeDAO.getTypeById(this.getType());
			if (type != null) {
				KnifeThreadPool.getInstance().execute(
						TypeGenerate.getInstance(this.getType(),
								KnifeThreadPool.getLock(), 6));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 文章发布
	 */
	public void pub() {
		Date nowDate = new Date();
		Date thisDate = this.getDate();
		if (nowDate.getTime() >= thisDate.getTime()) {
			if (this.getDisplay() == 1) {
				this.onLine();
			} else {
				this.cancelOnLine();
			}
		}
	}

	/**
	 * 取消发布
	 */
	public void unPub() {
		Date nowDate = new Date();
		Date thisDate = this.getOffdate();
		if (this.getDisplay() == 1) {
			if (nowDate.getTime() >= thisDate.getTime()) {
				this.offLine();
			}
		}
	}

	/**
	 * 更新发布
	 */
	public void updatePub() {
		Date nowDate = new Date();
		long onlinetime = this.getDate().getTime();
		if (nowDate.getTime() >= onlinetime) {
			this.pub();
		}
		if (this.getOffdate() != null) {
			long offlinetime = this.getOffdate().getTime();
			if (nowDate.getTime() >= offlinetime) {
				this.unPub();
			}
		}
	}
}
