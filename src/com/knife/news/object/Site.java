package com.knife.news.object;

import com.knife.news.logic.ThemeService;
import com.knife.news.logic.impl.ThemeServiceImpl;

/**
 * 站点对象
 * @author Knife
 *
 */
public class Site {
	
	private String id;
	private String name;
	private String domain;
	private String admin;
	private String lang;
	private int html;
	private int html_rule;
	private int html_dir;
	private String mailserver;
	private String mailuser;
	private String mailpass;
	private int isthumb;
	private int order;
	private String pub_dir;
	private String template;
	private int big5;
	private String skin;
	private String index;
	private String theme;
	private String css_file;
	//jlm
	private int commcheck;
	private int isconvert_streammedia;
	private int isconvert_library;
	
	private ThemeService themeDAO=ThemeServiceImpl.getInstance();
	
	public Site(){}
	
    public Site(com.knife.news.model.Site site){
		this.id			= site.getId();
		this.name		= site.getName();
    	this.domain		= site.getDomain();
    	this.admin		= site.getAdmin();
    	this.lang		= site.getLang();
    	this.html		= site.getHtml();
    	this.html_rule	= site.getHtml_rule();
    	this.html_dir	= site.getHtml_dir();
    	this.mailserver	= site.getMailserver();
    	this.mailuser	= site.getMailuser();
    	this.mailpass	= site.getMailpass();
    	this.isthumb	= site.getIsthumb();
    	this.order		= site.getOrder();
    	this.pub_dir	= site.getPub_dir();
    	this.template	= site.getTemplate();
    	this.skin 		= "/skin/"+site.getTemplate();
    	this.big5 		= site.getBig5();
    	this.index 		= site.getIndex();
    	this.theme		= site.getTheme();
    	this.css_file	= site.getCss_file();
    	
    	this.commcheck  = site.getCommcheck();
    	this.isconvert_library = site.getIsconvert_library();
    	this.isconvert_streammedia = site.getIsconvert_library();
    }

	/**
	 * 取得站点编号
	 * @return 站点编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置站点编号
	 * @param id 站点编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得站点名称
	 * @return 站点名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置站点名称
	 * @param name 站点名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 取得站点域名
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * 设置站点域名
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * 取得站点管理员
	 * @return 管理员
	 */
	public String getAdmin() {
		return admin;
	}

	/**
	 * 设置站点管理员
	 * @param admin 管理员
	 */
	public void setAdmin(String admin) {
		this.admin = admin;
	}

	/**
	 * 取得站点语言
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * 设置站点语言
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * 是否生成静态页面  0不生成，1生成
	 * @return the html
	 */
	public int getHtml() {
		return html;
	}

	/**
	 * 设置是否生成静态页面  0不生成，1生成
	 * @param html the html to set
	 */
	public void setHtml(int html) {
		this.html = html;
	}
	
	/**
	 * 取得命名规则  0标题命名，1编号命名
	 * @return 命名规则值
	 */
	public int getHtml_rule() {
		return html_rule;
	}

	/**
	 * 设置命名规则  0标题命名，1编号命名
	 * @param html_rule
	 */
	public void setHtml_rule(int html_rule) {
		this.html_rule = html_rule;
	}
	
	/**
	 * 取得目录深度  0按年分，1按年月分，2按年月日分
	 * @return 目录深度值
	 */
	public int getHtml_dir() {
		return html_dir;
	}

	/**
	 * 设置目录深度  0按年分，1按年月分，2按年月日分
	 * @param html_dir 目录深度值
	 */
	public void setHtml_dir(int html_dir) {
		this.html_dir = html_dir;
	}

	/**
	 * 取得邮件服务器地址
	 * @return 邮件服务器
	 */
	public String getMailserver() {
		return mailserver;
	}

	/**
	 * 设置邮件服务器地址
	 * @param mailserver 邮件服务器
	 */
	public void setMailserver(String mailserver) {
		this.mailserver = mailserver;
	}

	/**
	 * 取得邮件账号
	 * @return 邮件帐号
	 */
	public String getMailuser() {
		return mailuser;
	}

	/**
	 * 设置邮件账号
	 * @param mailuser 邮件帐号
	 */
	public void setMailuser(String mailuser) {
		this.mailuser = mailuser;
	}

	/**
	 * 取得邮件密码
	 * @return 邮件密码
	 */
	public String getMailpass() {
		return mailpass;
	}

	/**
	 * 设置邮件密码
	 * @param mailpass 邮件密码
	 */
	public void setMailpass(String mailpass) {
		this.mailpass = mailpass;
	}

	/**
	 * 取得缩略图参数
	 * @return 0不生成，1生成
	 */
	public int getIsthumb() {
		return isthumb;
	}

	/**
	 * 设置缩略图参数
	 * @param isthumb 0不生成，1生成
	 */
	public void setIsthumb(int isthumb) {
		this.isthumb = isthumb;
	}

	/**
	 * 取得排序编号
	 * @return 排序编号
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * 设置排序编号
	 * @param order 排序编号
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * 取得发布目录
	 * @return 发布目录
	 */
	public String getPub_dir() {
		return pub_dir;
	}

	/**
	 * 设置发布目录
	 * @param pub_dir 发布目录
	 */
	public void setPub_dir(String pub_dir) {
		this.pub_dir = pub_dir;
	}

	/**
	 * 取得模板目录
	 * @return 模板目录
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * 设置模板目录
	 * @param template 模板目录
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * 是否生成繁体站
	 * @return 0不生成，1生成
	 */
	public int getBig5() {
		return big5;
	}

	/**
	 * 设置生成繁体站
	 * @param big5 0不生成，1生成
	 */
	public void setBig5(int big5) {
		this.big5 = big5;
	}

	/**
	 * 取得皮肤目录
	 * @return 皮肤目录
	 */
	public String getSkin() {
		return skin;
	}

	/**
	 * 设置皮肤目录
	 * @param skin 皮肤目录
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * 取得首页文件
	 * @return 首页文件名,默认为index.htm
	 */
	public String getIndex() {
		if(this.index!=null){
			return index;
		}else{
			return "index.htm";
		}
	}

	/**
	 * 设置首页文件名
	 * @param index 首页文件名
	 */
	public void setIndex(String index) {
		this.index = index;
	}

	/**
	 * 取得主题
	 * @return 主题编号
	 */
	public String getTheme() {
		return theme;
	}

	/**
	 * 设置主题
	 * @param theme 主题编号
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}

	/**
	 * 取得css样式文件名
	 * @return 样式文件名
	 */
	public String getCss_file() {
		return css_file;
	}

	/**
	 * 设置css样式文件名
	 * @param css_file 样式文件名
	 */
	public void setCss_file(String css_file) {
		this.css_file = css_file;
	}
	
	/**
	 * 取得主题名称
	 * @return 主题名称
	 */
	public String getThemeName(){
		try{
			Theme theme=themeDAO.getThemeById(this.getTheme());
			return theme.getName();
		}catch(Exception e){
			return null;
		}
	}
	
	//jlm
	public int getCommcheck(){
		return commcheck;
	}
	
	public void setCommcheck(int commcheck){
		this.commcheck = commcheck;
	}
	
	public int getIsconvert_streammedia(){
		return isconvert_streammedia;
	}
	
	public void setIsconvert_streammedia(int isconvert_streammedia){
		this.isconvert_streammedia = isconvert_streammedia;
	}
	
	public int getIsconvert_library(){
		return isconvert_library;
	}
	
	public void setIsconvert_library(int isconvert_library){
		this.isconvert_library = isconvert_library;
	}
}
