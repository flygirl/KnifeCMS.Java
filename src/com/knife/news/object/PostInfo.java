package com.knife.news.object;

/**
 * 发布信息统计
 * @author Smith
 *
 */
public class PostInfo {
	private String name;
	private int nums;
	private int lastMonthNum;
	private int monthNum;
	private int searchNum;
	private boolean detail;
	private String detailUrl;
	
	public PostInfo(){
		this.detail=false;
	}
	
	/**
	 * 取得信息名称
	 * @return 信息名称
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * 设置信息名称
	 * @param name 信息名称
	 */
	public void setName(String name){
		this.name=name;
	}
	
	/**
	 * 取得信息数据
	 * @return 信息数据
	 */
	public int getNums(){
		return nums;
	}
	
	/**
	 * 设置信息数据
	 * @param nums 信息数据
	 */
	public void setNums(int nums){
		this.nums=nums;
	}
	
	/**
	 * 取得上月信息
	 * @return 上月信息
	 */
	public int getLastMonthNum(){
		return lastMonthNum;
	}
	
	/**
	 * 设置上月信息
	 * @param lastMonthNum 上月信息
	 */
	public void setLastMonthNum(int lastMonthNum){
		this.lastMonthNum=lastMonthNum;
	}
	
	/**
	 * 取得本月信息
	 * @return 本月信息
	 */
	public int getMonthNum(){
		return monthNum;
	}
	
	/**
	 * 设置本月信息
	 * @param monthNum 本月信息
	 */
	public void setMonthNum(int monthNum){
		this.monthNum=monthNum;
	}
	
	/**
	 * 取得搜索统计信息
	 * @return 搜索统计信息
	 */
	public int getSearchNum(){
		return searchNum;
	}
	
	/**
	 * 设置搜索统计信息
	 * @param searchNum 搜索统计信息
	 */
	public void setSearchNum(int searchNum){
		this.searchNum=searchNum;
	}
	
	/**
	 * 是否获取详情
	 * @return true是,false否
	 */
	public boolean getDetail(){
		return detail;
	}
	
	/**
	 * 设置是否获取详情
	 * @param detail true是,false否
	 */
	public void setDetail(boolean detail){
		this.detail=detail;
	}
	
	/**
	 * 获取详情地址
	 * @return 详情地址
	 */
	public String getDetailUrl(){
		return detailUrl;
	}
	
	/**
	 * 设置详情地址
	 * @param detailUrl 详情地址
	 */
	public void setDetailUrl(String detailUrl){
		this.detailUrl=detailUrl;
	}
}
