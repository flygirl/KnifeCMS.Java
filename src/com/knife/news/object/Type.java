package com.knife.news.object;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.FlowService;
import com.knife.news.logic.FormService;
import com.knife.news.logic.NewsService;
import com.knife.news.logic.SiteService;
import com.knife.news.logic.TreeService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.UserService;
import com.knife.news.logic.impl.FlowServiceImpl;
import com.knife.news.logic.impl.FormServiceImpl;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TreeServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.model.Form;
import com.knife.news.model.Tree;
import com.knife.tools.PinYinUtil;
import com.knife.util.CommUtil;

/**
 * 频道对象
 * 
 * @author Knife
 * 
 */
public class Type {
	private String id;
	private String name;
	private String tree;
	private String tree_id;
	private String parent;
	private Type parentType;
	private String value;
	private int order;
	private boolean isLeave;
	private String index_template;
	private String type_template;
	private String news_template;
	private String form;
	private String admin;
	private String audit;
	private int display;
	private int show;
	private int ismember;
	private String url;
	private int target;
	private String href;
	private String pagenum;
	private int pagesize;
	private String orderby;
	private int search; 
	private String treenews;
	private String site;
	private int needpub;
	private int html;
	private int html_rule;
	private int html_dir;
	private int pubnum;
	private int totalnum;
	private String flow;

	private SiteService siteDAO = SiteServiceImpl.getInstance();
	private TypeService typeDAO = TypeServiceImpl.getInstance();
	private TreeService treeDAO = TreeServiceImpl.getInstance();
	private NewsService newsDAO = NewsServiceImpl.getInstance();
	private UserService userDAO = UserServiceImpl.getInstance();
	private FormService formDAO = FormServiceImpl.getInstance();
	private FlowService flowDao = FlowServiceImpl.getInstance();
	public Type() {
	}

	/**
	 * 构造方法，将数据模型转换为对象
	 * 
	 * @param atype
	 *            频道的数据模型对象
	 */
	public Type(com.knife.news.model.Type atype) {
		this.id = atype.getId();
		this.name = atype.getName();
		this.tree = atype.getTree();
		this.tree_id = atype.getTree_id();
		this.parent = atype.getParent();
		this.value = atype.getValue();
		this.order = atype.getOrder();
		String tree_id = "";
		if (atype.getTree_id() != null) {
			tree_id = atype.getTree_id();
		}
		if (typeDAO.getSubTypesById(atype.getSite(), atype.getId(), -1, "",
				tree_id).size() > 0) {
			this.isLeave = false;
		} else {
			this.isLeave = true;
		}
		this.index_template = atype.getIndex_template();
		this.type_template = atype.getType_template();
		this.news_template = atype.getNews_template();
		this.form = atype.getForm();
		this.admin = atype.getAdmin();
		this.audit = atype.getAudit();
		this.display = atype.getDisplay();
		this.show = atype.getShow();
		this.ismember = atype.getIsmember();
		this.url = atype.getUrl();
		this.target = atype.getTarget();
		this.pagenum = atype.getPagenum();
		this.pagesize = atype.getPagesize();
		this.orderby = atype.getOrderby();
		this.search = atype.getSearch();
		this.site = atype.getSite();
		this.needpub = atype.getNeedpub();
		this.html = atype.getHtml();
		this.html_rule = atype.getHtml_rule();
		this.html_dir = atype.getHtml_dir();
		this.flow = atype.getFlow();
	}
	
	public String getFlow(){
		return flow;
	}
	
	public void setFlow(String flow){
		this.flow = flow;
	}

	/**
	 * 取得频道描述
	 * 
	 * @return 频道描述
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 设置频道描述
	 * 
	 * @param value
	 *            频道描述
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 取得父频道编号
	 * 
	 * @return 频道编号
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * 设置父频道编号
	 * 
	 * @param parent
	 *            频道编号
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * 取得父频道
	 * 
	 * @return 频道对象
	 */
	public Type getParentType() {
		if (!parent.equals("0")) {
			parentType = typeDAO.getTypeById(parent);
		}
		return parentType;
	}

	/**
	 * 设置父频道
	 * 
	 * @param ptype
	 *            频道对象
	 */
	public void setParentType(Type ptype) {
		this.parentType = ptype;
	}

	/**
	 * 取得频道编号
	 * 
	 * @return 频道编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置频道编号
	 * 
	 * @param id
	 *            频道编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得频道名称
	 * 
	 * @return 频道名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置频道名称
	 * 
	 * @param name
	 *            频道名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 取得附加频道树编号
	 * 
	 * @return 树编号
	 */
	public String getTree() {
		return tree;
	}

	/**
	 * 设置附加频道树编号
	 * 
	 * @param tree
	 *            树编号
	 */
	public void setTree(String tree) {
		this.tree = tree;
	}

	/**
	 * 取得所属频道树编号
	 * 
	 * @return 树编号
	 */
	public String getTree_id() {
		return tree_id;
	}

	/**
	 * 设置所属频道树编号
	 * 
	 * @param tree_id
	 *            树编号
	 */
	public void setTree_id(String tree_id) {
		this.tree_id = tree_id;
	}

	/**
	 * 取得频道排序编号
	 * 
	 * @return 排序编号
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * 设置频道排序编号
	 * 
	 * @param order
	 *            排序编号
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * 是否终极频道（没有子频道）
	 * 
	 * @return true是，false否
	 */
	public boolean isLeave() {
		return isLeave;
	}

	/**
	 * 设置是否终极频道
	 * 
	 * @param isLeave
	 *            true是，false否
	 */
	public void setLeave(boolean isLeave) {
		this.isLeave = isLeave;
	}

	/**
	 * 取得频道封面模板
	 * 
	 * @return 频道封面模板名称
	 */
	public String getIndex_template() {
		return index_template;
	}

	/**
	 * 设置频道封面模板
	 * 
	 * @param index_template
	 *            频道封面模板
	 */
	public void setIndex_template(String index_template) {
		this.index_template = index_template;
	}

	/**
	 * 取得频道模板
	 * 
	 * @return 频道模板名称
	 */
	public String getType_template() {
		return type_template;
	}

	/**
	 * 设置频道模板
	 * 
	 * @param type_template
	 *            频道模板
	 */
	public void setType_template(String type_template) {
		this.type_template = type_template;
	}

	/**
	 * 取得内容模板
	 * 
	 * @return 内容模板名称
	 */
	public String getNews_template() {
		return news_template;
	}

	/**
	 * 设置内容模板
	 * 
	 * @param news_template
	 *            内容模板名称
	 */
	public void setNews_template(String news_template) {
		this.news_template = news_template;
	}

	/**
	 * 取得自定义表单
	 * 
	 * @return 表单对象
	 */
	public String getForm() {
		return form;
	}

	/**
	 * 设置自定义表单
	 * 
	 * @param form
	 *            表单对象
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * 取得频道编辑
	 * 
	 * @return 编辑名称
	 */
	public String getAdmin() {
		return admin;
	}

	/**
	 * 设置频道编辑
	 * 
	 * @param admin
	 *            编辑名称
	 */
	public void setAdmin(String admin) {
		this.admin = admin;
	}

	/**
	 * 取得频道审核员
	 * 
	 * @return 频道审核员
	 */
	public String getAudit() {
		return audit;
	}

	/**
	 * 设置频道审核员
	 * 
	 * @param audit
	 *            频道审核员
	 */
	public void setAudit(String audit) {
		this.audit = audit;
	}

	/**
	 * 是否显示频道
	 * 
	 * @return 1显示，0不显示
	 */
	public int getDisplay() {
		return display;
	}

	/**
	 * 设置是否显示频道
	 * 
	 * @param display
	 *            1显示，0不显示
	 */
	public void setDisplay(int display) {
		this.display = display;
	}

	/**
	 * 是否单页模式
	 * 
	 * @return 1单页，0列表
	 */
	public int getShow() {
		return show;
	}

	/**
	 * 设置是否单页模式
	 * 
	 * @param show
	 *            1单页，0列表
	 */
	public void setShow(int show) {
		this.show = show;
	}

	/**
	 * 是否会员
	 * 
	 * @return 1会员，0非会员
	 */
	public int getIsmember() {
		return ismember;
	}

	/**
	 * 设置是否会员
	 * 
	 * @param ismember
	 *            1会员，0非会员
	 */
	public void setIsmember(int ismember) {
		this.ismember = ismember;
	}

	/**
	 * 取得外部链接
	 * 
	 * @return 链接地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置外部链接
	 * 
	 * @param url
	 *            链接地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 是否新窗口打开
	 * 
	 * @return 1是，0否
	 */
	public int getTarget() {
		return target;
	}

	/**
	 * 设置是否新窗口打开
	 * 
	 * @param target
	 *            1是，0否
	 */
	public void setTarget(int target) {
		this.target = target;
	}

	/**
	 * 取得所属频道树文章编号
	 * 
	 * @return 文章编号
	 */
	public String getTreenews() {
		return treenews;
	}

	/**
	 * 设置所属频道树文章编号
	 * 
	 * @param treenews
	 *            文章编号
	 */
	public void setTreenews(String treenews) {
		this.treenews = treenews;
	}

	/**
	 * 取得频道链接地址
	 * 
	 * @return 链接地址
	 */
	public String getHref() {
		// 如果是分类树文章伪造的频道时，就直接返回文章路径
		if (this.getId() == null) {
			return this.href;
		}
		String tmpUrl = CommUtil.null2String(this.getUrl());
		if (tmpUrl.length() > 0) {
			this.href = tmpUrl;
			return this.href;
		}
		Site site = new Site();
		try {
			if (!"0".equals(this.getSite())) {
				site = siteDAO.getSiteById(this.getSite());
			}
		} catch (Exception e) {
			return null;
		}
		if (site.getHtml() == 0) {
			this.href = "/type.do?tid=" + this.getId();
			// 如果是分类树
			if (this.getTree_id() != null) {
				this.href += "&sid=" + this.getSite();
				if (this.getTreenews() != null) {
					this.href += "&treenews=" + this.getTreenews();
				}
			}
		} else {
			if (this.getHtml() != 0) {
				if (site.getHtml_rule() == 0) {
					this.href = "/html/" + site.getPub_dir()
							+ this.getTypeNamePath() + "/index.htm";
				} else if (site.getHtml_rule() == 1) {
					this.href = "/html/" + site.getPub_dir()
							+ this.getTypeIdPath() + "/index.htm";
				} else {
					this.href = "/html/" + site.getPub_dir()
							+ this.getTypeCNSpellPath() + "/index.htm";
				}
			} else {
				this.href = "/type.do?tid=" + this.getId();
				// 如果是分类树
				if (this.getTree_id() != null) {
					this.href += "&sid=" + this.getSite();
					if (this.getTreenews() != null) {
						this.href += "&treenews=" + this.getTreenews();
					}
				}
			}
		}

		return this.href;
	}

	/**
	 * 取得频道分页地址
	 * 
	 * @param pageNum
	 *            页码
	 * @return 链接地址
	 */
	public String getHref(int pageNum) {
		// 如果是分类树文章伪造的频道时，就直接返回文章路径
		if (this.getId() == null) {
			return this.href;
		}
		String tmpUrl = CommUtil.null2String(this.getUrl());
		if (tmpUrl.length() > 0) {
			this.href = tmpUrl;
			return this.href;
		}
		Boolean hasUrl = false;
		Site site = new Site();
		try {
			site = siteDAO.getSiteById(this.getSite());
		} catch (Exception e) {
			return null;
		}
		if (this.getUrl() != null) {
			if (this.getUrl().length() > 0) {
				this.href = this.getUrl();
				hasUrl = true;
			}
		}
		if (!hasUrl) {
			if (site != null) {
				if (site.getHtml() == 0) {
					this.href = "/type.do?parameter=page&page=" + pageNum
							+ "&tid=" + this.getId();
					// 如果是分类树
					if (this.getTree_id() != null) {
						this.href += "&sid=" + this.getSite();
						if (this.getTreenews() != null) {
							this.href += "&treenews=" + this.getTreenews();
						}
					}
				} else {
					if (this.getHtml() != 0) {
						String pagefile = "/index.htm";
						if (pageNum > 1) {
							pagefile = "/index" + pageNum + ".htm";
						}
						if (site.getHtml_rule() == 0) {
							this.href = "/html/" + site.getPub_dir()
									+ this.getTypeNamePath() + pagefile;
						} else if (site.getHtml_rule() == 1) {
							this.href = "/html/" + site.getPub_dir()
									+ this.getTypeIdPath() + pagefile;
						} else {
							this.href = "/html/" + site.getPub_dir()
									+ this.getTypeCNSpellPath() + pagefile;
						}
					} else {
						this.href = "/type.do?parameter=page&page=" + pageNum
								+ "&tid=" + this.getId();
						// 如果是分类树
						if (this.getTree_id() != null) {
							this.href += "&sid=" + this.getSite();
							if (this.getTreenews() != null) {
								this.href += "&treenews=" + this.getTreenews();
							}
						}
					}
				}// 2012.4.8标记
			}
		}
		return this.href;
	}

	/**
	 * 设置链接地址
	 * 
	 * @param href
	 *            链接地址
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * 取得每页新闻条数
	 * 
	 * @return 新闻条数
	 */
	public String getPagenum() {
		if (pagenum != null) {
			if (pagenum.equals("")) {
				pagenum = "20";
			}
		} else {
			pagenum = "20";
		}
		return pagenum;
	}

	/**
	 * 设置每页新闻条数
	 * 
	 * @param pagenum
	 *            新闻条数
	 */
	public void setPagenum(String pagenum) {
		this.pagenum = pagenum;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public String getOrderby() {
		return orderby;
	}

	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}

	public int getSearch() {
		return search;
	}

	public void setSearch(int search) {
		this.search = search;
	}

	/**
	 * 取得所有子频道
	 * 
	 * @param display
	 *            1显示，0不显示
	 * @return 频道列表
	 */
	public List<Type> getSubTypes(int display) {
		return typeDAO.getSubTypesById(this.getId(), display);
	}
	
	/**
	 * 取得包含某个名称的子频道
	 * @param typeName
	 * @return
	 */
	public Type getSubTypeByName(String typeName){
		Type atype=null;
		List<Type> subtypes=typeDAO.getAllSubTypesById(this.getId(),-1);
		for(Type btype:subtypes){
			if(typeName.equals(btype.getName())){
				atype=btype;
				break;
			}
		}
		return atype;
	}

	/**
	 * 取得所有父频道
	 * 
	 * @return 频道列表
	 */
	public List<Type> getParentTypes() {
		return getParentTypes(true);
	}

	public List<Type> getParentTypes(boolean order) {
		List<Type> otypes = new ArrayList<Type>();
		List<Type> ptypes = getParentTypes(this);
		if (order) {
			if (ptypes.size() > 0) {
				// 重置顺序
				for (int i = (ptypes.size()); i > 0; i--) {
					otypes.add(ptypes.get(i - 1));
				}
			}
		} else {
			otypes = ptypes;
		}
		return otypes;
	}

	private List<Type> getParentTypes(Type type) {
		List<Type> ptypes = new ArrayList<Type>();
		if (type != null) {
			if (!type.getParent().equals("0")) {
				ptypes.add(type.getParentType());
				List<Type> restptypes = getParentTypes(type.getParentType());
				if (restptypes.size() > 0) {
					ptypes.addAll(restptypes);
				}
			} else if (type.getTreenews() != null) {
				// 如果是分类树
				if (!"".equals(type.getTreenews())) {
					News anew = newsDAO.getNewsById(type.getTreenews());
					if (anew != null) {
						Type stype = new Type();
						stype.setName(anew.getTitle());
						stype.setHref(this.getHref());
						stype.setSite(anew.getSite());
						stype.setDisplay(1);
						ptypes.add(stype);
					}
					Type realType = typeDAO.getTypeById(anew.getType());
					if (realType != null) {
						ptypes.add(realType);
						List<Type> restptypes = getParentTypes(realType);
						if (restptypes.size() > 0) {
							ptypes.addAll(restptypes);
						}
					}
				}
			}
		}
		return ptypes;
	}

	/**
	 * 取得根频道 如果是频道树，则返回调用树的第一个根频道
	 * 
	 * @return 根频道
	 */
	public Type getRootType() {
		if (this.getParent().equals("0") || this.getParent().equals("")) {
			// 如果是分类树
			if (this.getTree_id().length() > 0) {
				List<Type> types = typeDAO.getTypesByTree(this.getTree_id(), 1);
				if (types.size() > 0) {
					return types.get(0).getRootType();
				} else {
					return this;
				}
			} else {
				return this;
			}
		} else {
			Type ptype = this.getParentType();
			return ptype.getRootType();
		}
	}

	/**
	 * 取得频道名称路径，形如"/首页/新闻/国际新闻"
	 * 
	 * @return 频道名称路径
	 */
	public String getTypeNamePath() {
		String path = "";
		if(this.getTreenews()!=null){
			News treenews=newsDAO.getNewsById(this.getTreenews());
			Type realParentType=typeDAO.getTypeById(treenews.getType());
			path+=realParentType.getTypeNamePath()+"/"+treenews.getTitle();
		}
		path += "/" + this.getName();
		if (!this.getParent().equals("0")) {
			Type ptype = this.getParentType();
			if (ptype != null) {
				path = ptype.getTypeNamePath() + path;
			}
		}
		return path;
	}

	/**
	 * 取得频道编号路径，形如"/111/222/333"
	 * 
	 * @return 频道编号路径
	 */
	public String getTypeIdPath() {
		String path = "";
		if(this.getTreenews()!=null){
			News treenews=newsDAO.getNewsById(this.getTreenews());
			Type realParentType=typeDAO.getTypeById(treenews.getType());
			path+=realParentType.getTypeIdPath()+"/"+treenews.getId();
		}
		path += "/" + this.getId();
		if (!"0".equals(this.getParent())) {
			Type ptype = this.getParentType();
			if (ptype != null) {
				path = ptype.getTypeIdPath() + path;
			}
		}
		return path;
	}

	/**
	 * 
	 * @return
	 */
	public String getTypeCNSpellPath() {
		String path = "";
		if(this.getTreenews()!=null){
			News treenews=newsDAO.getNewsById(this.getTreenews());
			Type realParentType=typeDAO.getTypeById(treenews.getType());
			path+=realParentType.getTypeCNSpellPath()+"/"+treenews.getCNSpellTitle();
		}
		path += "/" + PinYinUtil.cn2FirstSpell(this.getName());
		if (!this.getParent().equals("0")) {
			Type ptype = this.getParentType();
			if (ptype != null) {
				path = ptype.getTypeCNSpellPath() + path;
			}
		}
		return path;
	}

	/**
	 * 取得频道编辑用户
	 * 
	 * @return 用户列表
	 */
	public List<User> getAdminUsers() {
		List<User> userList = new ArrayList<User>();
		try {
			if (this.getAdmin() != null) {
				String[] unames = this.getAdmin().split(",");
				for (int i = 0; i < unames.length; i++) {
					if (unames[i].length() > 0) {
						// java.lang.System.out.println("seach userid:"+unames[i]);
						User auser = userDAO.getUserById(unames[i]);
						if (auser != null) {
							// java.lang.System.out.println("add user:"+auser.getUsername());
							userList.add(auser);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return userList;
	}

	public String getFlowName(){
		if (this.getFlow() != null) {
			Flow flow= flowDao.getFlowById(this.getFlow());
			if(flow!=null){
				return flow.getName();
			}else{return null;}
		}else{
			return null;
		}
	}
	/**
	 * 取得频道审核员用户
	 * 
	 * @return 用户列表
	 */
	public List<User> getAuditUsers() {
		List<User> userList = new ArrayList<User>();
		try {
			if (this.getAudit() != null) {
				String[] unames = this.getAudit().split(",");
				for (int i = 0; i < unames.length; i++) {
					if (unames[i].length() > 0) {
						User auser = userDAO.getUserById(unames[i]);
						if (auser != null) {
							java.lang.System.out.println("add user:"
									+ auser.getUsername());
							userList.add(auser);
						}
					}
				}
			}
		} catch (Exception e) {
			return null;
		}
		return userList;
	}

	/**
	 * 取得自定义表单
	 * 
	 * @return 表单对象
	 */
	public Form getUserForm() {
		if (this.getForm() != null) {
			return formDAO.getFormById(this.getForm());
		} else {
			return null;
		}
	}

	/**
	 * 取得分类树名称
	 * 
	 * @return 树名称
	 */
	public String getTreeName() {
		if (this.getTree() != null) {
			Tree atree = treeDAO.getTreeById(this.getTree());
			if (atree != null) {
				return atree.getName();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * 取得站点编号
	 * 
	 * @return 站点编号
	 */
	public String getSite() {
		return site;
	}

	/**
	 * 设置站点编号
	 * 
	 * @param site
	 *            站点编号
	 */
	public void setSite(String site) {
		this.site = site;
	}

	public int getNeedpub() {
		return needpub;
	}

	public void setNeedpub(int needpub) {
		this.needpub = needpub;
	}

	public int getHtml() {
		return html;
	}

	public void setHtml(int html) {
		this.html = html;
	}

	public int getHtml_rule() {
		return html_rule;
	}

	public void setHtml_rule(int html_rule) {
		this.html_rule = html_rule;
	}

	public int getHtml_dir() {
		return html_dir;
	}

	public void setHtml_dir(int html_dir) {
		this.html_dir = html_dir;
	}

	public int getPubnum() {
		pubnum=newsDAO.getAllSum(1, "", this.getId(), "", "", "");
		return pubnum;
	}

	public void setPubnum(int pubnum) {
		this.pubnum = pubnum;
	}

	public int getTotalnum() {
		totalnum=newsDAO.getAllSum(-1, "", this.getId(), "", "", "");
		return totalnum;
	}

	public void setTotalnum(int totalnum) {
		this.totalnum = totalnum;
	}

	public List<Type> getBrotherTypes() {
		List<Type> atypes = new ArrayList<Type>();
		if (this.getParentType() != null) {
			atypes = this.getParentType().getSubTypes(1);
		} else {
			if (this.getTree_id() != null) {
				if (this.getTree_id().length() > 0) {
					List<Type> tempTypes = typeDAO.getAllTypes(
							this.getTree_id(), 1);
					for (Type atype : tempTypes) {
						atype.setSite(this.getSite());
						atype.setTreenews(this.getTreenews());
						atype.setHref(atype.getHref());
						atypes.add(atype);
					}
				} else {
					atypes = typeDAO.getRootTypes(this.getSite(), 1);
				}
			} else {
				atypes = typeDAO.getRootTypes(this.getSite(), 1);
			}
		}
		return atypes;
	}
}