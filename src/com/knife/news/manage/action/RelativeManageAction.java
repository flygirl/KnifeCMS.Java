package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.NewsService;
import com.knife.news.logic.RelativeService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.RelativeServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.News;
import com.knife.news.object.Relative;
import com.knife.news.object.Type;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class RelativeManageAction extends ManageAction {
	private RelativeService relativeDAO = RelativeServiceImpl.getInstance();
	private NewsService newsDAO = NewsServiceImpl.getInstance();
	private TypeService typeDAO = TypeServiceImpl.getInstance();
	private Type thisType = new Type();
	private String tid = "";
	private String treenews = "";

	public Page doInit(WebForm form, Module module) {
		return doChoosed(form, module);
	}
	
	public Page doIndex(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("newsid"));
		form.addResult("newsid", newsid);
		return module.findPage("index");
	}
	
	public Page doChoosed(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		String newsid = CommUtil.null2String(form.get("newsid"));
		List<News> listNews = new ArrayList<News>();
		try{
			Relative rel = relativeDAO.getRelativeByNewsId(newsid);
			String[] rel_ids=rel.getRels().split(",");
			for(Object news_id:rel_ids){
				if(news_id.toString().length()>0){
					News anews=newsDAO.getNewsById(news_id.toString());
					listNews.add(anews);
				}
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		form.addResult("newsid", newsid);
		form.addResult("newsList", listNews);
		return module.findPage("choosed");
	}
	
	public Page doList(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("newsid"));
		form.addResult("newsid", newsid);
		if (tid.equals("")) {
			tid = CommUtil.null2String(form.get("tid"));
		}
		if (treenews.equals("")) {
			treenews = CommUtil.null2String(form.get("treenews"));
		}
		if (popedom == 0) {
			if (!typeDAO.hasRightUser(tid, user.getId(),popedom)) {
				return new Page("noright", "/manage/user_index.html");
			}
		}
		String addCondition = "";
		String key_words = CommUtil.null2String(form.get("key_words"));
		if (key_words.length() > 0) {
			addCondition = " and (k_title like '%" + key_words
					+ "%' or k_content like '%" + key_words + "%')";
		}
		if (tid.length() > 0) {
			addCondition += " and k_type='" + tid + "'";
			form.addResult("tid", tid);
			thisType = typeDAO.getTypeById(tid);
			form.addResult("thisType", thisType);
		}
		if (treenews.length() > 0) {
			addCondition += " and k_treenews='" + treenews + "'";
			form.addResult("treenews", treenews);
		}
		List<News> allNews = newsDAO.getNews("id!=''" + addCondition);
		if (allNews == null) {
			allNews = new ArrayList<News>();
		}
		int rows = allNews.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = currentPage + 1;
		List<News> firstNews = new ArrayList<News>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		firstNews = newsDAO.getNewsBySql("id!=''" + addCondition
				+ " order by length(k_order) desc,k_order desc", null, 0,
				pageSize);
		form.addResult("key_words", key_words);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("newsList", firstNews);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("newsid"));
		form.addResult("newsid", newsid);
		if (tid.equals("")) {
			tid = CommUtil.null2String(form.get("tid"));
		}
		if (treenews.equals("")) {
			treenews = CommUtil.null2String(form.get("treenews"));
		}
		if (popedom == 0) {
			if (!typeDAO.hasRightUser(tid, user.getId(),popedom)) {
				return new Page("noright", "/manage/user_index.html");
			}
		}
		String addCondition = "";
		String key_words = CommUtil.null2String(form.get("key_words"));
		if (key_words.length() > 0) {
			addCondition = " and (k_title like '%" + key_words
					+ "%' or k_content like '%" + key_words + "%')";
		}
		if (tid.length() > 0) {
			addCondition += " and k_type='" + tid + "'";
			form.addResult("tid", tid);
			thisType = typeDAO.getTypeById(tid);
			form.addResult("thisType", thisType);
		}
		if (treenews.length() > 0) {
			addCondition += " and k_treenews='" + treenews + "'";
			form.addResult("treenews", treenews);
		}
		List<News> allNews = newsDAO.getNews("id!=''" + addCondition);
		int rows = allNews.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<News> newsList = new ArrayList<News>();
		/*
		 * if (frontPage <= 0) { frontPage=1; form.addResult("msg", "这是第一页了！");
		 * } if (nextPage >= totalPage+1) { nextPage=totalPage;
		 * form.addResult("msg", "这是最后一页了！"); }
		 */
		form.addResult("keywords", key_words);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			newsList = newsDAO.getNewsBySql("id!=''" + addCondition
					+ " order by length(k_order) desc,k_order desc", null,
					begin - 1, end);
		} else {
			newsList = newsDAO.getNewsBySql("id!=''" + addCondition
					+ " order by length(k_order) desc,k_order desc", null,
					begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("newsList", newsList);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("newsid"));
		String relid = CommUtil.null2String(form.get("relid"));
		Relative aRel=relativeDAO.getRelativeByNewsId(newsid);
		boolean ret=false;
		if(aRel!=null){
			aRel.setNews_id(newsid);
			if(aRel.getRels()!=null){
				aRel.setRels(aRel.getRels()+","+relid);
			}else{
				aRel.setRels(relid);
			}
			ret=relativeDAO.updateRelative(aRel);
		}else{
			aRel=new Relative(new com.knife.news.model.Relative());
			aRel.setNews_id(newsid);
			if(aRel.getRels()!=null){
				aRel.setRels(aRel.getRels()+","+relid);
			}else{
				aRel.setRels(relid);
			}
			ret=relativeDAO.saveRelative(aRel);
		}
		if (ret) {
			form.addResult("msg", "添加成功！");
			return doChoosed(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doChoosed(form, module);
		}
	}

	public Page doDelete(WebForm form, Module module) {
		String newsid =  CommUtil.null2String(form.get("newsid"));
		String relid = CommUtil.null2String(form.get("relid"));
		boolean ret = false;
		Relative aRel = relativeDAO.getRelativeByNewsId(newsid);
		String rels=aRel.getRels();
		if(relid.length()>0){
			rels=rels.replace(relid+",", "");
			rels=rels.replace(relid, "");
			aRel.setRels(rels);
		}
		ret=relativeDAO.updateRelative(aRel);
		if (ret) {
			form.addResult("msg", "删除成功！");
			return doChoosed(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doChoosed(form, module);
		}
	}
}
