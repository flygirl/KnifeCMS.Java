package com.knife.news.manage.action;

import java.io.File;

import com.knife.news.logic.CustomService;
import com.knife.news.logic.impl.CustomServiceImpl;
import com.knife.news.object.Custom;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class CustomManageAjaxAction extends ManageAction {
	private CustomService customDAO = CustomServiceImpl.getInstance();

	public Page doAdd(WebForm form, Module module) {
		return module.findPage("ajax");
	}
	
	public Page doRemove(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("id"));
		if (!newsid.equals("")) {
			Custom anews = customDAO.getCustomById(newsid);
			if (anews.getUrl() != null) { 
				//System.out.println("删除文件:"+attachFile);
				File originalFile = anews.getFile();
				if(originalFile.exists()){
					originalFile.delete();
					File pubFile=new File(originalFile.getAbsolutePath().replace("html", "wwwroot"));
					if(pubFile.exists()){
						pubFile.delete();
					}
				}
				// 更新数据库
				anews.setUrl(null);
				customDAO.updateCustom(anews);
			}
		}
		return null;
	}
}
