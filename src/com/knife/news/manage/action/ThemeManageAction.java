package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.ThemeService;
import com.knife.news.logic.impl.ThemeServiceImpl;
import com.knife.news.object.Theme;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class ThemeManageAction extends ManageAction {
	ThemeService themeDAO = ThemeServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Theme> allfilts = themeDAO.getAllTheme();
		if (allfilts != null) {
			int rows = allfilts.size();// 分页开始
			int pageSize = 15;
			int currentPage = 1;
			int frontPage = 0;
			int nextPage = 2;
			int totalPage = (int) Math
					.ceil((float) (rows) / (float) (pageSize));
			List<Theme> firstfilts = themeDAO.getThemeBySql("id!=''", null, 0,
					pageSize);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("themeList", firstfilts);
		}
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Theme> allfilts = themeDAO.getAllTheme();
		int rows = allfilts.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Theme> logList = new ArrayList<Theme>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			logList = themeDAO
					.getThemeBySql("id!=''", null, begin - 1, end);
		} else {
			logList = themeDAO.getThemeBySql("id!=''", null, begin - 1,
					pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("themeList", logList);
		return module.findPage("list");
	}

	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Theme theme = themeDAO.getThemeById(id);
		//System.out.println("edit...."+id + ",--"+theme.getName());
		form.addResult("t", theme);
		form.addResult("action", "update");
		return module.findPage("edit");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result = false;
		//System.out.println("获取到的ID是:" + id);
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Theme log = themeDAO.getThemeById(ids[i]);
				result = themeDAO.delTheme(log);
			}
		} else {
			Theme log = themeDAO.getThemeById(id);
			result = themeDAO.delTheme(log);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
		} else {
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}

	public Page doSave(WebForm form, Module module) {
		Theme log = (Theme) form.toPo(Theme.class);
		if (themeDAO.saveTheme(log)) {
			form.addResult("msg", "添加成功！");
		}
		return doList(form, module);
	}

	public Page doUpdate(WebForm form, Module module) {
		Theme log = (Theme) form.toPo(Theme.class);
		if (themeDAO.updateTheme(log)) {
			form.addResult("msg", "编辑成功！");
		}
		return doList(form, module);
	}
}
