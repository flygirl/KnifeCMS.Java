package com.knife.news.manage.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.News;
import com.knife.news.object.Site;
import com.knife.news.object.Type;
import com.knife.service.HTMLGenerater;
import com.knife.tools.string.SearchStringUtil;

public class NewsGenerate extends Thread {
	private static NewsGenerate ng;
	private SiteServiceImpl siteDAO = SiteServiceImpl.getInstance();
	private TypeServiceImpl typeDAO = TypeServiceImpl.getInstance();
	private News news;
	private Lock alock;
	private int priority=1;
	
	public static NewsGenerate getInstance(News news,Lock mylock,int apriority){
		ng=new NewsGenerate(news,mylock,apriority);
		return ng;
	}
	
    public NewsGenerate(News news,Lock mylock,int apriority) {
    	super("generate"+news.getId());
    	priority=apriority;
    	this.setPriority(priority);
    	this.news=news;
    	alock=mylock;
    }
    
    public void run() {
        System.out.println(getName() + " 线程运行开始!");
        try{
		alock.lock();
        Type type= typeDAO.getTypeById(news.getType());
        Site site = siteDAO.getSiteById(type.getSite());
		if (site.getHtml() > 0) {
			//if publish task is running,stop it first.
			KnifeThreadPool.getPublish().stop=true;
			HTMLGenerater generater = new HTMLGenerater(site.getId());
			if(type.getHtml()>0){
				type.setNeedpub(1);
				typeDAO.updateType(type);
			}
			//直接调用的频道
			List<Type> ptypes=type.getParentTypes();
			if(ptypes.size()>0){
				for(Type atype:ptypes){
					if(atype.getHtml()>0){
						atype.setNeedpub(1);
						typeDAO.updateType(atype);
					}
				}
			}
			//交叉调用的频道
			//查找调用此文章的模板
			SearchStringUtil fgen = new SearchStringUtil();
	        String[] templateTypes={"htm","html","xml"};
	        //先搜索inc文件夹
	        List<String> incNames=fgen.findStr(news.getType(), templateTypes, generater.getTemplatePath()+site.getTemplate()+"/inc/");
	        List<String> retInc=new ArrayList<String>();
	        List<String> ret=new ArrayList<String>();
			if(incNames.size()>0){
				for(String incName:incNames){
					retInc=fgen.findStr("('/inc/"+incName+"')", templateTypes, generater.getTemplatePath()+site.getTemplate()+"/");
				}
			}
			List<String> retHtm=fgen.findStr(news.getType(), templateTypes, generater.getTemplatePath()+site.getTemplate()+"/");
			if(retInc.size()>0){
				ret.addAll(retInc);
				ret.removeAll(retHtm);
				ret.addAll(retHtm);
			}else if(retHtm.size()>0){
				ret.addAll(retHtm);
				ret.removeAll(retInc); 
				ret.addAll(retInc);
			}
			//找到使用了模板的频道
			List<Type> gTypes=new ArrayList<Type>();
			if(ret.size()>0){
				for(String template:ret){
					List<Type> gtypes=typeDAO.getTypesByTemplate(template);
					gTypes.removeAll(gtypes);
					gTypes.addAll(gtypes);
				}
			}
			if(gTypes.size()>0){
				for(Type gtype:gTypes){
					if(gtype.getNeedpub()==0){
						gtype.setNeedpub(1);
						typeDAO.updateType(gtype);
						System.out.println("需要更新的频道:"+gtype.getName());
					}
				}
			}
			if(type.getHtml()>0){
				if(news.getId()!=null){
					generater.saveNewsToHtml(news.getId());
				}
				generater.saveTypeIndexToHtml(news.getType(),"");
			}
			generater.saveIndexToHtml();
		}
		alock.unlock();
		}catch(Exception e){
			System.out.println(getName() + " 线程运行异常:"+e.getMessage());
		}
        System.out.println(getName() + " 线程运行结束!");
    }
}
