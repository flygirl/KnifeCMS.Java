package com.knife.tools;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.knife.news.action.SingletonClient;

/**
 * 短信处理类
 * 
 * @author Smith
 * 
 */
public class SMSUtil {
	private static int type = 0;// 短信接口类型:0 亿美,1 91短信通
	private static boolean ret = false;

	public static int getType() {
		return type;
	}

	public static void setType(int type) {
		SMSUtil.type = type;
	}

	public static boolean send(String mobile, String msg) {
		SMSUtil book = new SMSUtil();
		for (int i = 0; i < book.getClass().getMethods().length; i++) {
			Method f = book.getClass().getMethods()[i];
			if (("send" + getType()).equals(f.getName())) {
				try {
					Object result = f
							.invoke(book, new Object[] { mobile, msg });
					ret = Boolean.parseBoolean(result.toString());
				} catch (Exception e) {
					ret = false;
					e.printStackTrace();
				}
			}
		}
		return ret;
	}

	// 亿美短信发送
	public static boolean send0(String mobile, String msg) {
		try {
			int sendSmsSucOrFail = SingletonClient.getClient().sendSMS(
					new String[] { mobile }, msg, 3);
			ret = (sendSmsSucOrFail == 0) ? true : false;
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

	// 91短信通发送
	public static boolean send1(String mobile, String msg) {
		String url = "http://125.208.3.91:8888/sms.aspx";
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", "4042");
		params.put("account", "liujikuan4");
		params.put("password", "c1234567");
		params.put("mobile", mobile);
		params.put("content", msg);
		params.put("action", "send");
		try {
			ret = SMSUtil.sendPostRequest(url, params, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		}
		return ret;
	}

	public static boolean sendPostRequest(String path,
			Map<String, String> params, String enc) throws Exception {
		// title=dsfdsf&timelength=23&method=save
		StringBuilder sb = new StringBuilder();
		if (params != null && !params.isEmpty()) {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				sb.append(entry.getKey()).append('=').append(
						URLEncoder.encode(entry.getValue(), enc)).append('&');
			}
			sb.deleteCharAt(sb.length() - 1);
		}
		byte[] entitydata = sb.toString().getBytes();// 得到实体的二进制数据
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setConnectTimeout(5 * 1000);
		conn.setDoOutput(true);// 如果通过post提交数据，必须设置允许对外输出数据
		// 至少要设置的两个请求头
		// Content-Type: application/x-www-form-urlencoded //内容类型
		// Content-Length: 38 //实体数据的长度
		conn.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");//
		// 长度是实体的二进制长度
		conn.setRequestProperty("Content-Length", String
				.valueOf(entitydata.length));

		OutputStream outStream = conn.getOutputStream();
		outStream.write(entitydata);
		outStream.flush();
		outStream.close();
		conn.connect();
		if (conn.getResponseCode() == 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			String line = br.readLine(); 
			while(line != null){ 
				System.out.println(line); 
				line = br.readLine();
			}
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		SMSUtil.setType(1);
		SMSUtil.send("13911415506", "这是一封测试短信");
	}
}
