package com.knife.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class SessionUtil implements HttpSessionAttributeListener {
	private static List<String> list = new ArrayList<String>();// 存放用户
	private static Map<String, HttpSession> sessionList = new HashMap<String, HttpSession>();// 存放用户session

	public void attributeAdded(HttpSessionBindingEvent bindingevent) {
		//System.out.println("bindingevent.getName()"+bindingevent.getName());
		if ("loginuser".equals(bindingevent.getName())) {
			list.add((String) bindingevent.getValue());
			sessionList.put((String) bindingevent.getValue(),
					bindingevent.getSession());
		}
	}

	public void attributeRemoved(HttpSessionBindingEvent bindingevent) {
		if ("loginuser".equals(bindingevent.getName())) {
			list.remove(bindingevent.getValue());
			sessionList.remove(bindingevent.getValue());
		}
	}

	public void attributeReplaced(HttpSessionBindingEvent bindingevent) {
		
	}

	public static List<String> getUserList() {
		return list;
	}
	
	public static void RemoveUser(String username){
		list.remove(username);
	}

	public static Map<String, HttpSession> getUserSessions() {
		return sessionList;
	}
}