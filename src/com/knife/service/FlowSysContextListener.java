package com.knife.service;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;

public class FlowSysContextListener extends HttpServlet implements ServletContextListener {

//	public ContextListener(){}
	private Timer timer = null;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		timer = new Timer(true);
		sce.getServletContext().log("工作流已启动。");
		
		//默认的情况下是调用run()方法，
		//定时器调度，FlowWorkTask是自定义需要调度的执行任务（就是报表计算引擎入口java.util.TimerTask继承，第三个参数表示每小时（ 60*60*1000）被触发一次），中间参数0表示无延迟
		
		timer.schedule(new FlowNodeTask(sce.getServletContext()), 0, 60*60*1000);
		sce.getServletContext().log("已经添加任务调度表");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		timer.cancel();
		sce.getServletContext().log("工作流定时器销毁");
	}

}
