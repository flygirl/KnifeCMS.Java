package com.knife.member;

import java.sql.Timestamp;

/**
 * Meeting entity. @author MyEclipse Persistence Tools
 */

public class Meeting implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -5696888350007058774L;
	private Integer id;
	private String title;
	private String host;
	private String meetingid;
	private Timestamp starttime;
	private Timestamp endtime;
	private Boolean isclosed;

	// Constructors

	/** default constructor */
	public Meeting() {
	}

	/** minimal constructor */
	public Meeting(String title, Boolean isclosed) {
		this.title = title;
		this.isclosed = isclosed;
	}

	/** full constructor */
	public Meeting(String title, String host, Timestamp starttime,
			Timestamp endtime, Boolean isclosed) {
		this.title = title;
		this.host = host;
		this.starttime = starttime;
		this.endtime = endtime;
		this.isclosed = isclosed;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getMeetingid() {
		return this.meetingid;
	}

	public void setMeetingid(String meetingid) {
		this.meetingid = meetingid;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Boolean getIsclosed() {
		return this.isclosed;
	}

	public void setIsclosed(Boolean isclosed) {
		this.isclosed = isclosed;
	}

}