package com.knife.member;



/**
 * Risktype entity. @author MyEclipse Persistence Tools
 */

public class Risktype  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -8572280615588389092L;
	private Integer id;
     private String type;


    // Constructors

    /** default constructor */
    public Risktype() {
    }

    
    /** full constructor */
    public Risktype(String type) {
        this.type = type;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
   








}