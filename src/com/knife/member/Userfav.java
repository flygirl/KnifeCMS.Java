package com.knife.member;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Userfav entity. @author MyEclipse Persistence Tools
 */

public class Userfav implements java.io.Serializable {

	private static final long serialVersionUID = 1457091910201447148L;
	private Integer id;
	private Integer userid;
	private String docid;
	private Integer type;
	private Timestamp favdate;

	// Constructors

	/** default constructor */
	public Userfav() {
	}

	/** full constructor */
	public Userfav(Integer userid, String docid) {
		this.userid = userid;
		this.docid = docid;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getDocid() {
		return this.docid;
	}

	public void setDocid(String docid) {
		this.docid = docid;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
	public Timestamp getFavdate() {
		return this.favdate;
	}

	public void setFavdate(Timestamp favdate) {
		this.favdate = favdate;
	}
}