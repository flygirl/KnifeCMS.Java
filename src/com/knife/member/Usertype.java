package com.knife.member;

import java.util.List;

/**
 * Usertype entity. @author MyEclipse Persistence Tools
 */

public class Usertype implements java.io.Serializable {
	
	private UsertypeDAO utDAO=new UsertypeDAO();

	// Fields

	private Integer id;
	private String name;
	private Integer rights;
	private Integer parent;
	private Integer onscore;
	private Integer macnum;
	private Integer order;

	// Constructors

	/** default constructor */
	public Usertype() {
	}

	/** minimal constructor */
	public Usertype(Integer rights) {
		this.rights = rights;
	}

	/** full constructor */
	public Usertype(String name, Integer rights) {
		this.name = name;
		this.rights = rights;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRights() {
		return this.rights;
	}

	public void setRights(Integer rights) {
		this.rights = rights;
	}
	
	public Integer getParent() {
		return this.parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public Integer getOnscore() {
		return this.onscore;
	}

	public void setOnscore(Integer onscore) {
		this.onscore = onscore;
	}
	
	public Integer getMacnum() {
		return this.macnum;
	}

	public void setMacnum(Integer macnum) {
		this.macnum = macnum;
	}
	
	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public boolean isLeaf(){
		if(utDAO.findByParent(this.getId()).size()>0){
			return false;
		}else{
			return true;
		}
	}
	
	public List<Usertype> getSubTypes(){
		return utDAO.findByParent(this.getId());
	}
}