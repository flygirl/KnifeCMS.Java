package com.knife.member;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.PicService;
import com.knife.news.logic.PicTypeService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.impl.PicServiceImpl;
import com.knife.news.logic.impl.PicTypeServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.Pic;
import com.knife.news.object.PicType;
import com.knife.news.object.Type;

/**
 * Userinfo entity. @author MyEclipse Persistence Tools
 */
public class Userinfo implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -259155125444480063L;
	
	// Fields
    private Long id;
    private String cstguid;
    private String memguid;
    private String idcard;
    private String acount;
    private String type;
    private String password;
    private String realname;
    private Timestamp birthday;
    private Timestamp sysj;
    private Boolean sex;
    private String education;
    private String professional;
    private String email;
    private String phone;
    private String mobile;
    private String organization;
    private String area;
	private String department;
    private String position;
    private String location;
    private String zipcode;
    private int isfee;
    private int isadmin;
    private int adminnum;
    private int usednum;
    private int score;
    private String mac;
    private int active;
	private String hobby;
	private String salary;
    private String attend;
    private String exhibition;
    private String receive;
    private int ismarry;
    private String spouse;
    private String family;
    private String workarea;
    private String unit;
    private String homearea;
    private String officephone;
    private int logincount;
    private double ljpoint;
    private double ljsjpoint;
    private double ljddhpoint;
    private String country;
    private String homeaddress;
    private String childage;
    private String traffictool;
    private String companyname;
    private String companynature;
    private String buytarget;
    private String buyuse;
    private String payway;
    private String selectreason;
    private String buyagain;
    private String attitude;
    private String joinreason;
    private String avatar;
    private int count;
    private int fans;
    private int attention;
    private int favorite;
    private int article;
    
    //private static UserinfoDAO userDAO=new UserinfoDAO();
    private static TypeService typeDAO=TypeServiceImpl.getInstance();
    private static PicService picDAO=PicServiceImpl.getInstance();
    private static PicTypeService picTypeDAO=PicTypeServiceImpl.getInstance();
    
    public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	} 
   
    public String getJoinreason() {
		return joinreason;
	}

	public void setJoinreason(String joinreason) {
		this.joinreason = joinreason;
	}

	public String getAttitude() {
		return attitude;
	}

	public void setAttitude(String attitude) {
		this.attitude = attitude;
	}

	public String getBuyagain() {
		return buyagain;
	}

	public void setBuyagain(String buyagain) {
		this.buyagain = buyagain;
	}

	public String getSelectreason() {
		return selectreason;
	}

	public void setSelectreason(String selectreason) {
		this.selectreason = selectreason;
	}

	public String getPayway() {
		return payway;
	}

	public void setPayway(String payway) {
		this.payway = payway;
	}

	public String getBuytarget() {
		return buytarget;
	}

	public void setBuytarget(String buytarget) {
		this.buytarget = buytarget;
	}

	public String getBuyuse() {
		return buyuse;
	}

	public void setBuyuse(String buyuse) {
		this.buyuse = buyuse;
	}

	public String getCompanynature() {
		return companynature;
	}

	public void setCompanynature(String companynature) {
		this.companynature = companynature;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getChildage() {
		return childage;
	}

	public void setChildage(String childage) {
		this.childage = childage;
	}

	public String getTraffictool() {
		return traffictool;
	}

	public void setTraffictool(String traffictool) {
		this.traffictool = traffictool;
	}

	public String getHomeaddress() {
		return homeaddress;
	}

	public void setHomeaddress(String homeaddress) {
		this.homeaddress = homeaddress;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCstguid() {
		return cstguid;
	}

	public void setCstguid(String cstguid) {
		this.cstguid = cstguid;
	}

	public String getMemguid() {
		return memguid;
	}

	public void setMemguid(String memguid) {
		this.memguid = memguid;
	}

	public double getLjpoint() {
		return ljpoint;
	}

	public void setLjpoint(double ljpoint) {
		this.ljpoint = ljpoint;
	}

	public double getLjsjpoint() {
		return ljsjpoint;
	}

	public void setLjsjpoint(double ljsjpoint) {
		this.ljsjpoint = ljsjpoint;
	}

	public double getLjddhpoint() {
		return ljddhpoint;
	}

	public void setLjddhpoint(double ljddhpoint) {
		this.ljddhpoint = ljddhpoint;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setLogincount(int logincount){
    	this.logincount = logincount;
    }
    public int getLogincount(){
    	return logincount;
    }
    public String getBestway() {
		return bestway;
	}

	public void setBestway(String bestway) {
		this.bestway = bestway;
	}

	public String getIncomingway() {
		return incomingway;
	}

	public void setIncomingway(String incomingway) {
		this.incomingway = incomingway;
	}

	public Timestamp getRegdate() {
		return regdate;
	}

	public void setRegdate(Timestamp regdate) {
		this.regdate = regdate;
	}

	private String fax;
    private String rejection;
    private String bestway;
    private String incomingway;
    private Timestamp regdate;
    private int identity;
    public int getIdentity() {
		return identity;
	}

	public void setIdentity(int identity) {
		this.identity = identity;
	}

	// Constructors
    /** default constructor */
    public Userinfo() {
    }

    public Userinfo(int isfee) {
        this.isfee = isfee;
    }

    /** full constructor */
    public Userinfo(String idcard,String acount, String password, String realname,
            Timestamp brithdate, Boolean sex, String education,
            String professional, String email, String phone, String mobile,
            String organization, String department, String position,
            String location, String zipcode, int isfee,int isadmin,int adminnum,int usednum,int score,String mac,int active,
            String hobby,String salary,String attend,String exhibition,String receive,String avatar,int fans,int attention,int article,int favorite) {
    	this.idcard = idcard;
        this.acount = acount;
        this.password = password;
        this.realname = realname;
        this.birthday = brithdate;
        this.sysj = sysj;
        this.sex = sex;
        this.education = education;
        this.professional = professional;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
        this.organization = organization;
        this.department = department;
        this.position = position;
        this.location = location;
        this.zipcode = zipcode;
        this.isfee = isfee;
        this.isadmin = isadmin;
        this.adminnum = adminnum;
        this.usednum = usednum;
        this.score = score;
        this.mac = mac;
        this.active = active;
        this.hobby=hobby;
        this.salary=salary;
        this.attend=attend;
        this.exhibition=exhibition;
        this.receive=receive;
        this.avatar=avatar;
        this.fans = fans;
        this.attention=attention;
        this.article=article;
        this.favorite=favorite;
    }

    // Property accessors
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getIdcard() {
        return this.idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getAcount() {
        return this.acount;
    }

    public void setAcount(String acount) {

        this.acount = acount;
    }

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return this.realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Timestamp getBirthday() {
        return this.birthday;
    }
    public Timestamp getSysj() {
        return this.sysj;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }
    public void setSysj(Timestamp sysj) {
        this.sysj = sysj;
    }

    public void setBirthday(String birthday) {
        this.birthday = Timestamp.valueOf(birthday);
    }
    public void setSysj(String sysj) {
        this.sysj = Timestamp.valueOf(sysj);
    }

    public Boolean getSex() {
        return this.sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public String getEducation() {
        return this.education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getProfessional() {
        return this.professional;
    }

    public void setProfessional(String professional) {
        this.professional = professional;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOrganization() {
        return this.organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDepartment() {
        return this.department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZipcode() {
        return this.zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public int getIsfee() {
        return this.isfee;
    }

    public void setIsfee(int isfee) {
        this.isfee = isfee;
    }
    
    public int getIsadmin() {
        return this.isadmin;
    }

    public void setIsadmin(int isadmin) {
        this.isadmin = isadmin;
    }
    
    public int getAdminnum() {
        return this.adminnum;
    }

    public void setAdminnum(int adminnum) {
        this.adminnum = adminnum;
    }
    
    public int getUsednum() {
    	//this.usednum=userDAO.findAllDepartment(this.department).size()-1;
    	//if(this.usednum<0){this.usednum=0;}
        return this.usednum;
    }

    public void setUsednum(int usednum) {
        this.usednum = usednum;
    }
    
    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    public String getMac() {
        return this.mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
    
    public int getActive() {
        return this.active;
    }

    public void setActive(int active) {
        this.active = active;
    }
    
    public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	
    public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getAttend() {
		return attend;
	}

	public void setAttend(String attend) {
		this.attend = attend;
	}

	public String getExhibition() {
		return exhibition;
	}

	public void setExhibition(String exhibition) {
		this.exhibition = exhibition;
	}

	public String getReceive() {
		return receive;
	}

	public void setReceive(String receive) {
		this.receive = receive;
	}

	public int getIsmarry() {
		return ismarry;
	}

	public void setIsmarry(int ismarry) {
		this.ismarry = ismarry;
	}

	public String getSpouse() {
		return spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getWorkarea() {
		return workarea;
	}

	public void setWorkarea(String workarea) {
		this.workarea = workarea;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getHomearea() {
		return homearea;
	}

	public void setHomearea(String homearea) {
		this.homearea = homearea;
	}

	public String getOfficephone() {
		return officephone;
	}

	public void setOfficephone(String officephone) {
		this.officephone = officephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getRejection() {
		return rejection;
	}

	public void setRejection(String rejection) {
		this.rejection = rejection;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	
	/**
	 * @return the fans
	 */
	public int getFans() {
		return fans;
	}

	/**
	 * @param fans the fans to set
	 */
	public void setFans(int fans) {
		this.fans = fans;
	}

	/**
	 * @return the attention
	 */
	public int getAttention() {
		return attention;
	}

	/**
	 * @param attention the attention to set
	 */
	public void setAttention(int attention) {
		this.attention = attention;
	}

	/**
	 * @return the favorite
	 */
	public int getFavorite() {
		return favorite;
	}

	/**
	 * @param favorite the favorite to set
	 */
	public void setFavorite(int favorite) {
		this.favorite = favorite;
	}

	/**
	 * @return the article
	 */
	public int getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(int article) {
		this.article = article;
	}

	/**
	 * 取得会员拥有权限的频道
	 * @return 频道列表
	 */
	public List<Type> getTypes(){
		List<Type> atypes=new ArrayList<Type>();
		if(this.getType()!=null){
		String[] types=this.getType().split(",");
		if(types.length>0){
			for(String tid:types){
				if(tid.length()>0){
					Type atype=typeDAO.getTypeById(tid);
					atypes.add(atype);
				}
			}
		}
		}
		return atypes;
	}

	/**
	 * 取得会员拥有权限的频道名称
	 * @return
	 */
	public String getTypeName(){
		String typename="";
		List<Type> types=getTypes();
		if(types!=null){
			for(Type atype:types){
				if(atype!=null){
					typename += atype.getName() + ",";
				}
			}
		}
		return typename;
	}
	
	public List<PicType> getPicTypes(){
		List<PicType> displayList=new ArrayList<PicType>();
		List<PicType> allList = picTypeDAO.getPicTypesByUser(this.getId().intValue());
		for(PicType apt:allList){
			if(apt.getCoverPic(1)!=null){
				displayList.add(apt);
			}
		}
		return displayList;
	}
	
	public List<PicType> getMyPicTypes(){
		return picTypeDAO.getPicTypesByUser(this.getId().intValue());
	}
	
	/**
	 * 取得会员图片
	 * @return
	 */
	public List<Pic> getPics(){
		return picDAO.getPicsByUserId(this.getId().intValue());		
	}
	
	/**
	 * 取得图片集下图片
	 * @param picTypeId
	 * @return
	 */
	public List<Pic> getPicsByPicType(String picTypeId){
		return getPicsByPicType(picTypeId,1);
	}
	
	/**
	 * 取得图片集下图片
	 * @param picTypeId
	 * @return
	 */
	public List<Pic> getPicsByPicType(String picTypeId,int display){
		return picDAO.getPicsByPicType(picTypeId,display);
	}
}