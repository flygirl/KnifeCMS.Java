--KnifeCMS 执行脚本 for Oracle
create or replace procedure proc_dropifexists(
    p_table in varchar2 
) is
    v_count number(10);
begin
   select count(*)
   into v_count
   from user_tables
   where table_name = upper(p_table);
   if v_count > 0 then
      execute immediate 'drop table '|| p_table ||' purge';
   end if;
end;
/

call proc_dropifexists('k_grab');
CREATE TABLE IF NOT EXISTS k_grab (
  id varchar2(32) default '' not null,
  k_title varchar(200) DEFAULT NULL,
  k_url varchar(255) DEFAULT NULL,
  k_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

call proc_dropifexists('k_grab_news');
CREATE TABLE k_grab_news (
  id varchar2(32) default '' not null,
  k_title varchar(200) DEFAULT NULL,
  k_source varchar(100) DEFAULT NULL,
  k_author varchar(45) DEFAULT NULL,
  k_url varchar(200) DEFAULT NULL,
  k_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  k_content nclob,
  primary key (id)
);

call proc_dropifexists('k_document');
create sequence seq_k_document start with 1 increment by 1;
CREATE TABLE k_document (
  id integer constraint seq_k_document primary key,
  k_title nvarchar2(200) DEFAULT NULL,
  k_source nvarchar2(100) DEFAULT NULL,
  k_author nvarchar2(45) DEFAULT NULL,
  k_risktype nvarchar2(32) DEFAULT NULL,
  k_documenttype nvarchar2(32) DEFAULT NULL,
  k_date timestamp NOT NULL,
  k_filetype varchar2(45) DEFAULT NULL,
  k_fileurl varchar2(300) DEFAULT NULL,
  k_isfee int NOT NULL
);

call proc_dropifexists('k_documenttype');
create sequence seq_k_documenttype start with 1 increment by 1;
CREATE TABLE k_documenttype (
  id integer constraint seq_k_documenttype primary key,
  k_type varchar2(100) DEFAULT NULL
);

call proc_dropifexists('k_english');
create sequence seq_k_english start with 1 increment by 1;
CREATE TABLE k_english (
  id integer constraint seq_k_english primary key,
  k_en varchar2(200) DEFAULT NULL,
  k_cn nvarchar2(200) DEFAULT NULL,
  k_means nclob,
  k_hot int DEFAULT '0'
);

call proc_dropifexists('k_history');
create sequence seq_k_history start with 1 increment by 1;
CREATE TABLE k_history (
  id integer constraint seq_k_history primary key,
  k_usermail varchar2(100) DEFAULT NULL,
  k_ip varchar2(50) DEFAULT NULL,
  k_os varchar2(50) DEFAULT NULL,
  k_browser varchar2(50) DEFAULT NULL,
  k_url varchar2(100) DEFAULT NULL,
  k_accessdate date,
  k_accesstime timestamp,
  k_endtime timestamp,
  k_type int DEFAULT '0'
);

call proc_dropifexists('k_comment');
create table k_comment (
  id varchar2(16) default '' not null,
  k_user varchar2(16) default null,
  k_date timestamp,
  k_news_id varchar2(16) default null,
  k_comment nclob,
  k_display int DEFAULT '0',
  primary key (id)
);

call proc_dropifexists('k_custom');
create table k_custom(
  id varchar2(32) default '' NOT NULL,
  k_site varchar2(32) DEFAULT NULL,
  k_name nvarchar2(100) DEFAULT NULL,
  k_date timestamp NOT NULL,
  k_url varchar2(200) DEFAULT NULL,
  k_status char(1) DEFAULT 0,
  k_description nclob,
  primary key (id)
);

call proc_dropifexists('k_fields');
create table k_fields(
  id varchar2(16) default '' not null,
  k_name nvarchar2(100),
  k_formid varchar2(16),
  k_newsid varchar2(16),
  k_order varchar2(16),
  k_value nclob,
  primary key (id)
);

call proc_dropifexists('k_filter');
CREATE TABLE k_filter (
  id varchar2(16) DEFAULT '' NOT NULL,
  k_name nvarchar2(32) DEFAULT NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_flow');
CREATE TABLE k_flow (
  id varchar2(32) DEFAULT '' NOT NULL,
  k_site varchar2(32) DEFAULT NULL,
  k_name nvarchar2(100) DEFAULT NULL,
  k_date timestamp NOT NULL,
  k_url varchar(200) DEFAULT NULL,
  k_status char(1) DEFAULT 0,
  k_description text,
  k_type int,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_form');
create table k_form
(
  id varchar2(16) default '' not null,
  k_name nvarchar2(200) DEFAULT NULL,
  k_content nclob,
  primary key (id)
);

call proc_dropifexists('k_userhistory');
create sequence seq_k_userhistory start with 1 increment by 1;
create table k_userhistory (
  id integer constraint seq_k_userhistory primary key,
  k_usermail varchar2(100) default null,
  k_ip varchar2(50) default null,
  k_os varchar2(50) default null,
  k_browser varchar2(50) default null,
  k_url varchar2(100) default null,
  k_accessdate date,
  k_accesstime timestamp,
  k_endtime timestamp
);

call proc_dropifexists('k_logs');
CREATE TABLE k_logs (
  id varchar2(16) default '' NOT NULL,
  k_content nvarchar2(255) NULL,
  k_date timestamp NOT NULL
);

call proc_dropifexists('k_news');
create table k_news (
  id varchar2(16) default '' not null,
  k_title nvarchar2(100) not null,
  k_source nvarchar2(100) default null,
  k_keywords nvarchar2(100) default null,
  k_file varchar2(200) default null,
  k_content nclob not null,
  k_date date default null,
  k_username nvarchar2(32) default null,
  k_type varchar2(16) default null,
  k_link varchar2(16) default null,
  k_commend varchar2(6) default null,
  k_template varchar2(32) default null,
  k_order varchar2(16) default null,
  k_display char(1) default '0',
  k_tofront char(1) default '0',
  k_count number default 0,
  k_treenews varchar2(16) DEFAULT NULL,
  k_docfile varchar2(200) DEFAULT NULL,
  k_isfee char(1) DEFAULT '0',
  k_type2 varchar2(255) DEFAULT NULL,
  k_doctype varchar2(16) DEFAULT NULL,
  k_summary nclob,
  k_rights int DEFAULT '0',
  k_author nvarchar2(100) DEFAULT NULL,
  k_score int DEFAULT '0',
  k_search char(1) DEFAULT '0',
  k_ispub char(1) DEFAULT '0',
  k_offdate timestamp NULL,
  k_xmldata nclob,
  primary key (id)
);

create or replace trigger t_add_news
  before insert
  on k_news
  for each row
declare
    -- local variables here
  begin
    select count(*) into :new.k_order from k_news where k_type=:new.k_type;
    :new.k_order:=:new.k_order+1;
  end;
/

call proc_dropifexists('k_news_version');
create table K_NEWS_VERSION
(
  id              VARCHAR2(36),
  k_versionnumber INTEGER,
  k_description   VARCHAR2(100),
  k_newsid        VARCHAR2(16),
  k_title         VARCHAR2(100),
  k_source        VARCHAR2(100),
  k_keywords      VARCHAR2(100),
  k_file          VARCHAR2(200),
  k_content       CLOB,
  k_date          DATE,
  k_username      VARCHAR2(32),
  k_type          VARCHAR2(16),
  k_link          VARCHAR2(16),
  k_commend       VARCHAR2(6),
  k_template      VARCHAR2(32),
  k_order         VARCHAR2(16),
  k_display       CHAR(1),
  k_tofront       CHAR(1),
  k_count         INTEGER,
  k_treenews      VARCHAR2(16),
  k_docfile       VARCHAR2(200),
  k_isfee         CHAR(1),
  k_type2         VARCHAR2(255),
  k_doctype       VARCHAR2(16),
  k_summary       CLOB,
  k_rights        INTEGER,
  k_author        VARCHAR2(100),
  k_score         INTEGER,
  k_search        CHAR(1),
  k_ispub         CHAR(1),
  k_offdate       DATE,
  k_flowusers     VARCHAR2(500),
  k_xmldata       CLOB,
  PRIMARY KEY (id)
);

call proc_dropifexists('K_WORKFLOW_NODE');
create table K_WORKFLOW_NODE
(
  id               VARCHAR2(32),
  k_name           VARCHAR2(200),
  k_type           INTEGER,
  k_workflow_id    VARCHAR2(32),
  k_step           INTEGER,
  k_init_function  VARCHAR2(200),
  k_trans_function VARCHAR2(200),
  k_run_function   VARCHAR2(200),
  k_save_function  VARCHAR2(200),
  k_prev_node      VARCHAR2(32),
  k_next_node      VARCHAR2(32),
  k_executor       VARCHAR2(200),
  k_execute_type   INTEGER,
  k_remind         INTEGER,
  k_field          VARCHAR2(200),
  k_max_day        INTEGER,
  k_status         INTEGER,
  PRIMARY KEY (id)
)
;


call proc_dropifexists('k_content');
CREATE TABLE k_content (
  id varchar2(32) not null,
  k_newsid varchar2(32) DEFAULT NULL,
  k_page int DEFAULT '0',
  k_content nclob,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_order');
CREATE TABLE k_order (
	id varchar2(16) NOT NULL,
	k_user varchar2(16) NOT NULL,
	k_title nvarchar2(150) not null,
	k_goods nvarchar2(200) NOT NULL,
	k_money int DEFAULT '0',
	k_date date NULL,
	k_status int DEFAULT '0' NOT NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_relative');
CREATE TABLE k_relative (
	id varchar2(16) NOT NULL,
	k_news_id varchar2(16) NOT NULL,
	k_rels varchar2(255) NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_pic');
create table k_pic (
  id varchar2(16),
  k_title nvarchar2(100),
  k_url varchar2(100),
  k_newsid varchar2(255),
  k_order varchar2(16),
  k_userid varchar2(16),
  k_date timestamp,
  k_pic_type varchar2(16) NULL,
  k_display int DEFAULT '0',
  primary key (id)
);

create or replace trigger t_add_pic
  before insert
  on k_pic
  for each row
declare
    -- local variables here
  begin
    select count(*) into :new.k_order from k_pic where k_newsid=:new.k_newsid;
    :new.k_order:=:new.k_order+1;
  end;
/

call proc_dropifexists('k_pic_type');
CREATE TABLE k_pic_type (
  id varchar2(32) NOT NULL,
  k_name varchar2(100) DEFAULT NULL,
  k_parent varchar2(16) DEFAULT '0',
  k_site varchar2(16) DEFAULT NULL,
  k_userid int DEFAULT NULL,
  k_date timestamp NOT NULL,
  k_order varchar2(16),
  k_description varchar2(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_video');
CREATE TABLE k_video (
  id varchar2(16) DEFAULT '' NOT NULL,
  k_title nvarchar2(100) DEFAULT NULL,
  k_url varchar2(100) DEFAULT NULL,
  k_newsid varchar2(255) DEFAULT NULL,
  k_order varchar2(16) DEFAULT NULL,
  k_userid varchar2(16),
  k_date timestamp,
  k_display int DEFAULT '0',
  PRIMARY KEY (id)
);

call proc_dropifexists('k_record');
CREATE TABLE k_record (
  id varchar2(16) NOT NULL,
  k_user_id varchar2(16) NOT NULL,
  k_date timestamp NOT NULL,
  k_operation nvarchar2(200) NOT NULL,
  k_news_id varchar2(16) NOT NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_role');
CREATE TABLE k_role (
  id varchar2(16) NOT NULL,
  k_name nvarchar2(100) NOT NULL,
  k_username nvarchar2(200) NOT NULL
);

call proc_dropifexists('k_subscribe');
create table k_subscribe (
  id varchar2(16) default '' not null,
  k_name nvarchar2(100),
  k_form nvarchar2(100),
  k_user nvarchar2(100),
  k_stype CHAR(1) NULL,
  k_mail VARCHAR2(150) NULL,
  k_phone VARCHAR2(50) NULL,
  primary key (id)
);

call proc_dropifexists('k_supplier');
CREATE TABLE k_supplier (
 id varchar2(16) NOT NULL,
 k_user varchar2(32) NULL,
 k_name nvarchar2(200) NULL,
 k_sid varchar2(20) NULL,
 k_sname nvarchar2(200) NULL,
 k_card varchar2(20) NULL,
 PRIMARY KEY (id)
);

call proc_dropifexists('k_bill');
CREATE TABLE k_bill (
 id varchar2(16) NOT NULL,
 k_suser varchar2(32) NULL,
 k_file varchar2(200) NULL,
 k_date date NULL,
 PRIMARY KEY (id)
);

call proc_dropifexists('k_theme');
CREATE TABLE k_theme (
  id varchar2(16) DEFAULT '' NOT NULL,
  k_name varchar2(50) DEFAULT NULL,
  k_description nvarchar2(200) DEFAULT NULL,
  k_css nclob,
  k_status char(1) DEFAULT '0',
  PRIMARY KEY (id)
);

call proc_dropifexists('k_thumbnail');
create table k_thumbnail (
  id varchar2(16) default '' not null,
  k_link varchar2(16),
  k_url varchar2(16),
  primary key (id)
);

call proc_dropifexists('k_tree');
CREATE TABLE k_tree (
  id varchar2(32) NOT NULL,
  k_name varchar2(100) DEFAULT NULL,
  k_tree_type varchar2(32) NOT NULL,
  k_value nclob,
  k_show char(1) DEFAULT '0',
  PRIMARY KEY (id)
);

call proc_dropifexists('k_type');
create table k_type (
  id varchar2(32) default '' not null,
  k_name nvarchar2(100) not null,
  k_parent varchar2(16) default null,
  k_value varchar2(255) default null,
  k_order varchar2(16) default null,
  k_index_template varchar(100) default null,
  k_type_template varchar2(100) default null,
  k_news_template varchar2(100) default null,
  k_form varchar2(100) default null,
  k_admin nvarchar2(200) default null,
  k_audit nvarchar2(200) DEFAULT NULL,
  k_display char(1) DEFAULT '0',
  k_show char(1) default '0',
  k_ismember char(1) DEFAULT '0',
  k_url nvarchar2(200),
  k_target char(1) default '0',
  k_pagenum varchar2(16),
  k_pagesize int default 10,
  k_orderby varchar2(100),
  k_search char(1) DEFAULT '1',
  k_tree varchar(32) DEFAULT NULL,
  k_tree_id varchar(32) DEFAULT '',
  k_site varchar(16) DEFAULT NULL,
  k_needpub char(1) default '0',
  k_html char(1) default '0',
  k_html_rule char(1) default '0',
  k_html_dir char(1) default '0',
  primary key (id)
);

create or replace trigger t_add_type
  before insert
  on k_type
  for each row
declare
    -- local variables here
  begin
    select count(*) into :new.k_order from k_type where k_parent=:new.k_parent;
    :new.k_order:=:new.k_order+1;
    if :new.k_type_template is null then
    	:new.k_type_template:='list.html';
    end if;
    if :new.k_news_template is null then
    	:new.k_news_template:='news.html';
    end if;
  end;
/

call proc_dropifexists('k_type2');
CREATE TABLE k_type2 (
 id varchar2(16) NOT NULL,
 k_name varchar2(255) NOT NULL
);

call proc_dropifexists('k_site');
create table k_site
(
  id varchar2(16) default '' not null,
  k_name nvarchar2(100),
  k_domain varchar2(200),
  k_admin nvarchar2(200),
  k_lang varchar2(8),
  k_html char(1) default '0',
  k_html_rule char(1) default '0',
  k_html_dir char(1) default '0',
  k_mailserver varchar2(200),
  k_mailuser nvarchar2(100),
  k_mailpass nvarchar2(100),
  k_isthumb char(1) DEFAULT '0',
  k_order varchar2(16) DEFAULT NULL,
  k_pub_dir varchar2(100) DEFAULT NULL,
  k_template varchar2(100) DEFAULT NULL,
  k_big5 char(1) DEFAULT '0',
  k_index varchar2(100) DEFAULT NULL,
  k_theme varchar2(32) DEFAULT NULL,
  k_css_file varchar2(100) DEFAULT NULL,
  primary key (id)
);
create or replace trigger t_add_site
  before insert
  on k_site
  for each row
declare
    -- local variables here
  begin
    select count(*) into :new.k_order from k_site;
    :new.k_order:=:new.k_order+1;
  end;
/

call proc_dropifexists('k_user');
create table k_user (
  id varchar2(16) default '' not null,
  k_username nvarchar2(32) not null,
  k_password varchar2(32) not null,
  k_email varchar2(32) default null,
  k_qq varchar2(32) default null,
  k_birthday date default null,
  k_sysj date default null,
  k_tel varchar2(32) default null,
  k_fax varchar2(32) default null,
  k_addr nvarchar2(32) default null,
  k_intro nvarchar2(200) default null,
  k_popedom char(1) default '0',
  k_document_show int DEFAULT '0',
  primary key (id)
);

call proc_dropifexists('k_version');
CREATE TABLE k_version (
  id varchar2(16) NOT NULL
);

call proc_dropifexists('k_vote');
CREATE TABLE k_vote (
  id varchar2(16) DEFAULT '' NOT NULL,
  k_title varchar2(32) DEFAULT '' NOT NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_voteitem');
CREATE TABLE k_voteitem (
  id varchar2(16) DEFAULT '' NOT NULL,
  k_name varchar2(32) DEFAULT '' NOT NULL,
  k_vote_id integer DEFAULT NULL,
  PRIMARY KEY (id)
);

call proc_dropifexists('k_meeting');
create sequence seq_k_meeting start with 1 increment by 1;
CREATE TABLE k_meeting (
  id integer constraint seq_k_meeting primary key,
  k_title nvarchar2(100) NOT NULL,
  k_host nvarchar2(100) DEFAULT NULL,
  k_starttime timestamp DEFAULT NULL,
  k_endtime timestamp DEFAULT NULL,
  k_isclosed integer DEFAULT '0' NOT NULL,
  k_meetingid varchar2(100) DEFAULT NULL
);

call proc_dropifexists('k_organclass');
create sequence seq_k_organclass start with 1 increment by 1;
CREATE TABLE k_organclass (
  id integer constraint seq_k_organclass primary key,
  k_oid integer DEFAULT NULL,
  k_class nvarchar2(45) DEFAULT NULL
);

call proc_dropifexists('k_organization');
create sequence seq_k_organization start with 1 increment by 1;
CREATE TABLE k_organization (
  id integer constraint seq_k_organization primary key,
  k_type nvarchar2(45) NOT NULL,
  k_classid integer DEFAULT NULL
);

call proc_dropifexists('k_risktype');
create sequence seq_k_risktype start with 1 increment by 1;
CREATE TABLE k_risktype (
  id integer constraint seq_k_risktype primary key,
  k_type nvarchar2(100) DEFAULT NULL
);

call proc_dropifexists('k_userfav');
create sequence seq_k_userfav start with 1 increment by 1;
CREATE TABLE k_userfav (
  id integer constraint seq_k_userfav primary key,
  k_userid int NOT NULL,
  k_docid varchar2(16) NOT NULL,
  k_type int DEFAULT '0',
  k_favdate date DEFAULT NULL
);

call proc_dropifexists('k_userinfo');
create sequence seq_k_userinfo start with 1 increment by 1;
CREATE TABLE k_userinfo (
  id integer constraint seq_k_userinfo primary key,
  k_idcard varchar2(20) DEFAULT NULL,
  k_acount varchar2(45) DEFAULT NULL,
  k_password varchar2(45) DEFAULT NULL,
  k_realname nvarchar2(45) DEFAULT NULL,
  k_birthday date DEFAULT NULL,
  k_sex int DEFAULT '0',
  k_education nvarchar2(100) DEFAULT NULL,
  k_professional nvarchar2(100) NULL,
  k_email varchar2(200) DEFAULT NULL,
  k_phone varchar2(45) DEFAULT NULL,
  k_mobile varchar2(45) DEFAULT NULL,
  k_organization nvarchar2(80) DEFAULT NULL,
  k_department nvarchar2(80) DEFAULT NULL,
  k_position nvarchar2(80) DEFAULT NULL,
  k_location nvarchar2(200) DEFAULT NULL,
  k_zipcode varchar2(20) DEFAULT NULL,
  k_isfee int DEFAULT '0',
  k_isadmin int DEFAULT '0',
  k_adminnum int DEFAULT '0',
  k_usednum int DEFAULT '0',
  k_score int DEFAULT '0',
  k_mac varchar2(500) DEFAULT '',
  k_active int DEFAULT '0',
  k_hobby nvarchar2(200) NULL,
  k_salary varchar2(100) NULL,
  k_attend varchar2(200) NULL,
  k_exhibition varchar2(200) NULL,
  k_receive varchar2(100) DEFAULT NULL,
  k_ismarry int DEFAULT '0',
  k_spouse varchar2(200) NULL,
  k_family nvarchar2(200) NULL,
  k_workarea nvarchar2(200) NULL,
  k_unit varchar2(200) NULL,
  k_homearea nvarchar2(200) NULL,
  k_officephone varchar2(45) NULL,
  k_fax varchar2(45) NULL,
  k_rejection varchar2(200) NULL,
  k_bestway varchar2(200) NULL,
  k_incomingway varchar2(200) NULL,
  k_regdate timestamp DEFAULT NULL,
  k_identity int DEFAULT '0',
  k_avatar varchar2(100) NULL,
  k_count int DEFAULT '0',
  k_type varchar2(255) NULL
);

call proc_dropifexists('k_usertype');
create sequence seq_k_usertype start with 1 increment by 1;
CREATE TABLE k_usertype (
  id integer constraint seq_k_usertype primary key,
  k_name nvarchar2(200) DEFAULT NULL,
  k_rights int DEFAULT '0' NOT NULL,
  k_parent int DEFAULT '0',
  k_onscore int DEFAULT '0',
  k_macnum int DEFAULT '0',
  k_order int DEFAULT '0'
);

insert into k_user (id, k_username, k_password, k_email, k_qq, k_birthday, k_tel, k_fax, k_addr, k_intro, k_popedom)
values ('10935283-5777490', 'admin', 'admin123', null, '710633139', null, null, null, null, null, '3');

insert into k_site (id, k_name, k_domain, k_admin, k_lang, k_html, k_html_rule, k_html_dir, k_mailserver, k_mailuser, k_mailpass, k_pub_dir, k_template)
values ('1', 'KnifeCMS', 'www.knifecms.com', null, 'zh-cn', '1', '0', '0', null, null, null, 'default', 'default');
commit;
