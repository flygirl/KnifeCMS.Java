SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*KnifeCMS 执行脚本 for Mysql*/;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
DROP TABLE IF EXISTS `k_grab`;
CREATE TABLE IF NOT EXISTS `k_grab` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `k_title` varchar(200) DEFAULT NULL,
  `k_url` varchar(255) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_grab_news`;
CREATE TABLE IF NOT EXISTS `k_grab_news` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `k_title` varchar(200) DEFAULT NULL,
  `k_source` varchar(100) DEFAULT NULL,
  `k_author` varchar(45) DEFAULT NULL,
  `k_url` varchar(200) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `k_filetype` varchar(45) DEFAULT NULL,
  `k_content` text,
  PRIMARY KEY (`id`)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_grab_rule`;
CREATE TABLE IF NOT EXISTS `k_grab_news_rule` (
	`id` varchar(32) NOT NULL DEFAULT '',
	
	PRIMARY KEY (`id`)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_document`;
CREATE TABLE IF NOT EXISTS `k_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `k_title` varchar(200) DEFAULT NULL,
  `k_source` varchar(100) DEFAULT NULL,
  `k_author` varchar(45) DEFAULT NULL,
  `k_risktype` varchar(32) DEFAULT NULL,
  `k_documenttype` varchar(32) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `k_filetype` varchar(45) DEFAULT NULL,
  `k_fileurl` varchar(300) DEFAULT NULL,
  `k_isfee` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_documenttype`;
CREATE TABLE IF NOT EXISTS `k_documenttype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `k_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_english`;
CREATE TABLE IF NOT EXISTS `k_english` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k_en` varchar(200) DEFAULT NULL,
  `k_cn` varchar(200) DEFAULT NULL,
  `k_means` text,
  `k_hot` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_history`;
CREATE TABLE IF NOT EXISTS `k_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k_usermail` varchar(100) DEFAULT NULL,
  `k_ip` varchar(50) DEFAULT NULL,
  `k_os` varchar(50) DEFAULT NULL,
  `k_browser` varchar(50) DEFAULT NULL,
  `k_url` varchar(100) DEFAULT NULL,
  `k_accessdate` date DEFAULT '0000-00-00',
  `k_accesstime` time DEFAULT '00:00:00',
  `k_endtime` time DEFAULT '00:00:00',
  `k_type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_comment`;
CREATE TABLE IF NOT EXISTS `k_comment` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_user` varchar(16) DEFAULT NULL,
  `k_date` timestamp DEFAULT CURRENT_TIMESTAMP,
  `k_news_id` varchar(16) DEFAULT NULL,
  `k_comment` text,
  `k_display` int(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_custom`;
CREATE TABLE IF NOT EXISTS `k_custom` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `k_site` varchar(32) DEFAULT NULL,
  `k_name` varchar(100) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_url` varchar(200) DEFAULT NULL,
  `k_status` char(1) DEFAULT 0,
  `k_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_fields`;
CREATE TABLE IF NOT EXISTS `k_fields` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_name` varchar(100) DEFAULT NULL,
  `k_formid` varchar(16) DEFAULT NULL,
  `k_newsid` varchar(16) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_filter`;
CREATE TABLE IF NOT EXISTS `k_filter` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_flow`;
CREATE TABLE IF NOT EXISTS `k_flow` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `k_site` varchar(32) DEFAULT NULL,
  `k_name` varchar(100) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_url` varchar(200) DEFAULT NULL,
  `k_status` int(1) DEFAULT 0,
  `k_description` text,
  `k_type` int(4),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_form`;
CREATE TABLE IF NOT EXISTS `k_form` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_name` varchar(200) DEFAULT NULL,
  `k_content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_userhistory`;
CREATE TABLE IF NOT EXISTS `k_userhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k_usermail` varchar(100) DEFAULT NULL,
  `k_ip` varchar(50) DEFAULT NULL,
  `k_os` varchar(50) DEFAULT NULL,
  `k_browser` varchar(50) DEFAULT NULL,
  `k_url` varchar(100) DEFAULT NULL,
  `k_accessdate` date DEFAULT '0000-00-00',
  `k_accesstime` time DEFAULT '00:00:00',
  `k_endtime` time DEFAULT '00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_logs`;
CREATE TABLE IF NOT EXISTS `k_logs` (
  `id` varchar(16) NOT NULL,
  `k_content` VARCHAR(255) NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_news`;
CREATE TABLE IF NOT EXISTS `k_news` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `k_title` varchar(100) NOT NULL DEFAULT '',
  `k_source` varchar(100) DEFAULT NULL,
  `k_keywords` varchar(100) DEFAULT NULL,
  `k_file` varchar(200) DEFAULT NULL,
  `k_content` text NOT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_username` varchar(32) DEFAULT NULL,
  `k_type` varchar(16) DEFAULT NULL,
  `k_link` varchar(16) DEFAULT NULL,
  `k_commend` varchar(6) DEFAULT NULL,
  `k_template` varchar(32) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_display` char(1) DEFAULT '0',
  `k_tofront` char(1) DEFAULT '0',
  `k_count` int(11) DEFAULT '0',
  `k_treenews` varchar(16) DEFAULT NULL,
  `k_docfile` varchar(200) DEFAULT NULL,
  `k_isfee` char(1) DEFAULT '0',
  `k_type2` varchar(255) DEFAULT NULL,
  `k_doctype` varchar(16) DEFAULT NULL,
  `k_summary` text,
  `k_rights` int(11) DEFAULT '0',
  `k_author` varchar(100) DEFAULT NULL,
  `k_score` int(11) DEFAULT '0',
  `k_search` char(1) DEFAULT '0',
  `k_ispub` char(1) DEFAULT '0',
  `k_offdate` timestamp NULL,
  `k_flowusers` varchar(500) DEFAULT NULL,
  `k_xmldata` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TRIGGER IF EXISTS `t_add_news`;
DELIMITER //
CREATE TRIGGER `t_add_news` BEFORE INSERT ON `k_news`
 FOR EACH ROW BEGIN
DECLARE ordernums INTEGER(4);
    SELECT COUNT(*) INTO ordernums FROM k_news Where k_type=NEW.k_type;
    SET NEW.k_order=ordernums+1;
END
//
DELIMITER ;

CREATE TABLE `k_news_version` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `k_versionnumber` int(11) DEFAULT NULL,
  `k_description` varchar(100) DEFAULT '',
  `k_newsid` varchar(16) DEFAULT NULL,
  `k_title` varchar(100) DEFAULT '',
  `k_source` varchar(100) DEFAULT NULL,
  `k_keywords` varchar(100) DEFAULT NULL,
  `k_file` varchar(200) DEFAULT NULL,
  `k_content` text NOT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_username` varchar(32) DEFAULT NULL,
  `k_type` varchar(16) DEFAULT NULL,
  `k_link` varchar(16) DEFAULT NULL,
  `k_commend` varchar(6) DEFAULT NULL,
  `k_template` varchar(32) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_display` char(1) DEFAULT '0',
  `k_tofront` char(1) DEFAULT '0',
  `k_count` int(11) DEFAULT '0',
  `k_treenews` varchar(16) DEFAULT NULL,
  `k_docfile` varchar(200) DEFAULT NULL,
  `k_isfee` char(1) DEFAULT '0',
  `k_type2` varchar(255) DEFAULT NULL,
  `k_doctype` varchar(16) DEFAULT NULL,
  `k_summary` text,
  `k_rights` int(11) DEFAULT '0',
  `k_author` varchar(100) DEFAULT NULL,
  `k_score` int(11) DEFAULT '0',
  `k_search` char(1) DEFAULT '0',
  `k_ispub` char(1) DEFAULT '0',
  `k_offdate` timestamp NULL DEFAULT NULL,
  `k_flowusers` varchar(500) DEFAULT NULL,
  `k_xmldata` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_content`;
CREATE TABLE `k_content` (
  `id` varchar(32) NOT NULL,
  `k_newsid` varchar(32) DEFAULT NULL,
  `k_page` int(11) DEFAULT '0',
  `k_content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_order`;
CREATE TABLE IF NOT EXISTS `k_order` (
	`id` VARCHAR(16) NOT NULL,
	`k_user` VARCHAR(16) NOT NULL,
	`k_title` varchar(150) not null,
	`k_goods` VARCHAR(200) NOT NULL,
	`k_money` int(11) DEFAULT '0',
	`k_date` date NULL,
	`k_status` TINYINT NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_relative`;
CREATE TABLE IF NOT EXISTS `k_relative` (
	`id` VARCHAR(16) NOT NULL,
	`k_news_id` VARCHAR(16) NOT NULL,
	`k_rels` VARCHAR(255) NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_pic`;
CREATE TABLE IF NOT EXISTS `k_pic` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_title` varchar(100) DEFAULT NULL,
  `k_url` varchar(100) DEFAULT NULL,
  `k_newsid` varchar(255) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_userid` varchar(16) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_pic_type` varchar(16) DEFAULT NULL,
  `k_display` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TRIGGER IF EXISTS `t_add_pic`;
DELIMITER //
CREATE TRIGGER `t_add_pic` BEFORE INSERT ON `k_pic`
 FOR EACH ROW BEGIN
	DECLARE ordernums INTEGER(4);
    SELECT COUNT(*) INTO ordernums FROM k_pic WHERE k_newsid=new.k_newsid;
    SET NEW.k_order=ordernums+1;
END
//
DELIMITER ;

/*
CREATE TABLE `k_pic_type` (
  `id` varchar(32) NOT NULL,
  `k_name` varchar(100) DEFAULT NULL,
  `k_parent` varchar(16) DEFAULT '0',
  `k_site` varchar(16) DEFAULT NULL,
  `k_userid` int(10) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_order` varchar(16) DEFAULT NULL,
  `k_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/
DROP TABLE IF EXISTS `k_pic_type`;
CREATE TABLE `k_pic_type` (
  `id` varchar(32) NOT NULL,
  `k_name` varchar(100) DEFAULT NULL,
  `k_userid` int(10) DEFAULT NULL,
  `k_description` varchar(255) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_parent` varchar(16) DEFAULT '0',
  `k_site` varchar(16) DEFAULT NULL,
  `k_display` char(1) DEFAULT '0',
  `k_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_video`;
CREATE TABLE IF NOT EXISTS `k_video` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_title` varchar(100) DEFAULT NULL,
  `k_url` varchar(100) DEFAULT NULL,
  `k_newsid` varchar(255) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_userid` varchar(16) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_display` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_record`;
CREATE TABLE IF NOT EXISTS `k_record` (
  `id` varchar(16) NOT NULL,
  `k_user_id` varchar(16) NOT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_operation` varchar(200) NOT NULL,
  `k_news_id` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_role`;
CREATE TABLE IF NOT EXISTS `k_role` (
  `id` varchar(16) NOT NULL,
  `k_name` varchar(100) NOT NULL,
  `k_username` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_subscribe`;
CREATE TABLE IF NOT EXISTS `k_subscribe` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_name` varchar(100) DEFAULT NULL,
  `k_form` varchar(100) DEFAULT NULL,
  `k_user` varchar(100) DEFAULT NULL,
  `k_stype` CHAR(1) NULL,
  `k_mail` VARCHAR(150) NULL,
  `k_phone` VARCHAR(50) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_position`;
CREATE TABLE `k_position` (
  `id` varchar(36) NOT NULL,
  `k_name` varchar(100) DEFAULT NULL,
  `k_author` varchar(100) DEFAULT NULL,
  `k_number` int(11) DEFAULT NULL,
  `k_type` varchar(100) DEFAULT NULL,
  `k_workplace` varchar(100) DEFAULT NULL,
  `k_department` varchar(100) DEFAULT NULL,
  `k_content` text NOT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_offdate` timestamp NULL DEFAULT NULL,
  `k_display` char(1) DEFAULT '0',
  `k_ispub` char(1) DEFAULT '0',
  `k_search` char(1) DEFAULT '0',
  `k_count` int(11) DEFAULT '0',
  `k_keywords` varchar(100) DEFAULT NULL,
  `k_summary` text,
  `k_applynumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for k_resume
-- ----------------------------
DROP TABLE IF EXISTS `k_resume`;
CREATE TABLE `k_resume` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `k_name` varchar(100) DEFAULT NULL,
  `k_phone` varchar(50) DEFAULT NULL,
  `k_mail` varchar(150) DEFAULT NULL,
  `k_resume_file` varchar(200) DEFAULT NULL,
  `k_doctype` varchar(16) DEFAULT NULL,
  `k_postid` varchar(36) DEFAULT NULL,
  `k_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `k_is_otherdepartment` varchar(1) DEFAULT NULL,
  `k_is_othercompnay` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_supplier`;
CREATE TABLE IF NOT EXISTS `k_supplier` (
 `id` VARCHAR(16) NOT NULL,
 `k_user` VARCHAR(32) NULL,
 `k_name` VARCHAR(200) NULL,
 `k_sid` VARCHAR(20) NULL,
 `k_sname` VARCHAR(200) NULL,
 `k_card` VARCHAR(20) NULL,
 PRIMARY KEY (`id`)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_bill`;
CREATE TABLE IF NOT EXISTS `k_bill` (
 `id` VARCHAR(16) NOT NULL,
 `k_suser` VARCHAR(32) NULL,
 `k_file` VARCHAR(200) NULL,
 `k_date` date NULL,
 PRIMARY KEY (`id`)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_theme`;
CREATE TABLE IF NOT EXISTS `k_theme` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_name` varchar(50) DEFAULT NULL,
  `k_description` varchar(200) DEFAULT NULL,
  `k_css` text,
  `k_status` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_thumbnail`;
CREATE TABLE IF NOT EXISTS `k_thumbnail` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_link` varchar(16) DEFAULT NULL,
  `k_url` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_tree`;
CREATE TABLE IF NOT EXISTS `k_tree` (
  `id` varchar(32) NOT NULL,
  `k_name` varchar(100) DEFAULT NULL,
  `k_tree_type` varchar(32) NOT NULL,
  `k_value` text,
  `k_show` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_type`;
CREATE TABLE IF NOT EXISTS `k_type` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `k_name` varchar(100) NOT NULL DEFAULT '',
  `k_parent` varchar(16) DEFAULT '0',
  `k_value` varchar(255) DEFAULT NULL,
  `k_order` varchar(16) DEFAULT NULL,
  `k_index_template` varchar(100) DEFAULT NULL,
  `k_type_template` varchar(100) DEFAULT NULL,
  `k_news_template` varchar(100) DEFAULT NULL,
  `k_form` varchar(100) DEFAULT NULL,
  `k_admin` varchar(200) DEFAULT NULL,
  `k_audit` varchar(200) DEFAULT NULL,
  `k_display` char(1) DEFAULT '0',
  `k_show` char(1) DEFAULT '0',
  `k_ismember` char(1) DEFAULT '0',
  `k_url` varchar(200) DEFAULT NULL,
  `k_target` char(1) DEFAULT '0',
  `k_pagenum` varchar(16) DEFAULT NULL,
  `k_pagesize` int DEFAULT 10,
  `k_orderby` varchar(100) DEFAULT NULL,
  `k_search` char(1) DEFAULT '1',
  `k_tree` varchar(32) DEFAULT NULL,
  `k_tree_id` varchar(32) DEFAULT '',
  `k_site` varchar(16) DEFAULT NULL,
  `k_needpub` char(1) default '0',
  `k_html` char(1) default '0',
  `k_html_rule` char(1) default '0',
  `k_flow` varchar(32) DEFAULT NULL,
  `k_html_dir` char(1) default '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TRIGGER IF EXISTS `t_add_type`;
DELIMITER //
CREATE TRIGGER `t_add_type` BEFORE INSERT ON `k_type`
 FOR EACH ROW BEGIN
	DECLARE ordernums INTEGER(4);
    SELECT COUNT(*) INTO ordernums FROM k_type WHERE k_parent=new.k_parent and k_tree_id=NEW.k_tree_id;
    SET NEW.k_order=ordernums+1;
    IF NEW.k_type_template='' or NEW.k_type_template is Null THEN
    	SET NEW.k_type_template='list.htm';
    END IF;
    IF NEW.k_news_template='' or NEW.k_news_template is Null THEN
    	SET NEW.k_news_template='news.htm';
    END IF;
    IF NEW.k_site='' or NEW.k_site is Null THEN
    	SET NEW.k_site='1';
    END IF;
END
//
DELIMITER ;

DROP TABLE IF EXISTS `k_type2`;
CREATE TABLE `k_type2` (
 `id` VARCHAR(16) NOT NULL,
 `k_name` VARCHAR(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_site`;
CREATE TABLE `k_site` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `k_name` varchar(100) DEFAULT NULL,
  `k_domain` varchar(200) DEFAULT NULL,
  `k_admin` varchar(200) DEFAULT NULL,
  `k_lang` varchar(8) DEFAULT NULL,
  `k_html` char(1) DEFAULT '0',
  `k_html_rule` char(1) DEFAULT '0',
  `k_html_dir` char(1) DEFAULT '0',
  `k_mailserver` varchar(200) DEFAULT NULL,
  `k_mailuser` varchar(100) DEFAULT NULL,
  `k_mailpass` varchar(100) DEFAULT NULL,
  `k_isthumb` char(1) DEFAULT '0',
  `k_order` varchar(16) DEFAULT NULL,
  `k_pub_dir` varchar(100) DEFAULT NULL,
  `k_template` varchar(100) DEFAULT NULL,
  `k_big5` char(1) DEFAULT '0',
  `k_index` varchar(100) DEFAULT NULL,
  `k_theme` varchar(32) DEFAULT NULL,
  `k_css_file` varchar(100) DEFAULT NULL,
  `k_commcheck` int(11) DEFAULT '0',
  `k_isconvert_library` int(11) DEFAULT '0',
  `k_isconvert_streammedia` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TRIGGER IF EXISTS `t_add_site`;
DELIMITER //
CREATE TRIGGER `t_add_site` BEFORE INSERT ON `k_site`
 FOR EACH ROW BEGIN
	DECLARE ordernums INTEGER(4);
    SELECT COUNT(*) INTO ordernums FROM k_site;
    SET NEW.k_order=ordernums+1;
    IF NEW.k_pub_dir='' or NEW.k_pub_dir is Null THEN
    	SET NEW.k_pub_dir=NEW.k_domain;
    END IF;
    IF NEW.k_template='' or NEW.k_template is Null THEN
    	SET NEW.k_template=NEW.k_domain;
    END IF;
END
//
DELIMITER ;

DROP TABLE IF EXISTS `k_user`;
CREATE TABLE IF NOT EXISTS `k_user` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_username` varchar(32) NOT NULL DEFAULT '',
  `k_password` varchar(32) NOT NULL DEFAULT '',
  `k_email` varchar(32) DEFAULT NULL,
  `k_qq` varchar(32) DEFAULT NULL,
  `k_birthday` date DEFAULT NULL,
  `k_sysj` date DEFAULT NULL,
  `k_tel` varchar(32) DEFAULT NULL,
  `k_fax` varchar(32) DEFAULT NULL,
  `k_addr` varchar(100) DEFAULT NULL,
  `k_intro` varchar(200) DEFAULT NULL,
  `k_popedom` int(1) DEFAULT 0,
  `k_document_show` int(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_version`;
CREATE TABLE IF NOT EXISTS `k_version` (
  `id` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_vote`;
CREATE TABLE IF NOT EXISTS `k_vote` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_title` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_voteitem`;
CREATE TABLE IF NOT EXISTS `k_voteitem` (
  `id` varchar(16) NOT NULL DEFAULT '',
  `k_name` varchar(32) NOT NULL DEFAULT '',
  `k_vote_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `k_meeting`;
CREATE TABLE IF NOT EXISTS `k_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k_title` varchar(100) NOT NULL,
  `k_host` varchar(100) DEFAULT NULL,
  `k_starttime` datetime DEFAULT NULL,
  `k_endtime` datetime DEFAULT NULL,
  `k_isclosed` tinyint(1) NOT NULL DEFAULT '0',
  `k_meetingid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_organclass`;
CREATE TABLE IF NOT EXISTS `k_organclass` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `k_oid` int(10) unsigned DEFAULT NULL,
  `k_class` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_organization`;
CREATE TABLE IF NOT EXISTS `k_organization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `k_type` varchar(45) NOT NULL,
  `k_classid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_risktype`;
CREATE TABLE IF NOT EXISTS `k_risktype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `k_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_userfav`;
CREATE TABLE IF NOT EXISTS `k_userfav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k_userid` int(11) NOT NULL,
  `k_docid` varchar(16) NOT NULL,
  `k_type` int(11) DEFAULT '0',
  `k_favdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_userinfo`;
CREATE TABLE IF NOT EXISTS `k_userinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `k_idcard` varchar(20) DEFAULT NULL,
  `k_acount` varchar(45) DEFAULT NULL,
  `k_password` varchar(45) DEFAULT NULL,
  `k_realname` varchar(45) DEFAULT NULL,
  `k_birthday` datetime DEFAULT NULL,
  `k_sex` tinyint(1) DEFAULT '0',
  `k_education` varchar(100) DEFAULT NULL,
  `k_professional` varchar(100) NULL,
  `k_email` varchar(200) DEFAULT NULL,
  `k_phone` varchar(45) DEFAULT NULL,
  `k_mobile` varchar(45) DEFAULT NULL,
  `k_area` varchar(80) DEFAULT NULL,
  `k_organization` varchar(80) DEFAULT NULL,
  `k_department` varchar(80) DEFAULT NULL,
  `k_position` varchar(80) DEFAULT NULL,
  `k_location` varchar(200) DEFAULT NULL,
  `k_zipcode` varchar(20) DEFAULT NULL,
  `k_isfee` int(11) DEFAULT '0',
  `k_isadmin` int(11) DEFAULT '0',
  `k_adminnum` int(11) DEFAULT '0',
  `k_usednum` int(11) DEFAULT '0',
  `k_score` bigint(20) DEFAULT '0',
  `k_mac` varchar(500) DEFAULT '',
  `k_active` tinyint(4) DEFAULT '0',
  `k_hobby` VARCHAR(200) NULL,
  `k_salary` VARCHAR(100) NULL,
  `k_attend` VARCHAR(200) NULL,
  `k_exhibition` VARCHAR(200) NULL,
  `k_receive` VARCHAR(100) DEFAULT NULL,
  `k_ismarry` int(1) DEFAULT '0',
  `k_spouse` VARCHAR(200) NULL,
  `k_family` VARCHAR(200) NULL,
  `k_workarea` VARCHAR(200) NULL,
  `k_unit` VARCHAR(200) NULL,
  `k_homearea` VARCHAR(200) NULL,
  `k_officephone` VARCHAR(45) NULL,
  `k_fax` VARCHAR(45) NULL,
  `k_rejection` VARCHAR(200) NULL,
  `k_bestway` VARCHAR(200) NULL,
  `k_incomingway` VARCHAR(200) NULL,
  `k_regdate` datetime DEFAULT NULL,
  `k_sysj` date DEFAULT NULL,
  `k_identity` int(11) DEFAULT '0',
  `k_avatar` VARCHAR(100) NULL,
  `k_count` int(11) DEFAULT '0',
  `k_type` VARCHAR(255) NULL,
  `k_fans` int(11) DEFAULT '0',
  `k_attention` int(11) DEFAULT '0',
  `k_favorite` int(11) DEFAULT '0',
  `k_article` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

/*
Navicat MySQL Data Transfer
Source Host     : localhost:3306
Source Database : wanda
Target Host     : localhost:3306
Target Database : wanda
Date: 2013-07-08 11:15:58
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for k_workflow_node
-- ----------------------------
DROP TABLE IF EXISTS `k_workflow_node`;
CREATE TABLE `k_workflow_node` (
  `id` varchar(32) NOT NULL,
  `k_name` varchar(200) DEFAULT NULL,
  `k_type` int(11) DEFAULT NULL,
  `k_workflow_id` varchar(32) DEFAULT NULL,
  `k_step` int(11) unsigned DEFAULT NULL,
  `k_init_function` varchar(200) DEFAULT NULL,
  `k_trans_function` varchar(200) DEFAULT NULL,
  `k_run_function` varchar(200) DEFAULT NULL,
  `k_save_function` varchar(200) DEFAULT NULL,
  `k_prev_node` varchar(32) DEFAULT NULL,
  `k_next_node` varchar(32) DEFAULT NULL,
  `k_executor` varchar(200) DEFAULT NULL,
  `k_execute_type` int(11) DEFAULT NULL,
  `k_remind` int(11) DEFAULT NULL,
  `k_field` varchar(200) DEFAULT NULL,
  `k_max_day` int(11) DEFAULT NULL,
  `k_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of k_workflow_node
-- ----------------------------



DROP TABLE IF EXISTS `k_usertype`;
CREATE TABLE IF NOT EXISTS `k_usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k_name` varchar(200) DEFAULT NULL,
  `k_rights` int(11) NOT NULL DEFAULT '0',
  `k_parent` int(11) DEFAULT '0',
  `k_onscore` int(11) DEFAULT '0',
  `k_macnum` int(11) DEFAULT '0',
  `k_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

insert into `k_user` (id, k_username, k_password, k_email, k_qq, k_birthday, k_tel, k_fax, k_addr, k_intro, k_popedom)
values ('10935283-5777490', 'admin', 'admin123', null, '710633139', null, null, null, null, null, '3');

insert into `k_site` (id, k_name, k_domain, k_admin, k_lang, k_html, k_html_rule, k_html_dir, k_mailserver, k_mailuser, k_mailpass, k_pub_dir, k_template)
values ('1', 'KnifeCMS', 'www.knifecms.com', null, 'zh-cn', '1', '0', '0', null, null, null, 'default', 'default');
commit;