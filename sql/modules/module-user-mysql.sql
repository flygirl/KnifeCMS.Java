SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*KnifeCMS执行脚本:数据初始化*/;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `databasedocument`;
CREATE TABLE IF NOT EXISTS `databasedocument` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `risktype` varchar(32) DEFAULT NULL,
  `documenttype` varchar(32) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `filetype` varchar(45) DEFAULT NULL,
  `fileurl` varchar(300) DEFAULT NULL,
  `isfee` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `documenttype`;
CREATE TABLE IF NOT EXISTS `documenttype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `english`;
CREATE TABLE IF NOT EXISTS `english` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en` varchar(200) DEFAULT NULL,
  `cn` varchar(200) DEFAULT NULL,
  `means` text,
  `hot` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usermail` varchar(100) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `accessdate` date DEFAULT '0000-00-00',
  `accesstime` time DEFAULT '00:00:00',
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `meeting`;
CREATE TABLE IF NOT EXISTS `meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `host` varchar(100) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isclosed` tinyint(1) NOT NULL DEFAULT '0',
  `meetingid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `organclass`;
CREATE TABLE IF NOT EXISTS `organclass` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oid` int(10) unsigned DEFAULT NULL,
  `Class` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `organization`;
CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `risktype`;
CREATE TABLE IF NOT EXISTS `risktype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `score`;
CREATE TABLE IF NOT EXISTS `score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `points` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `landlord`;
CREATE TABLE IF NOT EXISTS `landlord` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `buildarea` varchar(100) DEFAULT NULL,
  `rmbamount` DECIMAL(9,2) DEFAULT '0.00',
  `currency` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `iscomplete` int(11) DEFAULT '0',
  `prompt` varchar(100) DEFAULT NULL,
  `moneyitem` varchar(200) DEFAULT NULL,
  `amount` DECIMAL(9,2) DEFAULT '0.00',
  `currency` varchar(100) DEFAULT NULL,
  `rate` float(6,4) DEFAULT '1.0000',
  `rmbamount` DECIMAL(9,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `process`;
CREATE TABLE IF NOT EXISTS `process` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `level` int(11) DEFAULT '0',
  `type` varchar(200) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `userfav`;
CREATE TABLE IF NOT EXISTS `userfav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `docid` varchar(16) NOT NULL,
  `type` int(11) DEFAULT '0',
  `favdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idcard` varchar(20) DEFAULT NULL,
  `acount` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `realname` varchar(45) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `education` varchar(100) DEFAULT NULL,
  `professional` varchar(100) NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `organization` varchar(80) DEFAULT NULL,
  `department` varchar(80) DEFAULT NULL,
  `position` varchar(80) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `isfee` int(11) DEFAULT '0',
  `isadmin` int(11) DEFAULT '0',
  `adminnum` int(11) DEFAULT '0',
  `usednum` int(11) DEFAULT '0',
  `score` bigint(20) DEFAULT '0',
  `mac` varchar(500) DEFAULT '',
  `active` tinyint(4) DEFAULT '0',
  `hobby` VARCHAR(200) NULL,
  `salary` VARCHAR(100) NULL,
  `attend` VARCHAR(200) NULL,
  `exhibition` VARCHAR(200) NULL,
  `receive` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `usertype`;
CREATE TABLE IF NOT EXISTS `usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `rights` int(11) NOT NULL DEFAULT '0',
  `onscore` int(11) DEFAULT '0',
  `macnum` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `k_supplier`;
CREATE TABLE IF NOT EXISTS `k_supplier` (
 `id` VARCHAR(16) NOT NULL,
 `k_user` VARCHAR(32) NULL,
 `k_name` VARCHAR(200) NULL,
 `k_sid` VARCHAR(20) NULL,
 `k_sname` VARCHAR(200) NULL,
 `k_card` VARCHAR(20) NULL,
 `k_stype` VARCHAR(200) NULL,
 `k_tel` VARCHAR(20) NULL,
 `k_email` VARCHAR(20) NULL,
 `k_message` VARCHAR(255) NULL,
 `k_discount` VARCHAR(100) NULL,
 `k_license` VARCHAR(200) NULL,
 `k_logo` VARCHAR(200) NULL,
 PRIMARY KEY (`id`)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;